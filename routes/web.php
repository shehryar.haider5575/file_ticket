<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\CategoryController;
use Illuminate\Support\Facades\Auth;

Route::middleware('guest:admin')->group(function () {

    Route::get('admin/login','Admin\LoginController@loginView')->name('login');
    Route::get('admin/register','Admin\LoginController@registerView')->name('register');
});

Route::post('admin/login', 'Admin\LoginController@login')->name('login');
Route::post('admin/register', 'Admin\LoginController@register')->name('register');
Route::get('admin/logout', 'Admin\LoginController@logout')->name('logout');

// Route For Admin
Route::group(['prefix' => 'admin'], function () {
    Route::get('test', function () {
        $data = [
            "iconLocation"=> "1260",
            "ticketType"=> "noticeOfFine",
            "offenceNumber"=> "0843112F",
            "lastOrBusinessName"=> "SAJ",
            "dateOffence"=> "2021/03/16"
        ];
        $client     =   new \GuzzleHttp\Client();
        $URI = 'http://74.208.108.24:7001/dev/api';
        $full = json_encode($data);
        try {
            $response = $client->request('POST',$URI, [
                'body'          => $full,
                'headers'       => [
                    'Content-Type'  => 'application/json',
                    'Accept'  => 'application/json',
                ]
            ]);
            $res_data = $response->getBody();
            $res_data = json_decode($res_data);
            dd("dfasdfaf",$res_data);
            return response()->json(['success'=>$res_data->data],200);

        } catch (\Exception $exception) {
            $e = $exception->getMessage();
            dd($e);
            $res1 = explode('data',$exception->getMessage());
            $res2 = explode('error',$res1[1]);
            $error = str_replace( array( '\'', '"',':','}',',' , ';', '<', '>' ), ' ',$res2[1]);
            return response()->json(['error'=>$error],499);
        }
    });

    Route::middleware('auth')->group(function () {
        Route::post('update-personal-comments', 'Admin\DashboardController@updatePersonalComments')->name('update_comments');
        // Route::post('update-payticket-comments', 'Admin\DashboardController@updatePayticketComments')->name('update_payticket_comments');
        // Route::Profile Update
        Route::get('/', 'Admin\DashboardController@index')->name('home');
        Route::get('profile/setting', 'Admin\DashboardController@profile')->name('profile.setting');
        Route::patch('/profile/update', 'Admin\DashboardController@setting')->name('profile.update');

        // ROUTE::Trial Request
        Route::group(['prefix' => 'ticket_request/{type}'], function () {

            Route::get('/', 'Admin\TrialRequestController@index')->name('trial_request_dashboard');
            Route::get('/datatable', 'Admin\TrialRequestController@datatable')->name('trial_request_dashboard.datatable');
            Route::get('history', 'Admin\TrialRequestController@trialHistory')->name('trial_history_dashboard');
            Route::get('history/datatable', 'Admin\TrialRequestController@historyDatatable')->name('trial_history_dashboard.datatable');
            Route::get('/close/{contact}', 'Admin\TrialRequestController@closeTicket')->name('trial_request_dashboard.close');
            Route::get('/edit/{contact}', 'Admin\TrialRequestController@editTicket')->name('trial_request_dashboard.edit');
            Route::post('/ticketimageupdate/{ticket}', 'Admin\TrialRequestController@ticketImageUpdate')->name('trial_request_dashboard.image_update');
            Route::get('/tickets/word_export/{ticket}', 'Admin\TrialRequestController@wordExport')->name('trial_request_dashboard.word_export');
            Route::get('/tickets/send-email-pdf/{ticket}', 'Admin\TrialRequestController@mailToCourt')->name('trial_request_dashboard.mail_court');
            Route::put('/updateticket/{ticket}/', 'Admin\TrialRequestController@updateTicket')->name('trial_request_dashboard.update');
            Route::post('/updatecontact/{contact}/', 'Admin\TrialRequestController@updateContact')->name('trial_request_dashboard_contact.update');
            Route::get('/view/{contact}', 'Admin\TrialRequestController@view')->name('trial_request_dashboard.view');
            Route::put('/detail/update/{ticket}', 'Admin\TrialRequestController@updateTrialRequestTicketDetail')->name('trial_request_dashboard.detail.update');
            Route::delete('/detail/delete/{ticket}', 'Admin\TrialRequestController@destroyticket')->name('trial_request_dashboard.ticket.delete');
            Route::patch('/client-status', 'Admin\TrialRequestController@clientStatus')->name('trial_request_dashboard.client-status');

            Route::get('/exporthistory/excel/{entries}','Admin\TrialRequestController@exportHistory')->name('excel_export');
            Route::get('/exporthistory/payticket/excel/{entries}','Admin\TrialRequestController@exportPayTicketHistory')->name('excel_export_payticket');
        });

        // ROUTE::Pay Ticket
        Route::group(['prefix' => 'pay_ticket/{type}'], function () {

            Route::get('/', 'Admin\PayTicketController@index')->name('pay_ticket');
            Route::get('/datatable', 'Admin\PayTicketController@datatable')->name('pay_ticket.datatable');
            Route::get('history/', 'Admin\PayTicketController@history')->name('payticket_history_dashboard');
            Route::get('history/datatable', 'Admin\PayTicketController@historyDatatable')->name('payticket_history_dashboard.datatable');
            Route::get('/view/{payticket}', 'Admin\PayTicketController@view')->name('pay_ticket.view');
            Route::get('/edit/{payticket}', 'Admin\PayTicketController@edit')->name('pay_ticket.edit');
            Route::patch('/status', 'Admin\PayTicketController@status')->name('pay_ticket.status');
            Route::delete('/delete', 'Admin\PayTicketController@destroy')->name('pay_ticket.delete');
            Route::patch('/client-status', 'Admin\PayTicketController@clientStatus')->name('pay_ticket_type.client-status');

            Route::get('/close/{payticket}', 'Admin\PayTicketController@closeTicket')->name('pay_ticket_dashboard.close');
            Route::post('/ticketimageupdate/{payticket}', 'Admin\PayTicketController@ticketImageUpdate')->name('pay_ticket.image_update');
            Route::get('/tickets/word_export/{payticket}', 'Admin\PayTicketController@wordExport')->name('pay_ticket.word_export');
            Route::get('/tickets/send-email-pdf/{payticket}', 'Admin\PayTicketController@mailToCourt')->name('pay_ticket.mail_court');
            Route::post('/updateticket/{payticket}/', 'Admin\PayTicketController@updateTicket')->name('pay_ticket.update');
            Route::get('/view/{payticket}', 'Admin\PayTicketController@view')->name('pay_ticket.view');
            Route::put('/detail/update/{payticket}', 'Admin\PayTicketController@updateTrialRequestTicketDetail')->name('pay_ticket.detail.update');
            Route::delete('/detail/delete/{payticket}', 'Admin\PayTicketController@destroyticket')->name('pay_ticket.ticket.delete');
        });

        Route::prefix('potential_clients')->group(function () {

            // ROUTE::Trial Request
            Route::group(['prefix' => 'ticket_request/{type}'], function () {

                Route::get('/', 'Admin\TrialRequestController@index')->name('trial_request_potential_dashboard');
                Route::get('/datatable', 'Admin\TrialRequestController@potentialDatatable')->name('trial_request_potential_dashboard.datatable');
                Route::get('history', 'Admin\TrialRequestController@trialHistory')->name('trial_history_potential_dashboard');
                Route::get('history/datatable', 'Admin\TrialRequestController@historyDatatable')->name('trial_history_potential_dashboard.datatable');
                Route::get('/close/{contact}', 'Admin\TrialRequestController@closeTicket')->name('trial_request_potential_dashboard.close');
                Route::get('/edit/{contact}', 'Admin\TrialRequestController@editTicket')->name('trial_request_potential_dashboard.edit');
                Route::post('/ticketimageupdate/{ticket}', 'Admin\TrialRequestController@ticketImageUpdate')->name('trial_request_potential_dashboard.image_update');
                Route::get('/tickets/word_export/{ticket}', 'Admin\TrialRequestController@wordExport')->name('trial_request_potential_dashboard.word_export');
                Route::get('/tickets/send-email-pdf/{ticket}', 'Admin\TrialRequestController@mailToCourt')->name('trial_request_potential_dashboard.mail_court');
                Route::post('/updateticket/{ticket}/', 'Admin\TrialRequestController@updateTicket')->name('trial_request_potential_dashboard.update');
                Route::get('/view/{contact}', 'Admin\TrialRequestController@view')->name('trial_request_potential_dashboard.view');
                Route::put('/detail/update/{ticket}', 'Admin\TrialRequestController@updateTrialRequestTicketDetail')->name('trial_request_potential_dashboard.detail.update');
                Route::delete('/detail/delete/{ticket}', 'Admin\TrialRequestController@destroyticket')->name('trial_request_potential_dashboard.ticket.delete');
                // Route::patch('/client-status', 'Admin\TrialRequestController@clientStatus')->name('pay_ticket_potential.client-status');

                Route::get('/exporthistory/excel','Admin\TrialRequestController@exportHistory');
            });

            // ROUTE::Pay Ticket
            Route::group(['prefix' => 'pay_ticket/{type}'], function () {

                Route::get('/', 'Admin\PayTicketController@index')->name('pay_ticket_potential');
                Route::get('/datatable', 'Admin\PayTicketController@potentialDatatable')->name('pay_ticket_potential.datatable');
                Route::get('/view/{pay_ticket}', 'Admin\PayTicketController@view')->name('pay_ticket_potential.view');
                Route::patch('/status', 'Admin\PayTicketController@status')->name('pay_ticket_potential.status');
                Route::delete('/delete', 'Admin\PayTicketController@destroy')->name('pay_ticket_potential.delete');
            });
        });

        Route::prefix('callback_clients')->group(function () {

            // ROUTE::Trial Request
            Route::group(['prefix' => 'ticket_request/{type}'], function () {

                Route::get('/', 'Admin\TrialRequestController@index')->name('trial_request_callback_dashboard');
                Route::get('/datatable', 'Admin\TrialRequestController@callbackDatatable')->name('trial_request_callback_dashboard.datatable');
                Route::get('history', 'Admin\TrialRequestController@trialHistory')->name('trial_history_callback_dashboard');
                Route::get('history/datatable', 'Admin\TrialRequestController@historyDatatable')->name('trial_history_callback_dashboard.datatable');
                Route::get('/close/{contact}', 'Admin\TrialRequestController@closeTicket')->name('trial_request_callback_dashboard.close');
                Route::get('/edit/{contact}', 'Admin\TrialRequestController@editTicket')->name('trial_request_callback_dashboard.edit');
                Route::post('/ticketimageupdate/{ticket}', 'Admin\TrialRequestController@ticketImageUpdate')->name('trial_request_callback_dashboard.image_update');
                Route::get('/tickets/word_export/{ticket}', 'Admin\TrialRequestController@wordExport')->name('trial_request_callback_dashboard.word_export');
                Route::get('/tickets/send-email-pdf/{ticket}', 'Admin\TrialRequestController@mailToCourt')->name('trial_request_callback_dashboard.mail_court');
                Route::post('/updateticket/{ticket}/', 'Admin\TrialRequestController@updateTicket')->name('trial_request_callback_dashboard.update');
                Route::get('/view/{contact}', 'Admin\TrialRequestController@view')->name('trial_request_callback_dashboard.view');
                Route::put('/detail/update/{ticket}', 'Admin\TrialRequestController@updateTrialRequestTicketDetail')->name('trial_request_callback_dashboard.detail.update');
                Route::delete('/detail/delete/{ticket}', 'Admin\TrialRequestController@destroyticket')->name('trial_request_callback_dashboard.ticket.delete');
                // Route::patch('/client-status', 'Admin\TrialRequestController@clientStatus')->name('pay_ticket_callback.client-status');
                Route::get('/exporthistory/excel','Admin\TrialRequestController@exportHistory');
            });

            // ROUTE::Pay Ticket
            Route::group(['prefix' => 'pay_ticket/{type}'], function () {

                Route::get('/', 'Admin\PayTicketController@index')->name('pay_ticket_callback');
                Route::get('/datatable', 'Admin\PayTicketController@callbackDatatable')->name('pay_ticket_callback.datatable');
                Route::get('/view/{pay_ticket}', 'Admin\PayTicketController@view')->name('pay_ticket_callback.view');
                Route::patch('/status', 'Admin\PayTicketController@status')->name('pay_ticket_callback.status');
                Route::delete('/delete', 'Admin\PayTicketController@destroy')->name('pay_ticket_callback.delete');
            });
        });

        Route::get('send-disc/{ticket}', 'Admin\TrialRequestController@mailToDisc')->name('mailToDisc');
        Route::put('/sendmail/{ticket}','Admin\TrialRequestController@filedTicketMail')->name('filedticket');
    });
});

// Admin  Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['prefix' => '/'],function () {

    Route::get('','Web\SiteController@index')->name('main_page');

    // Pay Ticket Section
    Route::group(['prefix' => 'pay-ticket'],function () {

        Route::get('','Web\PayTicketController@payStep')->name('pay_step');

        // Traffic Ticket Section
        Route::group(['prefix' => '{type}'],function () {

            Route::get('','Web\TrafficController@trafficTicket')->name('traffic_step');
            Route::patch('step-1','Web\TrafficController@trafficStep1')->name('traffic_step_1');
            Route::patch('step-2','Web\TrafficController@trafficStep2')->name('traffic_step_2');
            Route::patch('step-3','Web\TrafficController@chargeCreditCard')->name('traffic_step_3');
            Route::patch('step-4','Web\TrafficController@trafficStep4')->name('traffic_step_4');

        });
    });

    // Trial Request Section
    Route::group(['prefix' => 'trial-request'],function () {

        Route::get('/','Web\TrialRequestController@trialRequest')->name('trial_request');
        Route::patch('step-1','Web\TrialRequestController@trialStep1')->name('trial_step_1');
        Route::post('step-2','Web\TrialRequestController@trialStep2')->name('trial_step_2');
        Route::patch('step-3','Web\TrialRequestController@trialStep3')->name('trial_step_3');
        Route::post('{type}/step-4','Web\TrialRequestController@trialStep4')->name('trial_step_4');
    });

     // Early Resolution
     Route::group(['prefix' => 'early-resolution'],function () {

        Route::get('/','Web\TrialRequestController@earlyRequest')->name('early_resolution');
        Route::patch('step-1','Web\TrialRequestController@trialStep1')->name('early_step_1');
        Route::post('step-2','Web\TrialRequestController@trialStep2')->name('early_step_2');
        Route::patch('step-3','Web\TrialRequestController@trialStep3')->name('early_step_3');
        Route::post('{type}/step-4','Web\TrialRequestController@trialStep4')->name('early_step_4');
    });
});
use App\Mail\CourtFiled;

Route::get('test/mail', function(){
    $ticket = \App\Models\Tickets::first();
    $subject='Trail request:'.$ticket->contact->first_name.$ticket->contact->last_name.' #1100-'.$ticket->ticket_no;
    $ticket=$ticket->ticket_no;

    Mail::to('shehryarhaider0316@gmail.com')->send(new CourtFiled($ticket,$subject));
});
Route::get('clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('storage:link');
    return ('done');
});
