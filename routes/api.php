<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () 
{
    Route::get('get-cities', 'API\v1\UserController@cities');
    Route::get('get-nationalities', 'API\v1\UserController@nationalities');
    Route::post('sign-up', 'API\v1\AuthController@signUp');
    Route::post('sign-up/verify-code', 'API\v1\AuthController@signUpVerifyCode');
    Route::post('login', 'API\v1\AuthController@login');
    Route::post('resend-otp', 'API\v1\AuthController@resendOTP');
    Route::post('forget-password', 'API\v1\AuthController@forgetPassword');
    Route::post('reset-password/verify-code', 'API\v1\AuthController@verifyCode');
    Route::post('verify-email', 'API\v1\AuthController@verifyEmail');
    Route::post('reset-password/reset', 'API\v1\AuthController@resetPassword');
    
    //Categories
    Route::prefix('categories')->group(function () {
        Route::get('get', 'API\v1\CategoryController@getAll');
    });
    
    //Playground
    Route::prefix('playground')->group(function () {
        Route::get('get', 'API\v1\PlaygroundController@getAll');
        Route::get('getDetail', 'API\v1\PlaygroundController@getDetail');
        Route::post('getBySearch', 'API\v1\PlaygroundController@getBySearch');
        // Route::get('get-booking','API\v1\PlaygroundController@getBooking');
        Route::get('near-you', 'API\v1\PlaygroundController@playgroundsNearYou');

        Route::get('checking-booking', 'API\v1\PlaygroundController@checkingBooking');

    });

    //Tournaments
    Route::prefix('tournament')->group(function () {
        Route::get('get', 'API\v1\TournamentController@getAll');
        Route::get('getDetail', 'API\v1\TournamentController@getDetail');
        Route::post('getByPlayground', 'API\v1\TournamentController@getByPlayground');
    });

    //Trainers
    Route::prefix('trainer')->group(function () {
        Route::get('get', 'API\v1\TrainerController@getAll');
        Route::get('getDetail', 'API\v1\TrainerController@getDetail');
        Route::post('review/add','API\v1\TrainerController@addReviw');

    Route::get('checking-booking', 'API\v1\TrainerController@checkingBooking');
    });

    
});
Route::middleware('auth:api')->prefix('v1')->group(function () 
{
    //User Interest
    Route::post('user-interest', 'API\v1\UserController@addRemoveUserInterest');
    Route::get('get-user-interest', 'API\v1\UserController@getUserInterest');
    // Route::post('remove-user-interest', 'API\v1\UserController@removeUserInterest');

    //General Routes
    Route::post('logout', 'API\v1\AuthController@logout');
    Route::post('password-change', 'API\v1\UserController@passwordChange');

    //OTP
    Route::post('otp/get', 'API\v1\OTPController@newOTP');
    Route::post('otp/verify', 'API\v1\OTPController@verifyOTP');

    Route::prefix('tournament')->group(function () {
        Route::post('register', 'API\v1\TournamentController@register');

    });

    Route::group(['prefix' => 'stories'], function () {
        Route::post('add', 'API\v1\UserController@addStories');
        Route::get('my', 'API\v1\UserController@myStories');
        Route::get('friends', 'API\v1\UserController@friendStories');
        Route::post('delete', 'API\v1\UserController@deleteStories');
    });

    Route::group(['prefix' => 'chat'], function () {
        Route::post('media', 'API\v1\UserController@addChatMedia');
    });

// Trainer
Route::group(['prefix' => 'trainer'], function () {
    // Route::post('profile/update', 'API\v1\TrainerController@profileUpdate');
    Route::get('favourite/get', 'API\v1\TrainerController@getFavourite');
    Route::post('favourite/add', 'API\v1\TrainerController@addInFavourite');
    Route::post('favourite/remove', 'API\v1\TrainerController@removeInFavourite');
    Route::post('rating', 'API\v1\RatingController@rateByTrainer');
    Route::post('confirm-booking', 'API\v1\TrainerController@confirmBooking');
    Route::post('profile/update', 'API\v1\TrainerController@profileUpdate');
    Route::get('request', 'API\v1\TrainerController@getPendingRequest');
    Route::get('request-notification-screen', 'API\v1\TrainerController@getPendingRequestNotification');
    Route::post('request/status-update', 'API\v1\TrainerController@updateRequestStatus');
    Route::get('coming-sessions', 'API\v1\TrainerController@getComingSession');
    Route::get('available-at-booking-time', 'API\v1\TrainerController@getAvailableTrainers');
    Route::get('up-coming-sessions-notifications', 'API\v1\TrainerController@getUpComingSessionNotification');
    Route::get('get-trainer-checkout-detail', 'API\v1\TrainerController@getCheckoutDetail');
    
});

// Playground
Route::group(['prefix' => 'playground'], function () {
    Route::get('favourite/get', 'API\v1\PlaygroundController@getFavourite');
    Route::post('favourite/add', 'API\v1\PlaygroundController@addInFavourite');
    Route::post('favourite/remove', 'API\v1\PlaygroundController@removeInFavourite');
    Route::post('confirm-booking', 'API\v1\PlaygroundController@confirmBooking');

});
// Auth
Route::get('profile', 'API\v1\UserController@profile');
Route::get('my-booked-playground', 'API\v1\UserController@myBookedPlayground');
Route::get('my-activity', 'API\v1\UserController@userActivity');
Route::get('my-booking-history', 'API\v1\UserController@myBookingHistory');
Route::get('my-training-history', 'API\v1\UserController@myTrainingHistory');
Route::get('my-notifications', 'API\v1\UserController@myNotifications');
Route::get('my-trainer-request-approve-notifications', 'API\v1\UserController@trainerRequestApproveNotification');
Route::get('my-upcoming-games-notifications', 'API\v1\UserController@upcomingGamesNotification');
  
 // Friend Routes
 Route::group(['prefix' => 'friend'], function () {
    Route::get('close-friend','API\v1\FriendController@closeFriend');
    Route::post('add/close-friend','API\v1\FriendController@addCloseFriend');
    Route::post('remove/close-friend','API\v1\FriendController@removeCloseFriend');
    Route::post('remove/friend','API\v1\FriendController@removeFriend');
    Route::get('my/{type?}', 'API\v1\FriendController@my');
    Route::get('request/pending', 'API\v1\FriendController@pendingRequest');
    Route::post('request/status-update', 'API\v1\FriendController@updayePendingRequest');
    Route::get('my-top-friends', 'API\v1\FriendController@topFriends');
});
    //Profile Routes
    Route::post('update-personal-detail', 'API\v1\UserController@updatePersonalDetail');
    Route::get('profile', 'API\v1\UserController@profile');
    Route::post('profile/update', 'API\v1\UserController@profileUpdate');

    //Transaction Routes
    Route::prefix('transaction')->group(function () {
        Route::post('add', 'API\v1\TransactionController@add');
        Route::get('get', 'API\v1\TransactionController@getAll');
        Route::post('getByType', 'API\v1\TransactionController@getByType');
    });

    // User Addesses
    Route::prefix('user/address')->group(function () {
        Route::post('add', 'API\v1\UserController@addUserAddress');
        Route::get('get', 'API\v1\UserController@getAllUserAddress');
    });
    Route::get('all/users','API\v1\UserController@allUser');
    Route::post('friend-request/send','API\v1\UserController@friendRequestSend');
    
});