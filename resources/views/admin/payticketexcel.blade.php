<table class="table">
    <thead>
    <tr>

        <th scope="col">Card Holder Name</th>
        <th scope="col">Email</th>
        <th scope="col">Postal Code</th>
        <th scope="col">Offence Date</th>
        <th scope="col">Icon Code</th>
        <th scope="col">Ticket Numbers</th>
        <th scope="col">Total Amount</th>
        <th scope="col">Email Send Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($details as $detail)
    <tr>
        <td scope="col">{{$detail->card_holder_name}}</td>
        <td scope="col">{{$detail->email}}</td>
        <td scope="col">{{$detail->postal_code}}</td>
        <td scope="col">{{$detail->offence_date}}</td>
        <td scope="col">{{$detail->icon_code}}</td>
        <td scope="col">{{$detail->ticket_no}}</td>
        <td scope="col">{{$detail->total_amount}}</td>
        <td scope="col">{{$detail->emaildateticket}}</td>
    </tr>
    @endforeach
    </tbody>
</table>
