<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>Admin Register</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/dash-assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/dash-assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/dash-assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/dash-assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{url('')}}/dash-assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('')}}/dash-assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('')}}/dash-assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{url('')}}/dash-assets/pages/css/login-2.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <style>
            form.register-form {
                background: #ffffffe8;
                width: 510px !important;
                padding: 77px 43px !important;
                color: black !important;
                margin-top: 20px;
            }
            .login .content {
                width: 100%;
                margin: 130px auto;
            }

            .login .content .form-title {
                font-weight: 300;
                margin-bottom: 25px;
                color: #000000ba;
                font-weight: bolder !important;
            }
            .login .content .form-subtitle {
                font-weight: 300;
                margin-bottom: 25px;
                font-size: 16px;
                color: #00000085;
                font-weight: bolder !important;
            }
            .login .content .mt-checkbox {
                color: #000000ba !important;
            }
            .hint{
                color: #000000c7 !important;
                text-align: left !important;
            }
        </style>
    </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <center>
            <div class="content">
                @if (session('error'))
                    <p class="alert alert-danger">{{session('error')}}</p>
                @endif
                    <!-- BEGIN REGISTRATION FORM -->
                <form class="register-form" action="{{route('register')}}" method="post" style="display: block">
                    <h2 style="margin-top: 0px;margin-bottom:25px; text-align:left;">Admin SignUp</h2>

                    @csrf
                    <p class="hint"> Enter your personal details below: </p>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Full Name</label>
                        <input class="form-control placeholder-no-fix" type="text" required placeholder="Full Name" name="name" value="{{old('name') ?? null}}"/> </div>
                        <span class="text-danger">{{$errors->first('name') ?? null}}</span>

                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Email</label>
                        <input class="form-control placeholder-no-fix" type="email" placeholder="Email" name="email" value="{{old('email') ?? null}}" />
                        <span class="text-danger">{{$errors->first('email') ?? null}}</span>

                    </div>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Password</label>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" />
                        <span class="text-danger">{{$errors->first('password') ?? null}}</span>

                    </div>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation" required/>
                        <span class="text-danger">{{$errors->first('password_confirmation') ?? null}}</span>

                    </div>
                    <div class="form-actions">
                        <a href="{{route('login')}}" style="float: left"><button type="button" class="btn btn-default">Back</button></a>
                        <button type="submit" id="register-submit-btn" class="btn red uppercase pull-right">Submit</button>
                    </div>
                </form>
                <!-- END REGISTRATION FORM -->
            </div>
        </center>
        <!-- END LOGIN -->
        <!--[if lt IE 9]>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{url('')}}/dash-assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/dash-assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/dash-assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/dash-assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/dash-assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/dash-assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{url('')}}/dash-assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{url('')}}/dash-assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{url('')}}/dash-assets/pages/scripts/login.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>

</html>
