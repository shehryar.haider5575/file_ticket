@extends('admin.layouts.master')
@section('title', 'Dashboard')

@section('top-styles')
<style>
    .desc {
        font-weight: bolder !important;
    }
</style>
@endsection
@section('content')
@section('breadcrumb')
   <ul class="page-breadcrumb">
        <li>
            <a href="{{route('home')}}">ADMIN</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
        <span>Dashboard</span>
        </li>
    </ul>
@endsection
        <!-- BEGIN DASHBOARD STATS 1-->
        <div class="row">
        
            <hr>
                <h1 class="text-center">Welcome To Dashboard</h1>
            <hr>
        </div>
        <div class="clearfix"></div>
       
<!-- END QUICK SIDEBAR -->
@endsection