@extends('admin.layouts.master')
@section('title', __('PayTicket History'))

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.css">

<!-- Plugins css-->
<link href="{{url('')}}/dash-assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- END PAGE LEVEL PLUGINS -->
<style>
    a.btn.btn-secondary {
        background: #dc3535bf;
        border: 1px solid white;
    }
    h2#swal2-title {
        font-size: 25px;
    }
    div#swal2-content {
        font-size: 17px;
    }
    .swal2-icon.swal2-warning, .swal2-success-ring {
        border-radius: 50% !important;
        font-size: 11px;
    }
    .swal2-popup.swal2-modal.swal2-icon-warning.swal2-show {
        height: 280px;
        width: 36em;
    }
    table.dataTable.no-footer
    {
        border-bottom:inherit !important;
    }

</style>

@endsection

@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="{{route('home')}}">{{ __('ADMIN')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>{{ __('PayTicket History')}}</span>
    </li>
</ul>
@endsection
<!-- BEGIN Category STATS 1-->
<!-- BEGIN Products STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-wrench"> </i> {{ __('PayTicket History')}}
                        <a id="excel_export" style="color: white; margin-left: 20px; position: relative; right: 135px;" class="float-sm-right" href="{{route('excel_export_payticket',[$type,'10'])}}">
                            <i class="fa fa-file"></i>
                        </a>
                        <button onclick="window.history.back(1)" type="button"
                        class="btn btn-primary btn-md float-sm-right" style="background:#00bcd4"
                        >{{ __('Go Back')}}</button>
                        {{-- <a href="{{route('trial_request_dashboard.create')}}">
                            <button type="button" class="btn btn-block btn-primary btn-md float-sm-right" >{{ __('Add Category')}}</button>
                        </a> --}}
                    </div>
                    <div class="panel-body">
                        <div class="custom_datatable">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                 </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th class="all">{{ __("S.No")}}</th>
                                                <th class="all">{{ __('Icon Code')}}</th>
                                                <th class="all">{{ __("Card Holder Name")}}</th>
                                                <th class="all">{{ __("Date Submitted")}}</th>
                                                <th class="all">{{ __("Offense Date")}}</th>
                                                <th class="all">{{ __("Court Email Date")}}</th>
                                                <th class="all">{{ __("View Ticket Information")}}</th>
                                                <th class="all">{{ __('Edit File')}}</th>
                                                <th class="all">{{ __('Close File')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection

@section('rightsidebar')
@parent
@endsection

@section('page-scripts')
{{-- SweetAlert2 --}}
<script src="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>

<script src="{{url('')}}/dash-assets/plugins/switchery/js/switchery.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/custom/jquery.nicescroll.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('')}}/dash-assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
{{-- PDF MAKE --}}
@endsection

@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#sample_1').DataTable({
            // retrieve: true,
            destroy: true,
            processing: true,
            serverSide: true,
            info: true,
            lengthChange: true,
            bPaginate: true,
            bLengthChange: true,
            bFilter: true,
            bInfo: true,
            bAutoWidth: true,
            
            ajax: '{{route("payticket_history_dashboard.datatable",$type)}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "icon_code",
                    "defaultContent": ""
                },
                {
                    "data": "card_holder_name",
                    "defaultContent": ""
                },
                // {
                //     "data": "email",
                //     "defaultContent": ""
                // },
                {
                    "data": "created_at",
                    "defaultContent": ""
                },
                {
                    "data": "offence_date",
                    "defaultContent": ""
                },
                {
                    "data": "emaildate",
                    "defaultContent": "NULL"
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "id",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return data;
                    },
                },

                // {
                //     "targets": -6,
                //     "render": function (data, type, row, meta) {
                //        return moment(data).format('MMMM Do YYYY, h:mm a');
                //     },
                // },
                // {
                //     "targets": -5,
                //     "render": function (data, type, row, meta) {
                //        return moment(data).format('MMMM Do YYYY');
                //     },
                // },
                {
                    "targets": -3,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("pay_ticket.view",["Early_Request",":id"])}}';
                        edit = edit.replace(':id', row.id);
                        return `
                            <a href="`+edit+`" class="btn btn-light-theme btn-block waves-effect waves-light text-primary p-2" style="padding-right:6px;">
                                    View
                            </a>`;
                    },
                },
                {
                    "targets": -2,
                    "render": function (data, type, row, meta) {
                        var edit = '{{route("pay_ticket.edit",["Early_Request",":id"])}}';
                        edit = edit.replace(':id', row.id);
                        return `
                            <a href="`+edit+`" class="p-2" style="padding-right:6px;">
                                <i class="fa fa-edit"></i>
                            </a>`;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var close = '{{route("pay_ticket_dashboard.close",["Early_Request",":id"])}}';
                        close = close.replace(':id', row.id);

                        if(row.length > 0 && row.completed == 0){
                            return `
                                <a href="`+close+`" class=" text-danger p-2" style="padding-right:6px;">
                                    Close File
                                </a>`;
                        }else{
                            return `<a class=" text-danger p-2" style="padding-right:6px;    text-decoration: none;font-weight: bold;">
                                    Closed
                                </a>`;
                        }
                    },
                },
            ],

            "drawCallback": function (settings) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                if (elems) {
                    elems.forEach(function (html) {
                        var switchery = new Switchery(html, {
                            color: '#36c6d3',
                            secondaryColor: '#dfdfdf',
                            jackColor: '#fff',
                            jackSecondaryColor: null,
                            className: 'switchery',
                            disabled: false,
                            disabledOpacity: 0.5,
                            speed: '0.1s',
                            size: 'small'
                        });

                    });
                }
            },
            //scrollX:true,
        });
        $('#advanceSearch').submit(function (e) {
            e.preventDefault();
            table.columns(4).search($('input[name="email"]').val());
            table.columns(1).search($('input[name="icon_code"]').val());
             table.draw();
        });

        $(".custom_datatable #DataTable_wrapper .row:nth-child(2) .col-sm-12").niceScroll();

        $('select.input-inline').change(function(){
            var entries = $(this).val();
            var url = "{{route('excel_export',['Early_Request',':entries'])}}";
            url = url.replace(':entries', entries);
            console.log(url);
            $('#excel_export').attr('href',url);
        });
    });
</script>
@if(session('error'))
    <script>
        Swal.fire(
            'failed!',
            "{{session('error')}}",
            'error'
        )

    </script>
@endif
@if(session('fileclose'))
    <script>
        Swal.fire(
            'Success!',
            'File Closed.',
            'success'
        )

    </script>
@endif
@if(session('emaildidntsent'))
<script>
    Swal.fire(
        'failed!',
        'Mail Sending Failed.',
        'error'
    )

</script>
@endif
@if(session('success'))
    <script>
        Swal.fire(
            'Success!',
            'File Record has Updated!',
            'success'
        )

    </script>
@endif
@if(session('emaildate'))
<script>
   Swal.fire(
        'failed!',
        'Mail Sending Failed.',
        'error'
    )

</script>
@endif
@if(session('emailcomments'))
<script>
   Swal.fire(
        'Failed!',
        'Comments is Required.',
        'error'
    )
</script>
@endif
@if(session('emailsent'))
<script>
    Swal.fire(
        'Success!',
        'Mail Sent.',
        'success'
    )

</script>
@endif
@if(session('emailattachment'))
<script>
    Swal.fire(
        'Failed!',
        'Mail Sending Failed.',
        'error'
    )
</script>
@endif
@endsection
