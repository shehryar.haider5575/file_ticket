@extends('admin.layouts.master')
@section('title', $type)

@section('top-styles')
<link href="{{url('')}}/dash-assets/global/plugins/summernote/summernote-bs4.css" rel="stylesheet">

<style>
    .dataTables_wrapper .dt-buttons, div#sample_1_length, div#sample_1_paginate{
        display: none
    }
    .btn-block {
        display: contents;
        width: 36%;
    }
    .card.comment-area {
        width: 100%;
        min-height: 125px;
        border: 1px solid #80808040;
        padding: 20px;
    }
    .note-editing-area{
        min-height: 200px !important;
    }
    .note-editable.card-block{
        min-height: 200px !important;
    }
</style>
@endsection
@section('content')

@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="{{route('home')}}">{{ __('ADMIN')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <a href="{{route('pay_ticket',$type)}}">{{ $type}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>{{ __('Ticket Details')}}</span>
    </li>
</ul>
@endsection
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="panel panel-primary">
                    @if (session('comment_update'))
                        <p class="alert alert-success">{{session('comment_update')}}</p>
                    @endif
                    <div class="panel-heading">
                        <i class="fa fa-wrench"> </i> {{ __('Ticket Details')}}
                        <button onclick="window.history.back(1)" type="button"
                            class="btn btn-primary btn-md float-sm-right" style="background:#00bcd4"
                            >{{ __('Go Back')}}</button>
                        <label style="font-size: 15px;" class="float-sm-right" for="potential_client">
                            Add to Potential Client
                            <input class="sd " data-id="{{$payticket->id}}" id="potential_client" style="margin-right: 25px;" type="checkbox" {{$payticket->potential_status == 1 ? 'checked' : null}} value="1">
                        </label>

                        <label style="font-size: 15px;" class="float-sm-right" for="callback_client">
                            Add to Callback Client
                            <input class="sd " data-id="{{$payticket->id}}" id="callback_client" style="margin-right: 25px;" type="checkbox" {{$payticket->client_status == 1 ? 'checked' : null}} value="2">
                        </label>
                    </div>
                    <div class="panel-body">

                        @if($payticket)
                        <div class="ticket_detail row">
                                <h4> Defendants Contact & Address Details</h4>
                                <div class="col-md-3">
                                    <h5> <strong>Card Holder Name:</strong>  </h5>
                                </div>
                                <div class="col-md-3">
                                    <h5>{{$payticket->card_holder_name}}</h5>
                                </div>
                                <div class="col-md-3">
                                    <h5> <strong>Email:</strong>  </h5>
                                </div>
                                <div class="col-md-3">
                                    <h5>{{$payticket->email}}</h5>
                                </div>
                                <div class="col-md-3">
                                    <h5> <strong>Icon Code:</strong>  </h5>
                                </div>
                                <div class="col-md-3">
                                    <h5>{{$payticket->icon_code}}</h5>
                                </div>
                                <div class="col-md-3">
                                    <h5><strong>Postal Code:</strong></h5>
                                </div>
                                <div class="col-md-3">
                                    <h5>{{$payticket->postal_code}}</h5>
                                </div>
                                <div class="col-md-3">
                                    <h5><strong>Total Amount:</strong></h5>
                                </div>
                                <div class="col-md-3">
                                    <h5>
                                        ${{$payticket->total_amount}}
                                    </h5>
                                </div>
                            </div>
                        @else
                            <div class="col-4">
                                No Data Available!
                            </div>
                        @endif
                    <div class="col-md-12">
                        <table class="table table-striped table-hover dt-responsive" id="sample_1">
                            <thead>
                            <tr>
                                <th>Id</th>
                                {{-- <th>Attached Image</th> --}}
                                <th>Ticket No </th>
                                <th>Comments</th>
                                {{-- <th>Personal Comments</th> --}}
                                <th>Date Filed</th>
                                <th>Completed</th>
                                <th>Email Date</th>
                                <th>Ticket Image</th>
                                {{-- <th>Download Form</th> --}}
                                {{-- <th>Mail to Court</th> --}}
                                {{-- <th>Delete</th> --}}
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                    <td>1</td>
                                    {{-- <td>
                                        <a  id="{{$payticket->id}}" data-toggle="modal" data-target="#exampleModalCenter" data-id="attachimg{{$payticket->id}}" data-source="{{url('')}}/uploads/file/{{$payticket->images_name}}">
                                            <div class="img-div"><img id="attachimg{{$payticket->id}}" style="height: 50px;width: 50px" src="{{url('')}}/uploads/file/{{$payticket->images_name}}" alt="{{$payticket->images_name}}"> </div>
                                        </a>
                                    </td> --}}
                                    <td style="display: inline-flex !important;">{{$payticket->ticket_no}}
                                        <button class="btn btn-light-theme btn-block waves-effect waves-light" type="button"   data-toggle="modal" data-target="#exampleModal" data-dd="{{$payticket->datefiled}}" data-whatever="{{$payticket->id}}" data-icon="{{$payticket->icon_id}}" data-comments="{{$payticket->comments}}" data-pcomments="{{$payticket->personal_comments}}" data-completed="{{$payticket->completed}}" ><i class="icon-plus" ></i>
                                        </button>
                                    </td>
                                    <td>{{$payticket->comments ?? 'no comments'}}</td>
                                    <td>{{$payticket->datefiled ?? '0000-00-00'}}</td>
                                    <td>{{$payticket->completed==1? 'Yes':'No'}}</td>
                                    <td>{{$payticket->emaildateticket ?? '0000-00-00'}}</td>
                                    <td>
                                        <a  id="{{$payticket->id}}" data-toggle="modal" data-target="#exampleModalCenter" data-id="attachimg{{$payticket->id}}" data-source="{{url('')}}/uploads/email/{{$payticket->attached_image}}">
                                            <div class="img-div"><img id="attachimg{{$payticket->id}}" style="height: 50px;width: 50px" src="{{url('')}}/uploads/file/{{$payticket->attached_image}}" alt="{{$payticket->attached_image}}"> </div>
                                        </a>
                                    </td>
                                    {{-- <td>{{$payticket->personal_comments ?? 'no personal comments'}}</td> --}}
                                    {{-- <td>{{$payticket->offence_date ?? '0000-00-00'}}</td>
                                    <td>{{$payticket->completed==1? 'Yes':'No'}}</td> --}}
                                    {{-- <td><a href="{{route('pay_ticket.word_export',["Trial_Request",$payticket->id])}}">Download</a></td> --}}
                                    {{-- <td><a href="{{route('pay_ticket.mail_court',["Trial_Request",$payticket->id])}}">Send</a></td> --}}
                                    {{-- <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" data-whatever="{{route('pay_ticket.ticket.delete',["Trial_Request",$payticket->id])}}">Delete</button></td> --}}

                                </tr>
                            </tbody>
                        </table>
                        <div class="col-md-12">
                        </br></br>
                        <form action="{{route('update_comments')}}" method="post">
                            @csrf
                            <div class="form-group client">
                                <h5>Personal Comments</h5>
                                <textarea id="summernote_1" name="personal_comments" class="form-control" placeholder="Enter Comments">{{$payticket->personal_comments ?? null}}</textarea>
                                <input type="hidden" name="type" value="payticket">
                                <input type="hidden" name="id" value="{{$payticket->id}}">

                              {{-- <div name="summernote" height="300px" id="summernote_1" class="summernote_2 summernote_1">{{$payticket->personal_comments ?? null}} </div> --}}
                                </br>
                              <button type="submit" id="update_commments" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                        </div>
                        {{-- <div class="card comment-area">
                            <h5>Ticket # {{$payticket->ticket_no}}</h5>
                            {{$payticket->personal_comments ?? 'no personal comments'}}
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
</div>
<!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Ticket Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img id="srcimg" src="" style="height: 400px;width: 450px"  alt="ticket.jpeg">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- MODAL--}}

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Ticket Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form  method="post" id="update-form" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="date" class="col-form-label">Date We Filed Ticket:</label>
                            <input type="date"  class="form-control" id="date" name="date">
                            Completion Date: <span id="datedisplay"></span>
                        </div>
                        <div class="form-group">
                            <label for="comments" class="col-form-label">Comments:</label>
                            <textarea class="form-control" id="comments" name="comments"></textarea>
                        </div>
                        {{-- <div class="form-group">
                            <label for="pcomments" class="col-form-label">Personal Comments:</label>
                            <textarea class="form-control" id="pcomments" name="pcomments"></textarea>
                        </div> --}}
                        <div class="form-group">
                            <label for="completed" class="col-form-label">Completed:</label>
                            <select name="completed" id="completed">
                                <option value="0" selected>No</option>
                                <option value="1">Yes</option>

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="images" class="col-form-label">Upload Image For Mail</label>
                            <input  style="overflow: hidden" type="file" capture="camera" accept=".jpg, .jpeg, .png" class="form-control" id="images" name="images">
                        </div>

                    </div>
                    <div class="modal-footer">
                        {{-- <a class="btn btn-primary" href="{{route('mailToDisc',$payticket->id)}}">Disc Request </a> --}}
                        {{-- <a class="btn btn-primary" href="{{route('pay_ticket.mail_court',["Trial_Request",$payticket->id])}}">Mail To Court </a> --}}
                        <!--<button class="btn btn-primary"  onclick="window.location.href='{{url('/send-email-pdf/'.$payticket->id)}}'">Mail To Court </button>-->
                        <button type="submit" class="btn btn-primary"  id="sendmail">Send Mail</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        {{-- <button type="submit" class="btn btn-primary" id="updateticketstats">Update</button> --}}
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" data-whatever="{{route('pay_ticket.ticket.delete',["Trial_Request",$payticket->id])}}">Delete</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
    {{----}}
    {{--Delete Modal--}}

    <!-- Modal -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirmation!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are You Sure You Want To Delete?
                </div>
                <div class="modal-footer">
                    <form action="" method="POST" id="delete-form">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@section('page-scripts')
{{-- SweetAlert2 --}}
<script src="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>

<script src="{{url('')}}/dash-assets/plugins/switchery/js/switchery.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/custom/jquery.nicescroll.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('')}}/dash-assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="{{url('')}}/dash-assets/global/plugins/summernote/summernote-bs4.min.js" type="text/javascript">
</script>
{{-- PDF MAKE --}}
@endsection
@section('custom-script')
    <script>
        $(document).ready( function () {
            $('#potential_client').change(function () {
                var $this = $(this);
                var id = $this.data('id');
                var status = this.checked;

                if (status) {
                    status = 1;
                } else {
                    status = 0;
                }
                axios
                    .post('{{route("pay_ticket_type.client-status",$type)}}', {
                        _token: '{{csrf_token()}}',
                        _method: 'patch',
                        id: id,
                        status: status,
                        type: "potential",

                    })
                    .then(function (responsive) {
                        console.log(responsive);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            });
            $('#callback_client').change(function () {
                var $this = $(this);
                var id = $this.data('id');
                var status = this.checked;

                if (status) {
                    status = 1;
                } else {
                    status = 0;
                }
                axios
                    .post('{{route("pay_ticket_type.client-status",$type)}}', {
                        _token: '{{csrf_token()}}',
                        _method: 'patch',
                        id: id,
                        status: status,
                        type: "callback",

                    })
                    .then(function (responsive) {
                        console.log(responsive);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            });
            $('#sample_1').DataTable();


            $('#exampleModalCenter').on('show.bs.modal', function (event) {
                let button = $(event.relatedTarget) // Button that triggered the modal
                let id = button.data('id') // Extract info from data-* attributes
                let src = button.data('source') // Extract info from data-* attributes

                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                let modal = $(this);

                modal.find('#srcimg').attr('src', src);

            });
            $('#exampleModal').on('show.bs.modal', function (event) {
                let button = $(event.relatedTarget) // Button that triggered the modal
                let id = button.data('whatever') // Extract info from data-* attributes
                var update = '{{route("pay_ticket.detail.update",["Trial_Request",":id"])}}';
                update = update.replace(':id', id);
                var sendmailroute = '{{route("filedticket",[":id"])}}';
                sendmailroute = sendmailroute.replace(':id', id);
                let comments = button.data('comments') // Extract info from data-* attributes
                let pcomments = button.data('pcomments')
                let date = button.data('dd') // Extract info from data-* attributes
                let completed = button.data('completed') // Extract info from data-* attributes
                let modal = $(this);
                modal.find('#contactid').val(id);
                $('#datedisplay').html(date);
                modal.find('#comments').text(comments);
                modal.find('#pcomments').text(pcomments);

                modal.find('#updateticketstats').attr('formaction', update);
                modal.find('#sendmail').attr('formaction', sendmailroute);
                if(completed==1) {
                    modal.find('#completed option[value="1"]').attr('selected','selected');
                }
            });
            $('#exampleModal2').on('show.bs.modal', function (event) {
                let button = $(event.relatedTarget) // Button that triggered the modal
                let action = button.data('whatever') // Extract info from data-* attributes
                let modal = $(this);
                modal.find('#delete-form').attr('action', action);

            });
        });

        $('#summernote_1').summernote();
    </script>
@endsection
@if(session('emailsent'))
<script>
    Swal.fire(
        'Success!',
        'Mail Sent.',
        'success'
    )

</script>
@endif
@endsection
