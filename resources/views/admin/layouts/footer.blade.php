   <!-- BEGIN FOOTER -->
   <div class="page-footer">
       <div class="page-footer-inner"> 2021 &copy; 
           {{-- Developed By
           <a target="_blank" href="http://keenthemes.com">Shehryar Haider</a>  --}}
       </div>
       <div class="scroll-to-top">
           <i class="icon-arrow-up"></i>
       </div>
   </div>
   <!-- END FOOTER -->
   <!-- BEGIN QUICK NAV -->
  
   <div class="quick-nav-overlay"></div>
   <!-- END QUICK NAV -->