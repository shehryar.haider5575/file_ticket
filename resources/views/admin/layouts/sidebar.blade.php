 <!-- BEGIN SIDEBAR -->
 <div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <!-- END SIDEBAR TOGGLER BUTTON -->
           <!-- END RESPONSIVE QUICK SEARCH FORM -->
            <li class="nav-item start {{Route::currentRouteName() == 'home' ? 'active open' : null}}">
                <a href="{{route('home')}}" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="{{Route::currentRouteName() == 'home' ? 'selected' : null}}"></span>
                </a>
            </li>
            <li class="nav-item start {{Route::currentRouteName() == 'profile.setting' ? 'active open' : null}}">
                <a href="{{route('profile.setting')}}" class="nav-link nav-toggle">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Profile Update</span>
                    <span class="{{Route::currentRouteName() == 'profile.setting' ? 'selected' : null}}"></span>
                </a>
            </li>
            <li class="nav-item start {{Route::currentRouteName() == 'trial_request_dashboard' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_dashboard.view' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_dashboard.edit' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_history_dashboard' && request()->route()->parameters['type'] == "Trial_Request" ? 'active open' : null}}">
                <a href="{{route('trial_request_dashboard','Trial_Request')}}" class="nav-link nav-toggle">
                    <i class="fa fa-wrench"></i>
                    <span class="title">TRIAL REQUEST</span>
                    <span class="{{Route::currentRouteName() == 'trial_request_dashboard' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_dashboard.view' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_dashboard.edit' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_history_dashboard' && request()->route()->parameters['type'] == "Trial_Request" ? 'selected' : null}}"></span>
                </a>
            </li>
            <li class="nav-item start {{Route::currentRouteName() == 'trial_request_dashboard' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_dashboard.view' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_dashboard.edit' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_history_dashboard' && request()->route()->parameters['type'] == "Early_Request" ? 'active open' : null}}">
                <a href="{{route('trial_request_dashboard','Early_Request')}}" class="nav-link nav-toggle">
                    <i class="fa fa-comments"></i>
                    <span class="title">EARLY RESOLUTION</span>
                    <span class="{{Route::currentRouteName() == 'trial_request_dashboard' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_dashboard.view' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_dashboard.edit' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_history_dashboard' && request()->route()->parameters['type'] == "Early_Request" ? 'selected' : null}}"></span>
                </a>
            </li>
            <li class="nav-item {{Route::currentRouteName() == 'pay_ticket'  || Route::currentRouteName() == 'payticket_history_dashboard'  ? 'active open' : null}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">{{ __('Pay Tickets') }}</span>
                    <span class="{{Route::currentRouteName() == 'pay_ticket' ? 'selected' : 'arrow'}}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket' && request()->route()->parameters['type'] == "traffic" ? 'active open' : null}}">
                        <a href="{{route('pay_ticket','traffic')}}" class="nav-link ">
                            <span class="title">{{ __('Traffic Ticket') }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket' && request()->route()->parameters['type'] == "parking" ? 'active open' : null}}">
                        <a href="{{route('pay_ticket','parking')}}" class="nav-link ">
                            <span class="title">{{ __('Parking Ticket') }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket' && request()->route()->parameters['type'] == "speed_camera" ? 'active open' : null}}">
                        <a href="{{route('pay_ticket','speed_camera')}}" class="nav-link ">
                            <span class="title">{{ __('Speed Camera') }}</span>
                        </a>
                    </li>
                    <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket' && request()->route()->parameters['type'] == "red_light_camera" ? 'active open' : null}}">
                        <a href="{{route('pay_ticket','red_light_camera')}}" class="nav-link ">
                            <span class="title">{{ __('Red Light Camera') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item {{Route::currentRouteName() == 'trial_request_callback_dashboard' || Route::currentRouteName() == 'trial_request_callback_dashboard.view' || Route::currentRouteName() == 'trial_request_callback_dashboard.edit' || Route::currentRouteName() == 'trial_history_callback_dashboard' || Route::currentRouteName() == 'pay_ticket_callback' ? 'active open' : null}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">{{ __('CallBack Clients') }}</span>
                    <span class="{{Route::currentRouteName() == 'pay_ticket_callback' ? 'selected' : 'arrow'}}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start {{Route::currentRouteName() == 'trial_request_callback_dashboard' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_callback_dashboard.view' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_callback_dashboard.edit' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_history_callback_dashboard' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'pay_ticket_callback' ? 'active open' : null}}">
                        <a href="{{route('trial_request_callback_dashboard','Trial_Request')}}" class="nav-link nav-toggle">
                            <i class="fa fa-wrench"></i>
                            <span class="title">TRIAL REQUEST</span>
                            <span class="{{Route::currentRouteName() == 'trial_request_callback_dashboard' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_callback_dashboard.view' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_callback_dashboard.edit' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_history_callback_dashboard' && request()->route()->parameters['type'] == "Trial_Request" ? 'selected' : null}}"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{Route::currentRouteName() == 'trial_request_callback_dashboard' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_callback_dashboard.view' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_callback_dashboard.edit' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_history_callback_dashboard' && request()->route()->parameters['type'] == "Early_Request" ? 'active open' : null}}">
                        <a href="{{route('trial_request_callback_dashboard','Early_Request')}}" class="nav-link nav-toggle">
                            <i class="fa fa-comments"></i>
                            <span class="title">EARLY RESOLUTION</span>
                            <span class="{{Route::currentRouteName() == 'trial_request_callback_dashboard' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_callback_dashboard.view' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_callback_dashboard.edit' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_history_callback_dashboard' && request()->route()->parameters['type'] == "Early_Request" ? 'selected' : null}}"></span>
                        </a>
                    </li>
                    <li class="nav-item {{Route::currentRouteName() == 'pay_ticket_callback' ? 'active open' : null}}">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-newspaper-o"></i>
                            <span class="title">{{ __('Pay Tickets') }}</span>
                            <span class="{{Route::currentRouteName() == 'pay_ticket_callback' ? 'selected' : 'arrow'}}"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket_callback' && request()->route()->parameters['type'] == "traffic" ? 'active open' : null}}">
                                <a href="{{route('pay_ticket_callback','traffic')}}" class="nav-link ">
                                    <span class="title">{{ __('Traffic Ticket') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket_callback' && request()->route()->parameters['type'] == "parking" ? 'active open' : null}}">
                                <a href="{{route('pay_ticket_callback','parking')}}" class="nav-link ">
                                    <span class="title">{{ __('Parking Ticket') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket_callback' && request()->route()->parameters['type'] == "speed_camera" ? 'active open' : null}}">
                                <a href="{{route('pay_ticket_callback','speed_camera')}}" class="nav-link ">
                                    <span class="title">{{ __('Speed Camera') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket_callback' && request()->route()->parameters['type'] == "red_light_camera" ? 'active open' : null}}">
                                <a href="{{route('pay_ticket_callback','red_light_camera')}}" class="nav-link ">
                                    <span class="title">{{ __('Red Light Camera') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="nav-item {{Route::currentRouteName() == 'trial_request_potential_dashboard' || Route::currentRouteName() == 'trial_request_potential_dashboard.view' || Route::currentRouteName() == 'trial_request_potential_dashboard.edit' || Route::currentRouteName() == 'trial_history_potential_dashboard' || Route::currentRouteName() == 'pay_ticket_potential' ? 'active open' : null}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">{{ __('Potential Clients') }}</span>
                    <span class="{{Route::currentRouteName() == 'pay_ticket_potential' ? 'selected' : 'arrow'}}"></span>
                </a>
                {{-- {{dd(Route::currentRouteName())}} --}}
                <ul class="sub-menu">
                    <li class="nav-item start {{Route::currentRouteName() == 'trial_request_potential_dashboard' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_potential_dashboard.view' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_potential_dashboard.edit' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_history_potential_dashboard' && request()->route()->parameters['type'] == "Trial_Request" ? 'active open' : null}}">
                        <a href="{{route('trial_request_potential_dashboard','Trial_Request')}}" class="nav-link nav-toggle">
                            <i class="fa fa-wrench"></i>
                            <span class="title">TRIAL REQUEST</span>
                            <span class="{{Route::currentRouteName() == 'trial_request_potential_dashboard' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_potential_dashboard.view' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_request_potential_dashboard.edit' && request()->route()->parameters['type'] == "Trial_Request" || Route::currentRouteName() == 'trial_history_potential_dashboard' && request()->route()->parameters['type'] == "Trial_Request" ? 'selected' : null}}"></span>
                        </a>
                    </li>
                    <li class="nav-item start {{Route::currentRouteName() == 'trial_request_potential_dashboard' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_potential_dashboard.view' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_potential_dashboard.edit' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_history_potential_dashboard' && request()->route()->parameters['type'] == "Early_Request" ? 'active open' : null}}">
                        <a href="{{route('trial_request_potential_dashboard','Early_Request')}}" class="nav-link nav-toggle">
                            <i class="fa fa-comments"></i>
                            <span class="title">EARLY RESOLUTION</span>
                            <span class="{{Route::currentRouteName() == 'trial_request_potential_dashboard' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_potential_dashboard.view' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_request_potential_dashboard.edit' && request()->route()->parameters['type'] == "Early_Request" || Route::currentRouteName() == 'trial_history_potential_dashboard' && request()->route()->parameters['type'] == "Early_Request" ? 'selected' : null}}"></span>
                        </a>
                    </li>
                    <li class="nav-item {{Route::currentRouteName() == 'pay_ticket_potential' ? 'active open' : null}}">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-newspaper-o"></i>
                            <span class="title">{{ __('Pay Tickets') }}</span>
                            <span class="{{Route::currentRouteName() == 'pay_ticket_potential' ? 'selected' : 'arrow'}}"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket_potential' && request()->route()->parameters['type'] == "traffic" ? 'active open' : null}}">
                                <a href="{{route('pay_ticket_potential','traffic')}}" class="nav-link ">
                                    <span class="title">{{ __('Traffic Ticket') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket_potential' && request()->route()->parameters['type'] == "parking" ? 'active open' : null}}">
                                <a href="{{route('pay_ticket_potential','parking')}}" class="nav-link ">
                                    <span class="title">{{ __('Parking Ticket') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket_potential' && request()->route()->parameters['type'] == "speed_camera" ? 'active open' : null}}">
                                <a href="{{route('pay_ticket_potential','speed_camera')}}" class="nav-link ">
                                    <span class="title">{{ __('Speed Camera') }}</span>
                                </a>
                            </li>
                            <li class="nav-item  {{Route::currentRouteName() == 'pay_ticket_potential' && request()->route()->parameters['type'] == "red_light_camera" ? 'active open' : null}}">
                                <a href="{{route('pay_ticket_potential','red_light_camera')}}" class="nav-link ">
                                    <span class="title">{{ __('Red Light Camera') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- <li class="nav-item start {{Route::currentRouteName() == 'pay_ticket' ? 'active open' : null}}">
                <a href="{{route('pay_ticket')}}" class="nav-link nav-toggle">
                    <i class="fa fa-envelope"></i>
                    <span class="title">PAY TICKET</span>
                    <span class="{{Route::currentRouteName() == 'pay_ticket' ? 'selected' : null}}"></span>
                </a>
            </li> --}}
        </ul>
        <!-- END SIDEBAR MENU -->
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
