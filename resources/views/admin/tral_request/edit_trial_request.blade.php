@extends('admin.layouts.master')
@section('title', __('Trial Request'))

@section('top-styles')
<link rel="stylesheet" href="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.css">

<!-- Plugins css-->
<link href="{{url('')}}/dash-assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" />
<style>
    a.btn.btn-secondary {
        background: #dc3535bf;
        border: 1px solid white;
    }
    h2#swal2-title {
        font-size: 25px;
    }
    div#swal2-content {
        font-size: 17px;
    }
    .swal2-icon.swal2-warning, .swal2-success-ring {
        border-radius: 50% !important;
        font-size: 11px;
    }
    .swal2-popup.swal2-modal.swal2-icon-warning.swal2-show {
        height: 280px;
        width: 36em;
    }
    table.dataTable.no-footer
    {
        border-bottom:inherit !important;
    }
    .dataTables_wrapper .dt-buttons, div#sample_1_length, div#sample_1_paginate{
        display: none
    }
</style>

@endsection

@section('content')
@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <a href="{{route('home')}}">{{ __('ADMIN')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>{{ __('Trial Request')}}</span>
        <i class="fa fa-circle"></i>

    </li>
    <li>
        <span>{{ __('Edit')}}</span>
    </li>
</ul>
@endsection

<!-- BEGIN Category STATS 1-->
<!-- BEGIN Products STATS 1-->
<div class="row">
    <div class="col-md-12">
        <!-- jquery validation -->
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-wrench"> </i> {{ __('Edit Request')}}
                            <a href="{{route('trial_request_dashboard','Trial_Request')}}"
                            class="btn btn-primary btn-md float-sm-right" style="background:#00bcd4"
                            >{{ __('Go Back')}}</a>
                        </div>
                        <div class="panel-body">
                            <form method="post" action="{{route('trial_request_dashboard_contact.update',["Trial_Request",$contact->id])}}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="col-md-6">
                                    <strong>First Name:</strong> </h5>
                                    <input type="text" class="form-control" value="{{$contact->first_name}}"
                                        name="first_name">
                                </div>
                                <div class="col-md-6">
                                    <strong>Last Name:</strong> </h5>
                                    <input type="text" class="form-control" value="{{$contact->last_name}}"
                                        name="last_name">
                                </div>
                                <div class="col-md-6">
                                    <strong>Email:</strong> </h5>
                                    <input type="text" class="form-control" value="{{$contact->email}}" name="email">
                                </div>
                                <div class="col-md-6">
                                    <strong>Phone:</strong> </h5>
                                    <input type="text" class="form-control" value="{{$contact->phone}}" name="phone">
                                </div>


                                <div class="col-md-6">
                                    <strong>Address:</strong> </h5>
                                    <input type="text" class="form-control" value="{{$contact->address}}" name="address">
                                </div>
                                <div class="col-md-6">
                                    <strong>City:</strong></h5>
                                    <input type="text" class="form-control" value="{{$contact->city}}" name="city">
                                </div>
                                <div class="col-md-6">
                                    <strong>Province:</strong></h5>
                                    <input type="text" class="form-control" value="{{$contact->province}}" name="province">
                                </div>
                                <div class="col-md-6">
                                    <strong>Postal Code:</strong></h5>
                                    <input type="text" class="form-control" value="{{$contact->postal_code}}"
                                        name="postal_code">

                                </div>
                            </div>
                            <h1>Icon and Tickets Section</h1>

                            <div class="row">

                                <div class="col-md-6">
                                    <strong>Icon Code:</strong> </h5>
                                    <select class="form-control" name="icon_code" parsley-trigger="change" required>
                                        @foreach ($iconlist as $icon)
                                            <option value="{{$icon->icon_code}}" {{$contact->icon_code == $icon->icon_code ? 'selected' : old('icon_code')}}>{{$icon->icon_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <strong>Interpreter:</strong></h5>
                                    <select class="form-control" name="interpreter" parsley-trigger="change" required>
                                        <option value="{{$contact->interpreter}}" selected>{{$contact->interpreter}}</option>
                                        <option value="Afrikaans">Afrikaans</option>
                                        <option value="Albanian">Albanian</option>
                                        <option value="Amharic">Amharic (Ethiopian)</option>
                                        <option value="Arabic">Arabic</option>
                                        <option value="Armenian">Armenian</option>
                                        <option value="Assyrian">Assyrian</option>
                                        <option value="Azeri">Azeri</option>
                                        <option value="Bambara">Bambara</option>
                                        <option value="Basque">Basque</option>
                                        <option value="Bengali">Bengali</option>
                                        <option value="Bulgarian">Bulgarian</option>
                                        <option value="Burmese">Burmese</option>
                                        <option value="Catalan">Catalan</option>
                                        <option value="Cambodian">Cambodian</option>
                                        <option value="Cantonese">Cantonese</option>
                                        <option value="Chinese ">Chinese </option>
                                        <option value="Creole ">Creole </option>
                                        <option value="Chiu-Chow">Chiu-Chow</option>
                                        <option value="Cree ">Cree </option>
                                        <option value="Croation">Croation</option>
                                        <option value="Czech">Czech</option>
                                        <option value="Danish">Danish</option>
                                        <option value="Dari">Dari</option>
                                        <option value="Dutch">Dutch</option>
                                        <option value="English">English</option>
                                        <option value="Edo (Nigerian)">Edo (Nigerian)</option>
                                        <option value="Estonian">Estonian</option>
                                        <option value="Farsi">Farsi</option>
                                        <option value="Filipino (Tagalog)">Filipino (Tagalog)</option>
                                        <option value="Finnish">Finnish</option>
                                        <option value="Flemish">Flemish</option>
                                        <option value="French">French</option>
                                        <option value="Fulani (Gambia)">Fulani (Gambia)</option>
                                        <option value="Georgian">Georgian</option>
                                        <option value="German">German</option>
                                        <option value="Greek">Greek</option>
                                        <option value="Gujarati">Gujarati</option>
                                        <option value="Hebrew">Hebrew</option>
                                        <option value="Hindi">Hindi</option>
                                        <option value="Hungarian">Hungarian</option>
                                        <option value="Ibo (Nigerian)">Ibo (Nigerian)</option>
                                        <option value="Igbo(Nigerian)">Igbo(Nigerian)</option>
                                        <option value="Indonesian">Indonesian</option>
                                        <option value="Irish">Irish</option>
                                        <option value="Inuit">Inuit</option>
                                        <option value="Italian">Italian</option>
                                        <option value="Japanese">Japanese</option>
                                        <option value="Korean">Korean</option>
                                        <option value="Kurdish">Kurdish</option>
                                        <option value="Lao">Lao</option>
                                        <option value="Latvian">Latvian</option>
                                        <option value="Lebanese">Lebanese</option>
                                        <option value="Lithuanian">Lithuanian</option>
                                        <option value="Lingala">Lingala</option>
                                        <option value="Macedonian">Macedonian</option>
                                        <option value="Mandingo">Mandingo</option>
                                        <option value="Malay">Malay</option>
                                        <option value="Maltese">Maltese</option>
                                        <option value="Nepalese">Nepalese</option>
                                        <option value="Norwegian">Norwegian</option>
                                        <option value="Ojibway">Ojibway</option>
                                        <option value="Oromo">Oromo</option>
                                        <option value="Pashtou">Pashtou</option>
                                        <option value="Polish">Polish</option>
                                        <option value="Persian (Farsi)">Persian (Farsi)</option>
                                        <option value="Portuguese">Portuguese</option>
                                        <option value="Punjabi">Punjabi</option>
                                        <option value="Pushtu">Pushtu</option>
                                        <option value="Rahaween">Rahaween</option>
                                        <option value="Romanian">Romanian</option>
                                        <option value="Russian">Russian</option>
                                        <option value="Serbo-Croation">Serbo-Croation</option>
                                        <option value="Shanghai">Shanghai</option>
                                        <option value="Shona">Shona</option>
                                        <option value="Sign Language">Sign Language</option>
                                        <option value="Sinhala">Sinhala</option>
                                        <option value="Slovak">Slovak</option>
                                        <option value="Slovenian ">Slovenian </option>
                                        <option value="Somalian">Somalian</option>
                                        <option value="Spanish">Spanish</option>
                                        <option value="Swahili">Swahili</option>
                                        <option value="Swedish">Swedish</option>
                                        <option value="Tagalog (Filipino)">Tagalog (Filipino)</option>
                                        <option value="Taiwanese">Taiwanese</option>
                                        <option value="Tamil">Tamil</option>
                                        <option value="Thai">Thai</option>
                                        <option value="Tibetan">Tibetan</option>
                                        <option value="Tigrigna">Tigrigna</option>
                                        <option value="Turkish">Turkish</option>
                                        <option value="Twi">Twi</option>
                                        <option value="Ukiranian">Ukiranian</option>
                                        <option value="Urdu">Urdu</option>
                                        <option value="Vietnamese">Vietnamese</option>
                                        <option value="Wolof">Wolof</option>
                                        <option value="Yiddish">Yiddish</option>
                                        <option value="Yoruba">Yoruba</option>
                                        <option value="Bosnian">Bosnian</option>
                                        <option value="Chaldean">Chaldean</option>
                                        <option value="Hakka">Hakka</option>
                                        <option value="Oji-Cree">Oji-Cree</option>
                                        <option value="Swampy Cree">Swampy Cree</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <strong>Date:</strong></h5>
                                    <input type="date" class="form-control" value="{{$contact->date}}" name="date">

                                </div>

                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary ">Update</button>

                        </form>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-striped table-hover dt-responsive" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Ticket No </th>
                                    <th>Date Filed</th>
                                    <th>Completed</th>
                                    <th>Ticket Image</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contact->trial_request_tickets as $ticket)
                                <tr>
                                    <td>{{$ticket->id}}</td>
                                    <td>{{$ticket->ticket_no}}
                                    </td>
                                    <td>{{$ticket->datefiled}}</td>
                                    <td>{{$ticket->completed==1? 'Yes':'No'}}</td>
                                    <td>
                                        <a id="{{$ticket->ticket_no}}" data-toggle="modal" data-target="#exampleModalCenter"
                                            data-id="img{{$ticket->ticket_id}}"
                                            data-source="{{url('/uploads/file')}}/{{$ticket->images_name}}">
                                            <div class="img-div">
                                                <img id="img{{$ticket->ticket_id}}" style="height: 50px;width: 50px"
                                                    src="{{url('/uploads/file')}}/{{$ticket->images_name}}"
                                                    alt="{{$ticket->images_name}}"> </div>
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal" data-target="#exampleModal" data-dd="{{$ticket->datefiled}}"
                                            data-whatever="{{$ticket->ticket_no}}" data-icon="{{$ticket->ticket_no}}"
                                            data-comments="{{$ticket->id}}" data-pcomments="{{$ticket->ticket_no}}"
                                            data-completed="{{$ticket->completed}}">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Ticket Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img id="srcimg" src="" style="height: 400px;width: 450px"  alt="ticket.jpeg">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- MODAL--}}



    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Ticket </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/ticketimageupdate" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="ticket_id" name="id">

                        <div class="form-group">
                            <label for="date" class="col-form-label">Ticket No:</label>
                            <input type="text" class="form-control" id="Ticket_no" name="ticket_no">
                        </div>

                        <div class="form-group">
                            <label for="images" class="col-form-label">Update Image </label>
                            <input style="overflow: hidden" type="file" capture="camera" accept=".jpg, .jpeg, .png"
                                class="form-control" id="images" name="images">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="updateticketstats">Update</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    </div>
</div>
<div class="clearfix"></div>
@endsection

@section('rightsidebar')
@parent
@endsection

@section('page-scripts')
{{-- SweetAlert2 --}}
<script src="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>

<script src="{{url('')}}/dash-assets/plugins/switchery/js/switchery.min.js"></script>
<script src="{{url('')}}/dash-assets/plugins/custom/jquery.nicescroll.js"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{url('')}}/dash-assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{url('')}}/dash-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{url('')}}/dash-assets/pages/scripts/table-datatables-responsive.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
{{-- PDF MAKE --}}
@endsection

@section('custom-script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#sample_1').DataTable();

        $('#exampleModalCenter').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget) // Button that triggered the modal
            let id = button.data('id') // Extract info from data-* attributes
            let src = button.data('source') // Extract info from data-* attributes

            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            let modal = $(this);

            modal.find('#srcimg').attr('src', src);

        });
        $('#exampleModal').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget) // Button that triggered the modal
            let id = button.data('comments') // Extract info from data-* attributes
            let comments = button.data('comments') // Extract info from data-* attributes
            let pcomments = button.data('pcomments')
            let date = button.data('dd') // Extract info from data-* attributes
            let completed = button.data('completed') // Extract info from data-* attributes
            var update = '{{route("trial_request_dashboard.image_update",["Trial_Request",":id"])}}';
            update = update.replace(':id', id);
            console.log(date);
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            let modal = $(this);
            modal.find('#Ticket_no').val(id);
            modal.find('#date').val(date);
            modal.find('#Ticket_no').val(pcomments);
            modal.find('#pcomments').text(pcomments);
            modal.find('#updateticketstats').attr('formaction', update);
            // modal.find('#sendmail').attr('formaction', '/sendmail/'+id);
            if (completed == 1) {
                modal.find('#completed option[value="1"]').attr('selected', 'selected');
            }
        });
    });
</script>

@endsection
