<table class="table">
    <thead>
    <tr>

        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Email</th>
        <th scope="col">Phone</th>
        <th scope="col">Address</th>
        <th scope="col">City</th>
        <th scope="col">Province</th>
        <th scope="col">Postal Code</th>
        <th scope="col">Interpreter</th>
        <th scope="col">Offence Date</th>
        <th scope="col">Icon Code</th>
        <th scope="col">Ticket Numbers</th>
        <th scope="col">Email Send Date</th>
    </tr>
    </thead>
    <tbody>
    @foreach($details as $detail)
    <tr>
        <td scope="col">{{$detail->first_name}}</td>
        <td scope="col">{{$detail->last_name}}</td>
        <td scope="col">{{$detail->email}}</td>
        <td scope="col">{{$detail->phone}}</td>
        <td scope="col">{{$detail->address}}</td>
        <td scope="col">{{$detail->city}}</td>
        <td scope="col">{{$detail->province}}</td>
        <td scope="col">{{$detail->postal_code}}</td>
        <td scope="col">{{$detail->interpreter}}</td>
        <td scope="col">{{$detail->icon_code}}</td>
        @foreach ($detail->trial_request_tickets as $ticket)
        <td scope="col">{{$ticket->offence_date}}</td>
        <td scope="col">{{$ticket->ticket_no}}</td>
        @endforeach
        <td scope="col">{{$detail->emaildateticket}}</td>
    </tr>
    @endforeach
    </tbody>
</table>
