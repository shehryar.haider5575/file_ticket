<script>
    $('#step_1').submit(function(e){
        $('#loader').removeClass('hidden');
        e.preventDefault();
        var $this = $(this);
        var type = $('.type').val();
        var icon_id = $('.icon_id').val();

        var ticket_type = '';

        switch (type) {
            case 'traffic':
                ticket_type = '999';
                break;

            case 'parking':
                ticket_type = '999';
                break;

            case 'speed-camera':
                ticket_type = '994';
                break;

            case 'red-light-camera':
                ticket_type = '997';
                break;
        }

        var datefiled = $('.datefiled').val();
        var ticket_no = icon_id + '  -'+ ticket_type+'- 00  -' + $('.ticket_no').val()+' -00';
        var ticket_data = $('.ticket_no').val();

        var bussiness_name = $('.bussiness_name').val();
        var section = Number($this.data('id'));
        $('.banner1').addClass('hidden');
        hideBeforeActiveSection(section);


        if((icon_id != null) && (datefiled != null) && (ticket_no != null) && (bussiness_name != null)){
        var url =  '{{route("traffic_step_1",[":type"])}}';
        url = url.replace(':type', type);

        axios
            .post(url, {
                _token: '{{csrf_token()}}',
                _method: 'patch',
                icon_id: icon_id,
                datefiled: datefiled,
                ticket_no: ticket_data,
                bussiness_name: bussiness_name,
            })
            .then(function ({data}) {
                try {
                    if(data.hasOwnProperty('error') && data.error != null){
                        errorHandler( new Error(data.error));
                }
                   if(data.data.hasOwnProperty('TicketAmount')){
                       let ticketAmount = data.data['TicketAmount']
                        // console.log(,'12345678');;
                $('#loader').addClass('hidden');
                // console.log(responsive.data,'success');
                $('.offence-date-sh').text(datefiled);
                $('.icon-code-sh').text(icon_id);
                $('.ticket-sh').text(ticket_no);
                $('.fine-amount-sh').text('$'+ ticketAmount);
                $('.convenience-sh').text('$'+0);
                $('.total-payment-sh').text('$'+ticketAmount);
                hideBeforeActiveSection(section);
                section = Number($this.data('id')) + 1;
                hideAfterActiveSection(section);
                   }
                } catch (error) {
                    // errorHandler(error);
                     }

            })
            .catch(errorHandler);
        }
    function errorHandler (error){
        console.log(error,'error here');
                if(error.response != null){
                    Swal.fire(
                        'Failed!',
                        error.message,
                        'error'
                    )
                }
                $('.banner1').removeClass('hidden');
                $('#loader').addClass('hidden');

                $('#errors1').append(`
                    <li style='list-style: arabic-indic;color:#ff5722;margin-top:10px;'>`+error.message+`</li>
                `);
    }

    });



    </script>
