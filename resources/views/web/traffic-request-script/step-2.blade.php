<script>
    $('#step_2').submit(function(e){
        e.preventDefault();
        var $this 		= $(this);
        var section = Number($this.data('id'));
        var fine_amount = $('.fine_amount').val();
        var convince_fee = $('.convince_fee').val();
        var total_payable = $('.total_payable').val();
        var datefiled = $('.datefiled').val();
        var ticket_no = $('.ticket_no').val();
        var bussiness_name = $('.bussiness_name').val();
        var icon_id = $('.icon_id').val();

        if((fine_amount != null) && (convince_fee != null) && (total_payable != null)){

        axios
            .post('{{route("traffic_step_2","traffic")}}', {
                _token: '{{csrf_token()}}',
                _method: 'patch',
                fine_amount: fine_amount,
                convince_fee: convince_fee,
                total_payable: total_payable,
            })
            .then(function (responsive) {
                $('.offence-date-sh').html(datefiled);
                $('.icon-code-sh').html(icon_id);
                $('.ticket-sh').html(ticket_no);
                $('.fine-amount-sh').html('$'+fine_amount);
                $('.convenience-sh').html('$'+convince_fee);
                $('.total-payment-sh').html('$'+total_payable);

                hideBeforeActiveSection(section);
                section = Number($this.data('id')) + 1;
                hideAfterActiveSection(section);
            })
            .catch(function (error) {
                console.log(error.response.data.message);
                $('#errors1').append(`
                    <li style='list-style: arabic-indic;color:#ff5722;margin-top:10px;'>`+error.response.data.message+`</li>
                `);
            });
        }
    });
</script>
