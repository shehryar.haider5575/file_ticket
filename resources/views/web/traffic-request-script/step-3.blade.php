<script>
    $('#step_2').submit(function(e){
			e.preventDefault();
			var $this               =     $(this);
            var section             =     Number($this.data('id'));
            var card_holder_name    =     $('.card_holder_name').val();
            var email               =     $('.email').val();
            var card_no             =     $('.card_no').val();
            var expiry_date         =     $('.expiry_date').val();
            // var expiry_year         =     $('.expiry_year').val();
            var postal_code         =     $('.postal_code').val();
            var phone               =     $('.phone').val();
            var terms_1             =     $('.terms_1').val();
            var terms_2             =     $('.terms_2').val();
            var total_payment       =     $('.total-payment-sh').text();
            var fine_amount         =     $('.fine-amount-sh').text();
            var convenience_amount  =     $('.convenience-sh').text();
            var offence_date        =     $('.offence-date-sh').text();
            var icon_code           =     $('.icon_id').val();
            var ticket_no           =     $('.ticket_no').val();
			var type 				= 	  $('.type').val();

            if((card_holder_name != null && card_holder_name != "") && (email != null && email != "") && (expiry_date != null && expiry_date != "") && (card_no != null && card_no != "" && card_no.length == 16) && (postal_code != null && postal_code != "") && (terms_1 != null && terms_1 != "") && (terms_2 != null && terms_2 != "")){

            axios
				.post('{{route("traffic_step_3","traffic")}}', {
					_token		        :   '{{csrf_token()}}',
					_method		        :   'patch',
					card_holder_name 	:   card_holder_name,
					email 		        :   email,
					card_no 	        :   card_no,
					expiry_date         :   expiry_date,
					// expiry_year         :   expiry_year,
					offence_date        :   offence_date,
					total_payment       :   total_payment,
					fine_amount         :   fine_amount,
					convenience_amount  :   convenience_amount,
					postal_code         :   postal_code,
					phone               :   phone,
					icon_code           :   icon_code,
					ticket_no           :   ticket_no,
					type           		:   type,
				})
				.then(function (responsive) {
                    $('#exampleModalCenter2').modal('show');
				})
				.catch(function (error) {
					console.log(error);
				});
            }
    });
</script>
