@extends('web.layouts.master')
@section('page-script')
<style>
    .parsley-errors-list {
        width: 100%;
        text-align: left;
        padding-bottom: 8px;

    }
</style>
@endsection
@section('content')

<div class="banner-item4">
<!-- banner-area start -->
<div class="banner-item2">
    <div class="banner-item2-inner">
        <img src="{{url('')}}/web_assets/images/13.png" alt="" />
        <h2>Select an <span>Option</span></h2>
        <img src="{{url('')}}/web_assets/images/14.png" alt="" />
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-3" id="traffic-ticket">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-envelope"></i>
                </div>
                <div>
                    <h3>TRAFFIC Ticket</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','traffic')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','traffic')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="parking-ticket">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-comments"></i>
                </div>
                <div>
                    <h3>PARKING TICKET</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','parking')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','parking')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="speed-camera">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="fas fa-wrench"></i>
                </div>
                <div>
                    <h3>SPEED CAMERA</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','speed-camera')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','speed-camera')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="light-camera">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-user"></i>
                </div>
                <div>
                    <h3>RED LIGHT CAMERA</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','red-light-camera')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','red-light-camera')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner-area end -->
<br>
<br>
<div class="banner-item2-inner">
    <img src="{{url('')}}/web_assets/images/13.png" alt="" />
    <h2>Pay  <span>Traffic Ticket</span></h2>
    <img src="{{url('')}}/web_assets/images/14.png" alt="" />
</div>

    {{-- Step - 01 --}}
    <div class="banner1 banner-item5 hidden">
        <div class="banner-item6">
            <div class="row">
                <form id="step_1" method="POST" data-id="1">
                    <div class="col-lg-6 col-xl-5">
                        <div class="banner-item6-inner">
                            <div class="row">
                                <div class="col-md-6">
                                    <h6>01. Icon Code </h6>
                                </div>
                                <div class="col-md-6">
                                    <h6>02. Ticket Number </h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="banner-item6-inner">
                                        <select name="icon_id" class="icon_id" required>
                                            <option disabled selected>Select Icon Code</option>
                                            @foreach ($icons as $icon)
                                            <option value="{{$icon->icon_code}}">{{$icon->icon_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="banner-item6-inner">
                                        <input type="text" name="ticket_no" class="ticket_no" placeholder="Ticket Number" parsley-trigger="change" minlength="8" maxlength="8" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-5 offset-xl-2">
                        <div class="row">
                            <div class="col-md-6">
                                <h6>03. Offence Date Of The Ticket  </h6>
                            </div>
                            <div class="col-md-6">
                                <h6>04. Last Name/Bussiness Name </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="banner-item6-inner">
                                    <input type="text" id="datepicker" placeholder="mm/dd/yyyy" name="datefiled"
                                    class="datefiled" parsley-trigger="change" required />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="banner-item6-inner">
                                    <input type="text" name="bussiness_name" class="bussiness_name" placeholder="Last Name/Bussiness Name" required/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="banner-item7">
                                    <div class="banner-item7-inner">
                                        <a href="{{route('main_page')}}"><i class="fas fa-long-arrow-alt-left"></i>Cancel</a>
                                    </div>
                                </div>
                                <div class="banner-item6-inner">
                                    <button type="submit">Search Fine Amount <i  class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Step - 02 --}}
    <div class="banner2 banner-item5 hidden">
        <div class="banner-item8">
            <h2>05. Searching Fine Amount <span>Please Allow Up To 30 Seconds</span></h2>
            <form id="step_2" data-id="2" method="POST">
                <div class="banner-item16">
                    <i class="fas fa-equals"></i>
                    <div class="row">
                        <div class="col-lg-6 col-xl-5">
                            <div class="banner-item14">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="banner-item8-inner">
                                            <span>Fine Amount</span>
                                            <input type="text" name="fine_amount" class="fine_amount"
                                                placeholder="$ 110.00" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="banner-item8-inner">
                                            <span>Convenience Fee</span>
                                            <input type="text" name="convince_fee" class="convince_fee"
                                                placeholder="$ 5.00" />
                                        </div>
                                    </div>
                                </div>
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-5 ml-auto">
                            <div class="banner-item15">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="banner-item8-inner">
                                            <span>Total Payable </span>
                                            <input type="text" name="total_payable" class="total_payable"
                                                placeholder="$ 115.00" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="banner-item8-inner2">
                                            <button type="submit">Proceed To Payment <i
                                                    class="fas fa-long-arrow-alt-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Step - 03 --}}
    <div class="banner3 banner-item5 ">
        <div class="banner-item9">
            <form id="step_3" data-id="3" method="POST">
            <div class="banner-item9-inner">
                <h2>Please Enter Credit Card Details</h2>
                    {{-- <div class="banner-item9-inner2">
                        <input type="text" name="first_name" class="input-inner first_name" placeholder="First Name" required/>
                        <input type="text" name="last_name" class="input-inner last_name" placeholder="Last Name" required/>
                    </div> --}}
                    <div class="banner-item9-inner2">
                        <span>
                            <input style="min-width: 95%" parsley-trigger="change" required type="text" name="card_holder_name"
                                class="input-inner card_holder_name" placeholder="Card Holder name" />
                        </span>
                        <span>
                            <input type="text" name="card_no" class="input-inner card_no" data-parsley-type="digits" placeholder="Card Number" required/>
                        </span>
                    </div>
                    <div class="banner-item9-inner2">
                        <span>
                            <input type="text" name="expiry_date" class="input-inner expiry_date" data-parsley-type="digits" placeholder="Expiry Date MM/YY" required/>
                        </span>
                        <span>
                            <input type="text" name="postal_code" data-parsley-type="digits" class="input-inner postal_code" placeholder="Postal Code" required/>
                        </span>
                    </div>
                    <input type="email" name="email" class="input-inner email" placeholder="Email Address" required/>
                    <div class="banner-item9-inner3">
                        <input type="checkbox" name="terms_1" class="terms_1" id="test" required>
                        <label for="test">I am Paying my ticket volentarily and understand a conviction will
                            register</label>
                    </div>
                    <div class="banner-item9-inner3">
                        <input type="checkbox" name="terms_2" class="terms_2" id="test2" required>
                        <label for="test2">I have read, understood & accept the <a href="#">Terms and
                                Conditions</a></label>
                    </div>

            </div>
            <div class="banner-item9-inner">
                <h2>Ticket Information</h2>
                <div class="banner-item9-inner4">
                    <div class="banner-item9-inner5">
                        <h6>Offence Date</h6>
                        <h5 class="offence-date-sh">2021-05-12</h5>
                    </div>
                    <div class="banner-item9-inner5">
                        <h6>Icon Code</h6>
                        <h5 class="icon-code-sh">3560</h5>
                    </div>
                    <div class="banner-item9-inner5">
                        <h6>Ticket # </h6>
                        <h5 class="ticket-sh">12345678</h5>
                    </div>
                    <div class="banner-item9-inner5">
                        <h6>Fine Amount</h6>
                        <h5 class="fine-amount-sh">$110</h5>
                    </div>
                    <div class="banner-item9-inner5">
                        <h6>Convenience Fee</h6>
                        <h5 class="convenience-sh">$5</h5>
                    </div>
                    <div class="banner-item9-inner5 banner-item9-inner50">
                        <h6>Total Payment</h6>
                        <h5 class="total-payment-sh">$115</h5>
                    </div>
                </div>
                <div class="col-md-4" style="float: right;">
                    <div class="banner-item8-inner2">
                        <button type="submit" class="continue-btn " style="margin-top: 20px">Pay My Ticket <i class="fas fa-long-arrow-alt-right"></i></button>
                    </div>
                </div>
            </div>
            </form>

        </div>
    </div>



    {{-- <div class="banner-item7">
        <div class="banner-item7-inner">
            <a href="{{route('main_page')}}"><i class="fas fa-long-arrow-alt-left"></i>Cancel</a>
        </div>
    </div> --}}
</div>

<!-- banner-area end -->
<!-- modal-area start -->
<div class="modal-area">
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" data-dismiss="modal">
                    <img src="{{url('')}}/web_assets/images/99.png" alt="" />
                </button>
                <div class="modal-item">
                    <img src="{{url('')}}/web_assets/images/17.png" alt="" />
                    <h2>Ticket Being Processed</h2>
                    <h3>E-mail confirmation will be sent to you in a couple of minutes</h3>
                    <p> Should you not receive confirmation, please E-mail us <span>Info@filetickets.ca</span></p>
                    <h6>Thank You</h6>
                    <a href="{{route('main_page')}}" data-dismiss="modal" aria-label="Close">Okay</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal-area end -->

@section('custom-script')
@include('web.traffic-request-script.step-1')
@include('web.traffic-request-script.step-2')
@include('web.traffic-request-script.step-3')

<script>
    $('form').parsley();

    $(document).ready(function () {
        $('.interpreter-check').change(function () {
            var value = $(this).val();
            if (value == "yes") {
                $('.interpreter').removeClass('hidden');
                $('.interpreter').attr('required', 'required');
                $('.banner-item22-inner4 > ul').removeClass('hidden');
            } else {

                $('.interpreter').addClass('hidden');
                $('.interpreter').removeAttr('required');
                $('.interpreter').removeClass('parsley-error');
                $('.banner-item22-inner4 > ul').addClass('hidden');
            }
        });

        $('.back-btn').click(function () {
            var section = $(this).data('id');
            console.log('banner' + section);
            $('.banner' + section).addClass('hidden');
            $('.banner-item4-inner2').removeClass('active');
            $('.step' + section + ' > span').removeClass('active');
            $('.step' + section + ' > h6').removeClass('active');
            var prev_section = section - 1;
            if (section == 2) {
                $('.banner-item4-inner2').removeClass('active' + section);
                // $('.banner-item4-inner2').addClass('active');
            } else if (section > 2) {
                $('.banner-item4-inner2').removeClass('active' + section);
                $('.banner-item4-inner2').addClass('active' + prev_section);
            }
            $('.banner' + prev_section).removeClass('hidden');
            $('.step' + prev_section + ' > span').addClass('active');
            $('.step' + prev_section + ' > h6').addClass('active');

        });

        $('.convince_fee').keyup(function(){
            var con_fee      = $(this).val();
            var fine_anmount = $('.fine_amount').val();
            $('.total_payable').val(Number(con_fee) + Number(fine_anmount));
        });

        $('#step_3').submit(function (e) {
            e.preventDefault();
            var $this = $(this);
            var section = Number($this.data('id'));
            var first_name = $('.first_name').val();
            var last_name = $('.last_name').val();
            var card_no = $('.card_no').val();
            var expiry_date = $('.expiry_date').val();
            var phone_no = $('.phone_no').val();
            var postal_code = $('.postal_code').val();
            var address = $('.address').val();
            var terms_1 = $('.terms_1').val();
            var terms_2 = $('.terms_2').val();

            axios
                .post('{{route("traffic_step")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'patch',
                    first_name: first_name,
                    last_name: last_name,
                    card_no: card_no,
                    expiry_date: expiry_date,
                    phone_no: phone_no,
                    postal_code: postal_code,
                    address: address,
                    terms_1: terms_1,
                    terms_2: terms_2,
                })
                .then(function (responsive) {
                    hideBeforeActiveSection(section);
                    section = Number($this.data('id')) + 1;
                    hideAfterActiveSection(section);
                })
                .catch(function (error) {
                    console.log(error);
                });
        });

        $('#step_4').submit(function (e) {
            e.preventDefault();
            $('#exampleModalCenter2').modal('show');
        });

    });
    $("#datepicker").datepicker({
        startDate: '-40d', // controll start date like startDate: '-2m' m: means Month
        endDate: new Date()
    });

    function hideBeforeActiveSection(section) {

        $('.banner' + section).addClass('hidden');
        if (section == 1) {
            $('.banner-item4-inner2').addClass('active');
        } else {
            $('.banner-item4-inner2').addClass('active' + section);
        }
        $('.step' + section + ' > span').removeClass('active');
        $('.step' + section + ' > h6').removeClass('active');

    }

    function hideAfterActiveSection(section) {

        $('.banner' + section).removeClass('hidden');
        $('.step' + section + ' > span').addClass('active');
        $('.step' + section + ' > h6').addClass('active');
    }

</script>
@endsection
@endsection
