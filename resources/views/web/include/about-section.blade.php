<!-- about-area start -->
<div class="about-area">
	<div class="container">
		<div class="row align-items-center no-gutters">
			<div class="col-lg-5">
				<div class="about-item">
					<div class="about-item-inner d-none d-lg-block">
						<h2>About <span>Us</span></h2>
						<p>Filetickets.Ca Is A Process Server Company That Provides A Fast, And Secure Service Filing Or Paying Your Tickets In Any Provincial Court. Our Service Streamlines The Access To Justice For The General Public To File Their Traffic Tickets Online Through A User-Friendly Website.</p>
						<p>Filetickets.Ca Provides A Simplified Approach Providing Access To Justice Without Requiring In-Person Attendance At Court For Simple Filing Procedures. Our Online Platform Reduces The Number Of Individuals Attending Each Courthouse, Which Helps Prevent The Spread Of COVID-19.</p>
						<p>Filetickets.Ca Was Designed To Give Individuals With Traffic Tickets In Ontario Access To Justice While Saving Them Time And Money!</p>
						<p>If A Traffic Ticket Isn’t Filed Or Payed On Time It Will Result In A Conviction To Your Driving Record, Possibly Add Demerit Points, Negatively Affect Your Insurance Rates Or A DRIVING SUSPENSION!</p>
						<p>If You’re Faced With The Daunting Task Of Filing Your Ticket, DON’T WAIT IN LINE, FILE ONLINE!</p>
					</div>
					<div class="d-lg-none">
						<h2>About <span>Us</span></h2>
						<p>Filetickets.Ca Is A Process Server Company That Provides A Fast, And Secure Service Filing Or Paying Your Tickets In Any Provincial Court. Our Service Streamlines The Access To Justice For The General Public To File Their Traffic Tickets Online Through A User-Friendly Website.</p>
						<p>Filetickets.Ca Provides A Simplified Approach Providing Access To Justice Without Requiring In-Person Attendance At Court For Simple Filing Procedures. Our Online Platform Reduces The Number Of Individuals Attending Each Courthouse, Which Helps Prevent The Spread Of COVID-19.</p>
						<p>Filetickets.Ca Was Designed To Give Individuals With Traffic Tickets In Ontario Access To Justice While Saving Them Time And Money!</p>
						<p>If A Traffic Ticket Isn’t Filed Or Payed On Time It Will Result In A Conviction To Your Driving Record, Possibly Add Demerit Points, Negatively Affect Your Insurance Rates Or A DRIVING SUSPENSION!</p>
						<p>If You’re Faced With The Daunting Task Of Filing Your Ticket, DON’T WAIT IN LINE, FILE ONLINE!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-7">
				<div class="about-item2">
					<img src="{{url('')}}/web_assets/images/15.png" alt="" />
				</div>
			</div>
		</div>
	</div>
</div>
<!-- about-area end -->
