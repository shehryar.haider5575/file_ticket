<!-- get-area start -->
<div class="get-area" id="contactSectiontop">
	<div class="container">
		<div class="inner-container text-center">
			<h2>Send Us A Message <br>We’ll Get Right Back To You</h2>
			<p>IF YOUR TICKET WAS NOT FOUND ON THE PAYMENT SYSTEM PLEASE UPLOAD A COPY OF YOUR NOTICE OF FINE OR ORIGINAL TICKET</p>
		</div>
		<div class="get-item">
			<form action="#" method="POST">
				<h2>Fill In The Contact Form To Rech Us:</h2>
				<input type="text" name="" placeholder="Your Name*" required />
				<input type="email" name="" placeholder="Your Email*" required />
				<input type="text" name="" placeholder="Subject*" />
				<textarea placeholder="Your Message*"></textarea>
				<div class="get-item-inner3">
					<div>
						<i class="far fa-user"></i>
					</div>
					<div>
						<i class="far fa-user"></i>
					</div>
				</div>
				<button type="submit">Continue <i class="fas fa-long-arrow-alt-right"></i></button>
			</form>
			<div class="get-item-inner">
				<div class="get-item-inner2">
					<a href="#"><i class="fas fa-envelope"></i></a>
				</div>
				<div>
					<h6>Reach us at: <span>info@filetickets.ca</span></h6>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- get-area end -->
