<div class="row">
    <div class="col-md-6 col-lg-5">
        <div class="banner-item">
            <h2>File Traffic Tickets <span>Online</span></h2>
            <p>File Tickets service streamlines the ability to file or pay Traffic <br>  Tickets online in less than a minute. <span>So Don’t Wait In Line, File Online. Let’s Get Started</span></p>
            <ul>
                <li><a href="#">About Us</a></li>
                <li><a href="#">LEARN MORE</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-6 col-lg-7">
        <div class="banner-item">
            <img src="{{url('')}}/web_assets/images/12.png" alt="" />
        </div>
    </div>
</div>