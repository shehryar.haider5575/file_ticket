<!-- step-area start -->
<div class="step-area">
	<div class="container">
		<div class="inner-container text-center">
			<h2>4 Step <span>process</span></h2>
			<img src="{{url('')}}/web_assets/images/03.png" alt="" />
		</div>
		<div class="row">
			<div class="col-md-6 col-xl-3">
				<div class="step-item step-item2">
					<h2>01</h2>
					<h3>SELECT AN OPTION </h3>
					<p>Select 1 Of The 3 Ticket Options Below  Pay Ticket , Early Resolution Or Trial And Then Follow Our Online Process</p>
				</div>
			</div>
			<div class="col-md-6 col-xl-3">
				<div class="step-item">
					<h2>02</h2>
					<h3>Submit Ticket Info </h3>
					<p>Submit Ticket Information Through Our Online Process Along With Your Contact Or Payment Details.</p>
				</div>
			</div>
			<div class="col-md-6 col-xl-3">
				<div class="step-item step-item2">
					<h2>03</h2>
					<h3>Receive CONFIRMATION </h3>
					<p>We Will Email You Initial Confirmation Of Your Submission & Than A Receipt  From The Courts For Your Records.</p>
				</div>
			</div>
			<div class="col-md-6 col-xl-3">
				<div class="step-item">
					<h2>04</h2>
					<h3>Free Legal Guide</h3>
					<p>Guide For Defendants Is Available <a href="#">Here</a> An Independant Legal Representative Can Provide You With A Consultation</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- step-area end -->
