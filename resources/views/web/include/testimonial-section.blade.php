<!-- testimonial-area start -->
<div class="testimonial-area">
	<div class="container">
		<div class="inner-container text-center">
			<h2>Testimonials</h2>
			<p>See What Our Users Are Saying About Their Experience!</p>
		</div>
		<div class="owl-carousel owl-theme">
			<div>
				<div class="testimonial-item">
					<img src="{{url('')}}/web_assets/images/07.png" alt="" />
					<h2>Outstanding Experience</h2>
					<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
					<h3>Charles Xavier</h3>
					<h4>New York</h4>
				</div>
			</div>
			<div>
				<div class="testimonial-item">
					<img src="{{url('')}}/web_assets/images/07.png" alt="" />
					<h2>Outstanding Experience</h2>
					<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
					<h3>Charles Xavier</h3>
					<h4>Toronto</h4>
				</div>
			</div>
			<div>
				<div class="testimonial-item">
					<img src="{{url('')}}/web_assets/images/07.png" alt="" />
					<h2>Outstanding Experience</h2>
					<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
					<h3>Charles Xavier</h3>
					<h4>Toronto</h4>
				</div>
			</div>
			<div>
				<div class="testimonial-item">
					<img src="{{url('')}}/web_assets/images/07.png" alt="" />
					<h2>Outstanding Experience</h2>
					<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
					<h3>Charles Xavier</h3>
					<h4>Toronto</h4>
				</div>
			</div>
			<div>
				<div class="testimonial-item">
					<img src="{{url('')}}/web_assets/images/07.png" alt="" />
					<h2>Outstanding Experience</h2>
					<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
					<h3>Charles Xavier</h3>
					<h4>Toronto</h4>
				</div>
			</div>
		</div>
		<div class="testimonial-item2">
			<a href="#">View All Reviews</a>
		</div>
	</div>
</div>
<!-- testimonial-area end -->
