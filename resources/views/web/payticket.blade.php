@extends('web.layouts.master')

@section('content')
<!-- banner-area start -->
<div class="banner-item2">
    <div class="banner-item2-inner">
        <img src="{{url('')}}/web_assets/images/13.png" alt="" />
        <h2>Select a <span>Ticket</span></h2>
        <img src="{{url('')}}/web_assets/images/14.png" alt="" />
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-3" id="traffic-ticket">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-envelope"></i>
                </div>
                <div>
                    <h3>TRAFFIC Ticket</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','traffic')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','traffic')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="parking-ticket">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-comments"></i>
                </div>
                <div>
                    <h3>PARKING TICKET</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','parking')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','parking')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="speed-camera">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="fas fa-wrench"></i>
                </div>
                <div>
                    <h3>SPEED CAMERA</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','speed-camera')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','speed-camera')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="light-camera">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-user"></i>
                </div>
                <div>
                    <h3>RED LIGHT CAMERA</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','red-light-camera')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','red-light-camera')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner-area end -->

@endsection
