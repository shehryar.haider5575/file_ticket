@extends('web.layouts.master')
@section('page-script')
<link rel="stylesheet" href="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.css">

<style>
    .parsley-errors-list {
        width: 100%;
        text-align: left;
        padding-bottom: 8px;

    }

    .CoE-fading-circle {
            margin: 100px auto;
            width: 40px;
            height: 40px;
            position: relative
        }

        .CoE-fading-circle .CoE-circle {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0
        }

        .CoE-fading-circle .CoE-circle:before {
            content: '';
            display: block;
            margin: 0 auto;
            width: 15%;
            height: 15%;
            background-color: #4bcd3e;
            border-radius: 100%;
            -webkit-animation: CoE-circleFadeDelay 1.2s infinite ease-in-out both;
            animation: CoE-circleFadeDelay 1.2s infinite ease-in-out both
        }

        .CoE-fading-circle .CoE-circle2 {
            -webkit-transform: rotate(30deg);
            -ms-transform: rotate(30deg);
            transform: rotate(30deg)
        }

        .CoE-fading-circle .CoE-circle3 {
            -webkit-transform: rotate(60deg);
            -ms-transform: rotate(60deg);
            transform: rotate(60deg)
        }

        .CoE-fading-circle .CoE-circle4 {
            -webkit-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg)
        }

        .CoE-fading-circle .CoE-circle5 {
            -webkit-transform: rotate(120deg);
            -ms-transform: rotate(120deg);
            transform: rotate(120deg)
        }

        .CoE-fading-circle .CoE-circle6 {
            -webkit-transform: rotate(150deg);
            -ms-transform: rotate(150deg);
            transform: rotate(150deg)
        }

        .CoE-fading-circle .CoE-circle7 {
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg)
        }

        .CoE-fading-circle .CoE-circle8 {
            -webkit-transform: rotate(210deg);
            -ms-transform: rotate(210deg);
            transform: rotate(210deg)
        }

        .CoE-fading-circle .CoE-circle9 {
            -webkit-transform: rotate(240deg);
            -ms-transform: rotate(240deg);
            transform: rotate(240deg)
        }

        .CoE-fading-circle .CoE-circle10 {
            -webkit-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            transform: rotate(270deg)
        }

        .CoE-fading-circle .CoE-circle11 {
            -webkit-transform: rotate(300deg);
            -ms-transform: rotate(300deg);
            transform: rotate(300deg)
        }

        .CoE-fading-circle .CoE-circle12 {
            -webkit-transform: rotate(330deg);
            -ms-transform: rotate(330deg);
            transform: rotate(330deg)
        }

        .CoE-fading-circle .CoE-circle2:before {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s
        }

        .CoE-fading-circle .CoE-circle3:before {
            -webkit-animation-delay: -1s;
            animation-delay: -1s
        }

        .CoE-fading-circle .CoE-circle4:before {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s
        }

        .CoE-fading-circle .CoE-circle5:before {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s
        }

        .CoE-fading-circle .CoE-circle6:before {
            -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s
        }

        .CoE-fading-circle .CoE-circle7:before {
            -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s
        }

        .CoE-fading-circle .CoE-circle8:before {
            -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s
        }

        .CoE-fading-circle .CoE-circle9:before {
            -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s
        }

        .CoE-fading-circle .CoE-circle10:before {
            -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s
        }

        .CoE-fading-circle .CoE-circle11:before {
            -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s
        }

        .CoE-fading-circle .CoE-circle12:before {
            -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s
        }

        @-webkit-keyframes CoE-circleFadeDelay {

            0%,
            39%,
            100% {
                opacity: 0
            }

            40% {
                opacity: 1
            }
        }

        @keyframes CoE-circleFadeDelay {

            0%,
            39%,
            100% {
                opacity: 0
            }

            40% {
                opacity: 1
            }
        }

</style>
@endsection
@section('content')

<div class="banner-item4">
<!-- banner-area start -->
<div class="banner-item2">
    <div class="banner-item2-inner">
        <img src="{{url('')}}/web_assets/images/13.png" alt="" />
        <h2>Select an <span>Option</span></h2>
        <img src="{{url('')}}/web_assets/images/14.png" alt="" />
    </div>
    <div class="row" style="padding: 20px">
        <div class="col-md-6 col-lg-3" id="traffic-ticket">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-envelope"></i>
                </div>
                <div>
                    <h3>TRAFFIC Ticket</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','traffic')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','traffic')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="parking-ticket">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-comments"></i>
                </div>
                <div>
                    <h3>PARKING TICKET</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','parking')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','parking')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="speed-camera">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="fas fa-wrench"></i>
                </div>
                <div>
                    <h3>SPEED CAMERA</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','speed-camera')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','speed-camera')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3" id="light-camera">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                    <i class="far fa-user"></i>
                </div>
                <div>
                    <h3>RED LIGHT CAMERA</h3>
                    <ul>
                        <li><a href="{{route('traffic_step','red-light-camera')}}">View More Info <i class="fas fa-angle-down"></i></a>
                        </li>
                        <li><a href="{{route('traffic_step','red-light-camera')}}">Pay Ticket <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner-area end -->
<br>
<br>
<div class="banner-item2-inner">
    <img src="{{url('')}}/web_assets/images/13.png" alt="" />
    <h2 style="font-size: 30px;">Pay  <span>{{str_replace('-',' ',$type)}} Ticket</span></h2>
    <img src="{{url('')}}/web_assets/images/14.png" alt="" />
</div>
<div id="loader" class="CoE-fading-circle hidden">
    <div class="CoE-circle1 CoE-circle"></div>
    <div class="CoE-circle2 CoE-circle"></div>
    <div class="CoE-circle3 CoE-circle"></div>
    <div class="CoE-circle4 CoE-circle"></div>
    <div class="CoE-circle5 CoE-circle"></div>
    <div class="CoE-circle6 CoE-circle"></div>
    <div class="CoE-circle7 CoE-circle"></div>
    <div class="CoE-circle8 CoE-circle"></div>
    <div class="CoE-circle9 CoE-circle"></div>
    <div class="CoE-circle10 CoE-circle"></div>
    <div class="CoE-circle11 CoE-circle"></div>
    <div class="CoE-circle12 CoE-circle"></div>
</div>
    {{-- Step - 01 --}}
    <div class="banner1 banner-item5 ">
        <div class="banner-item6 ">
            <div class="row">
                <form id="step_1" method="POST" data-id="1">
                    <div class="col-lg-6 col-xl-5">
                        <div class="banner-item6-inner">
                            <div class="row">
                                <div class="col-md-6">
                                    <h6>01. Icon Code </h6>
                                </div>
                                <div class="col-md-6">
                                    <h6>02. Ticket Number </h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="banner-item6-inner">
                                        <select name="icon_id" class="icon_id" required>
                                            <option disabled selected>Select Icon Code</option>
                                            @foreach ($icons as $icon)
                                            <option value="{{$icon->icon_code}}">{{$icon->icon_code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="banner-item6-inner">
                                        <input type="text" name="ticket_no" class="ticket_no" placeholder="Ticket Number" parsley-trigger="change" minlength="8" maxlength="8" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-5 offset-xl-2">
                        <div class="row">
                            <div class="col-md-6">
                                <h6>03. Offence Date Of The Ticket </h6>
                            </div>
                            <div class="col-md-6">
                                <h6>04. Last Name/Bussiness Name </h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="banner-item6-inner">
                                    <input type="text" id="datepicker" placeholder="mm/dd/yyyy" name="datefiled"
                                    class="datefiled" parsley-trigger="change" required />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="banner-item6-inner">
                                    <input type="text" name="bussiness_name" data-parsley-maxlength="3" class="bussiness_name" placeholder="Last Name/Bussiness Name" required/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 banner-item7 step2uploadphoto" style="padding: 18px 0px;">
                            <div class="banner-item7-inner">
                                    <a href="{{route('trial_request')}}">Cancel</a>
                            </div>
                            <div>
                                <ul>
                                    <li class="banner-item6-inner">
                                          <button type="submit"
                                            class="continue-btn " style="width:280px">Search Fine Amount <i
                                                class="fas fa-long-arrow-alt-right"></i></button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-6">
                                <div class="banner-item7">
                                    <div class="banner-item7-inner">
                                        <a href="{{route('main_page')}}"><i class="fas fa-long-arrow-alt-left"></i>Cancel</a>
                                    </div>
                                </div>
                                <div class="banner-item6-inner">
                                    <button type="submit">Search Fine Amount <i  class="fas fa-long-arrow-alt-right"></i></button>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Step - 03 --}}
    <div class="banner2 banner-item5  hidden">
        <div class="banner-item9">
            <form id="step_2" data-id="2" method="POST">
                    <div class="banner-item9-inner">
                            <h2>Ticket Information</h2>
                            <div class="banner-item9-inner4">
                                    <div class="banner-item9-inner5">
                                            {{-- offence number should be like --}}
                                          {{--
                                          997  is red light
                                          994 is speed camera
                                          999 is the pay tickets
                                           --}}

                                          {{-- 3161 -999        - 00 -9283873B  - 00 --}}
                                          {{-- icon -option code- 00 -ticket#   - 00 --}}
                                          <h6>Ticket # </h6>
                                          <h5 class="ticket-sh">12345678</h5>
                                      </div>
                                <div class="banner-item9-inner5">
                                    <h6>Offence Date</h6>

                                    <h5 class="offence-date-sh">2021-05-12</h5>
                                </div>

                                <div class="banner-item9-inner5">
                                    <h6>Fine Amount</h6>
                                    <h5 class="fine-amount-sh">$110</h5>
                                </div>
                                <div class="banner-item9-inner5">
                                    <h6>Convenience Fee</h6>
                                    <h5 class="convenience-sh">$5</h5>
                                </div>
                                <div class="banner-item9-inner5 banner-item9-inner50">
                                    <h6>Total Payment</h6>
                                    <h5 class="total-payment-sh">$115</h5>
                                </div>
                            </div>

                            <div class="banner-item9-inner3">
                                    <input type="checkbox" name="terms_1" class="terms_1" id="test" required>
                                    <label for="test">I am Paying my ticket volentarily and understand a conviction will
                                        register</label>
                                </div>
                                <div class="banner-item9-inner3">
                                    <input type="checkbox" name="terms_2" class="terms_2" id="test2" required>
                                    <label for="test2">I have read, understood & accept the <a href="#">Terms and
                                            Conditions</a></label>
                                </div>

                        </div>
            <div class="banner-item9-inner">
                <h2>Please Enter Credit Card Details</h2>
                    {{-- <div class="banner-item9-inner2">
                        <input type="text" name="first_name" class="input-inner first_name" placeholder="First Name" required/>
                        <input type="text" name="last_name" class="input-inner last_name" placeholder="Last Name" required/>
                    </div> --}}
                    <div class="banner-item9-inner2">
                        <span>
                            <input style="min-width: 95%" parsley-trigger="change" required type="text" name="card_holder_name"
                                class="input-inner card_holder_name" placeholder="Card Holder name" />

                        </span>
                        <span>
                            <input type="text" name="card_no" class="input-inner card_no" data-parsley-maxlength="16" data-parsley-minlength="16" data-parsley-type="digits" placeholder="Card Number" required/>

                        </span>
                    </div>
                    <div class="banner-item9-inner2">
                        <span>
                            {{-- <input type="text" name="card_expiry_month" class="input-inner expiry_date" data-parsley-type="digits" placeholder="Expiry Date MM/YY" required/> --}}
                            <input type="text" id="expirydatepicker" placeholder="Expiry Date (MM/YY)" name="card_expiry_month" class="input-inner expiry_date" parsley-trigger="change" required  style="min-width: 100% !important;"/>

                                {{-- {{ Form::selectMonth(null, null, ['name' => 'card_expiry_month', 'class' => 'input-inner expiry_date', 'required','style'=>'min-width: 45% !important;']) }} --}}
                                {{-- {{ Form::selectYear(null, date('Y'), date('Y') + 10, null, ['name' => 'card_expiry_year', 'class' => 'input-inner expiry_year', 'required','style'=>'min-width: 45% !important;']) }} --}}
                        </span>
                        <span>
                            <input type="hidden" class="type" name="type" value="{{$type}}">
                            <input type="text" name="postal_code" parsley-trigger="change" data-parsley-type="alphanum" class="input-inner postal_code" placeholder="Postal Code" required/>
                        </span>

                    </div>
                    <div class="banner-item9-inner2">

                        <span>
                            <input type="text" name="phone" parsley-trigger="change" class="input-inner phone" data-parsley-type="digits" placeholder="Phone Number" required/>
                        </span>
                        <span>
                            <input type="email" name="email" parsley-trigger="change" class="input-inner email" placeholder="Email Address" required/>

                        </span>
                    </div>


                    <div class="col-md-4" style="float: right;">
                            <div class="banner-item8-inner2">
                                <button type="submit" class="continue-btn " style="margin-top: 20px">Pay My Ticket </button>
                            </div>
                        </div>

            </div>

            </form>

        </div>
    </div>



    {{-- <div class="banner-item7">
        <div class="banner-item7-inner">
            <a href="{{route('main_page')}}"><i class="fas fa-long-arrow-alt-left"></i>Cancel</a>
        </div>
    </div> --}}
</div>

<!-- banner-area end -->
<!-- modal-area start -->
<div class="modal-area">
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" data-dismiss="modal">
                    <img src="{{url('')}}/web_assets/images/99.png" alt="" />
                </button>
                <div class="modal-item">
                    <img src="{{url('')}}/web_assets/images/17.png" alt="" />
                    <h2>Ticket Being Processed</h2>
                    <h3>E-mail confirmation will be sent to you in a couple of minutes</h3>
                    <p> Should you not receive confirmation, please E-mail us <span>Info@filetickets.ca</span></p>
                    <h6>Thank You</h6>
                    <a href="{{route('main_page')}}">Okay</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal-area end -->

@section('custom-script')
{{-- SweetAlert2 --}}
<script src="{{url('')}}/dash-assets/plugins/sweetalert2/sweetalert2.min.js" charset="UTF-8"></script>
@include('web.traffic-request-script.step-1')
@include('web.traffic-request-script.step-2')
@include('web.traffic-request-script.step-3')

<script>
    $('form').parsley();

    $(document).ready(function () {
        $('.interpreter-check').change(function () {
            var value = $(this).val();
            if (value == "yes") {
                $('.interpreter').removeClass('hidden');
                $('.interpreter').attr('required', 'required');
                $('.banner-item22-inner4 > ul').removeClass('hidden');
            } else {

                $('.interpreter').addClass('hidden');
                $('.interpreter').removeAttr('required');
                $('.interpreter').removeClass('parsley-error');
                $('.banner-item22-inner4 > ul').addClass('hidden');
            }
        });

        $('.back-btn').click(function () {
            var section = $(this).data('id');
            console.log('banner' + section);
            $('.banner' + section).addClass('hidden');
            $('.banner-item4-inner2').removeClass('active');
            $('.step' + section + ' > span').removeClass('active');
            $('.step' + section + ' > h6').removeClass('active');
            var prev_section = section - 1;
            if (section == 2) {
                $('.banner-item4-inner2').removeClass('active' + section);
                // $('.banner-item4-inner2').addClass('active');
            } else if (section > 2) {
                $('.banner-item4-inner2').removeClass('active' + section);
                $('.banner-item4-inner2').addClass('active' + prev_section);
            }
            $('.banner' + prev_section).removeClass('hidden');
            $('.step' + prev_section + ' > span').addClass('active');
            $('.step' + prev_section + ' > h6').addClass('active');

        });

        $('.convince_fee').keyup(function(){
            var con_fee      = $(this).val();
            var fine_anmount = $('.fine_amount').val();
            $('.total_payable').val(Number(con_fee) + Number(fine_anmount));
        });
    });
    $("#datepicker").datepicker({
        startDate: '-40d', // controll start date like startDate: '-2m' m: means Month
        endDate: new Date()
    });
    $("#expirydatepicker").datepicker({
        viewMode: 'years',
        // startView: 2,
        // minViewMode: 2,
        format: 'mm/yy'
    });

    function hideBeforeActiveSection(section) {

        $('.banner' + section).addClass('hidden');
        if (section == 1) {
            $('.banner-item4-inner2').addClass('active');
        } else {
            $('.banner-item4-inner2').addClass('active' + section);
        }
        $('.step' + section + ' > span').removeClass('active');
        $('.step' + section + ' > h6').removeClass('active');

    }

    function hideAfterActiveSection(section) {

        $('.banner' + section).removeClass('hidden');
        $('.step' + section + ' > span').addClass('active');
        $('.step' + section + ' > h6').addClass('active');
    }

</script>
@endsection
@endsection
