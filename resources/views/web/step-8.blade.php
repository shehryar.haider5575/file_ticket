@extends('web.layouts.master')

@section('content')
		<!-- banner-area start -->
		<div class="banner-area">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-5">
						<div class="banner-item">
							<h2>File Traffic Tickets <span>Online</span></h2>
							<p>File Tickets service streamlines the ability to file or pay Traffic <br>  Tickets online in less than a minute. <span>So don’t wait in line, File Online. Lets get started</span></p>
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">LEARN MORE</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-6 col-lg-7">
						<div class="banner-item">
							<img src="{{url('')}}/web_assets/images/12.png" alt="" />
						</div>
					</div>
				</div>
				<div class="banner-item4">
					<h2><span></span>   Pay SPEED CAMERA</h2>
					<div class="banner-item4-inner">
						<div class="banner-item4-inner2"></div>
					</div>
					<div class="banner-item4-inner3">
						<div class="banner-item4-inner4">
							<span class="active">01 <span></span></span>
							<h6 class="active">Offence Details</h6>
						</div>
						<div class="banner-item4-inner4">
							<span>02</span>
							<h6>Fine Amount</h6>
						</div>
						<div class="banner-item4-inner4">
							<span>03</span>
							<h6>Contact Details</h6>
						</div>
						<div class="banner-item4-inner4">
							<span>04</span>
							<h6>Submit</h6>
						</div>
					</div>
					<div class="banner-item5">
						<div class="banner-item6">
							<div class="row">
								<div class="col-lg-6 col-xl-5">
									<div class="banner-item6-inner">
										<h2>01. Ticket Number <span>*Ticket Example*</span></h2>
										<div class="row">
											<div class="col-md-6">
												<div class="banner-item6-inner">				
													<select>
														<option>Icon Code</option>
														<option>Icon Code 1</option>
														<option>Icon Code 2</option>
														<option>Icon Code 3</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="banner-item6-inner">
													<input type="text" name="" placeholder="Ticket Number" />
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-xl-5 offset-xl-2">
									<h2>02. Offence Date Of The Ticket</h2>			
									<form action="step-9.html">
										<div class="row">
											<div class="col-md-6">
												<div class="banner-item6-inner">				
													<select>
														<option>Select Date</option>
														<option>Select Date 1</option>
														<option>Select Date 2</option>
														<option>Select Date 3</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="banner-item6-inner">
													<button type="submit">Search Fine Amount   <i class="fas fa-long-arrow-alt-right"></i></button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="banner-item7">
						<div class="banner-item7-inner">
							<a href="#"><i class="fas fa-long-arrow-alt-left"></i>Cancel</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- banner-area end -->

		<!-- step-area start -->
		<div class="step-area">
			<div class="container">
				<div class="inner-container text-center">
					<h2>4 Step <span>process</span></h2>
					<img src="{{url('')}}/web_assets/images/03.png" alt="" />
				</div>
				<div class="row">
					<div class="col-md-6 col-xl-3">
						<div class="step-item step-item2">
							<h2>01</h2>
							<h3>SELECT AN OPTION </h3>
							<p>Select 1 Of The 3 Ticket Options Below  Pay Ticket , Early Resolution Or Trial And Then Follow Our Online Process</p>
						</div>
					</div>
					<div class="col-md-6 col-xl-3">
						<div class="step-item">
							<h2>02</h2>
							<h3>Submit Ticket Info </h3>
							<p>Submit Ticket Information Through Our Online Process Along With Your Contact Or Payment Details.</p>
						</div>
					</div>
					<div class="col-md-6 col-xl-3">
						<div class="step-item step-item2">
							<h2>03</h2>
							<h3>Receive CONFIRMATION </h3>
							<p>We Will Email You Initial Confirmation Of Your Submission & Than A Receipt  From The Courts For Your Records.</p>
						</div>
					</div>
					<div class="col-md-6 col-xl-3">
						<div class="step-item">
							<h2>04</h2>
							<h3>Free Legal Guide</h3>
							<p>Guide For Defendants Is Available <a href="#">Here</a> An Independant Legal Representative Can Provide You With A Consultation</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- step-area end -->

		<!-- introduction-area start -->
		<div class="introduction-area text-center">
			<div class="container">
				<div class="inner-container text-center">
					<h2>Introduction <span>Video</span></h2>
					<p>Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloree Laudantium, Totam Rem</p>
				</div>
				<div class="introduction-item">
					<img src="{{url('')}}/web_assets/images/16.png" alt="" />
				</div>
			</div>
		</div>
		<!-- introduction-area end -->

		<!-- about-area start -->
		<div class="about-area">
			<div class="container">
				<div class="row align-items-center no-gutters">
					<div class="col-lg-5">
						<div class="about-item">
							<div class="about-item-inner d-none d-lg-block">
								<h2>About <span>Us</span></h2>
								<p>Filetickets.Ca Is A Process Server Company That Provides A Fast, And Secure Service Filing Or Paying Your Tickets In Any Provincial Court. Our Service Streamlines The Access To Justice For The General Public To File Their Traffic Tickets Online Through A User-Friendly Website.</p>
								<p>Filetickets.Ca Provides A Simplified Approach Providing Access To Justice Without Requiring In-Person Attendance At Court For Simple Filing Procedures. Our Online Platform Reduces The Number Of Individuals Attending Each Courthouse, Which Helps Prevent The Spread Of COVID-19.</p>
								<p>Filetickets.Ca Was Designed To Give Individuals With Traffic Tickets In Ontario Access To Justice While Saving Them Time And Money!</p>
								<p>If A Traffic Ticket Isn’t Filed Or Payed On Time It Will Result In A Conviction To Your Driving Record, Possibly Add Demerit Points, Negatively Affect Your Insurance Rates Or A DRIVING SUSPENSION!</p>
								<p>If You’re Faced With The Daunting Task Of Filing Your Ticket, DON’T WAIT IN LINE, FILE ONLINE!</p>
							</div>
							<div class="d-lg-none">
								<h2>About <span>Us</span></h2>
								<p>Filetickets.Ca Is A Process Server Company That Provides A Fast, And Secure Service Filing Or Paying Your Tickets In Any Provincial Court. Our Service Streamlines The Access To Justice For The General Public To File Their Traffic Tickets Online Through A User-Friendly Website.</p>
								<p>Filetickets.Ca Provides A Simplified Approach Providing Access To Justice Without Requiring In-Person Attendance At Court For Simple Filing Procedures. Our Online Platform Reduces The Number Of Individuals Attending Each Courthouse, Which Helps Prevent The Spread Of COVID-19.</p>
								<p>Filetickets.Ca Was Designed To Give Individuals With Traffic Tickets In Ontario Access To Justice While Saving Them Time And Money!</p>
								<p>If A Traffic Ticket Isn’t Filed Or Payed On Time It Will Result In A Conviction To Your Driving Record, Possibly Add Demerit Points, Negatively Affect Your Insurance Rates Or A DRIVING SUSPENSION!</p>
								<p>If You’re Faced With The Daunting Task Of Filing Your Ticket, DON’T WAIT IN LINE, FILE ONLINE!</p>
							</div>
						</div>
					</div>
					<div class="col-lg-7">
						<div class="about-item2">
							<img src="{{url('')}}/web_assets/images/15.png" alt="" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- about-area end -->

		<!-- testimonial-area start -->
		<div class="testimonial-area">
			<div class="container">
				<div class="inner-container text-center">
					<h2>Testimonials</h2>
					<p>See What Our Users Are Saying About Their Experience!</p>
				</div>
				<div class="owl-carousel owl-theme">
					<div>
						<div class="testimonial-item">
							<img src="{{url('')}}/web_assets/images/07.png" alt="" />
							<h2>Outstanding Experience</h2>
							<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
							<h3>Charles Xavier</h3>
							<h4>New York</h4>
						</div>
					</div>
					<div>
						<div class="testimonial-item">
							<img src="{{url('')}}/web_assets/images/07.png" alt="" />
							<h2>Outstanding Experience</h2>
							<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
							<h3>Charles Xavier</h3>
							<h4>Toronto</h4>
						</div>
					</div>
					<div>
						<div class="testimonial-item">
							<img src="{{url('')}}/web_assets/images/07.png" alt="" />
							<h2>Outstanding Experience</h2>
							<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
							<h3>Charles Xavier</h3>
							<h4>Toronto</h4>
						</div>
					</div>
					<div>
						<div class="testimonial-item">
							<img src="{{url('')}}/web_assets/images/07.png" alt="" />
							<h2>Outstanding Experience</h2>
							<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
							<h3>Charles Xavier</h3>
							<h4>Toronto</h4>
						</div>
					</div>
					<div>
						<div class="testimonial-item">
							<img src="{{url('')}}/web_assets/images/07.png" alt="" />
							<h2>Outstanding Experience</h2>
							<p>“Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium,”</p>
							<h3>Charles Xavier</h3>
							<h4>Toronto</h4>
						</div>
					</div>
				</div>
				<div class="testimonial-item2">
					<a href="#">View All Reviews</a>
				</div>
			</div>
		</div>
		<!-- testimonial-area end -->

		<!-- get-area start -->
		<div class="get-area">
			<div class="container">
				<div class="inner-container text-center">
					<h2>Send Us A Message <br>We’ll Get Right Back To You</h2>
					<p>IF YOUR TICKET WAS NOT FOUND ON THE PAYMENT SYSTEM PLEASE UPLOAD A COPY OF YOUR NOTICE OF FINE OR ORIGINAL TICKET</p>
				</div>
				<div class="get-item">
					<form action="#" method="POST">
						<h2>Fill In The Contact Form To Rech Us:</h2>
						<input type="text" name="" placeholder="Your Name*" required />
						<input type="email" name="" placeholder="Your Email*" required />
						<input type="text" name="" placeholder="Subject*" />
						<textarea placeholder="Your Message*"></textarea>
						<div class="get-item-inner3">
							<div>
								<i class="far fa-user"></i>
							</div>
							<div>
								<i class="far fa-user"></i>
							</div>
						</div>
						<button type="submit">Continue <i class="fas fa-long-arrow-alt-right"></i></button>
					</form>
					<div class="get-item-inner">
						<div class="get-item-inner2">
							<a href="#"><i class="fas fa-envelope"></i></a>
						</div>
						<div>
							<h6>Reach us at: <span>info@filetickets.ca</span></h6>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- get-area end -->
@endsection