@extends('web.layouts.master')
@section('page-script')
<style>
    .parsley-errors-list {
        width: 100%;
        text-align: left;
        padding-bottom: 8px;

    }

    #click_upload {
        color: gray;
        padding: 44px;
        padding-bottom: 114px;
        cursor: pointer;
    }

</style>
@endsection
@section('content')
<div class="banner-item4">
    <center>
        <h2><span></span>TRIAL REQUEST</h2>
        <br>
    </center>
    <div class="banner-item4-inner">
        <div class="banner-item4-inner2"></div>
    </div>
    <div class="banner-item4-inner3">
        <div class="step1 banner-item4-inner4">
            <span class="active">01 <span></span></span>
            <h6 class="active">Offence Details</h6>
        </div>
        <div class="step2 banner-item4-inner4">
            <span>02</span>
            <h6>Upload Photo</h6>
        </div>
        <div class="step3 banner-item4-inner4">
            <span>03</span>
            <h6>Contact Details</h6>
        </div>
        <div class="step4 banner-item4-inner4">
            <span>04</span>
            <h6>Submit Request</h6>
        </div>
    </div>

    {{-- Step - 01 --}}
    <div class="banner1 banner-item5">
        <div class="banner-item6">
            <div class="row">
                <form id="step_1" method="POST" data-id="1">
                    <div class="col-lg-6 col-xl-5">
                        <div class="banner-item6-inner">
                            <h2>01. Offence Date Of The Ticket
                                    <a  data-toggle="modal" href="#offenceDateModal">
                                            <img src="{{url('')}}/web_assets/images/question-mark.png" alt="" />
                                    </a>
                            </h2>
                            <div class="row">
                                <div class="col-12">
                                    <div class="banner-item6-inner">
                                        <input type="text" id="datepicker" placeholder="mm/dd/yyyy" name="datefiled"
                                            class="datefiled" parsley-trigger="change" required />
                                    </div>
                                    <div class="banner-item6-inner">
                                        <ul id="errors1">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <p id="takeContactSection" class="extraDate">*Click here if Offence date 40+ days*</p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-5 offset-xl-2">
                        <div class="banner-item20">
                            <h2>02. Enter Your Ticket Number</h2>
                            <a href="#" id="loadMore"><i class="fas fa-plus"></i>Add Ticket</a>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="banner-item6-inner">
                                    <select name="icon_id" class="icon_id" parsley-trigger="change" required>
                                        <option disabled selected>Select Icon Code</option>
                                        @foreach ($icons as $icon)
                                        <option value="{{$icon->icon_code}}">{{$icon->icon_code}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 tick t1">
                                <div class="banner-item6-inner">
                                    <input type="text" name="ticket_no[]" class="ticket_no" placeholder="Enter Ticket #"
                                        parsley-trigger="change" data-value="1" minlength="8" maxlength="8" required />
                                </div>
                            </div>
                        </div>
                        <div class="banner-item30" >
                            <div class="row" id="add_more_tickets">

                            </div>
                        </div>

                    </div>
                    <div class="col-md-12 banner-item7" style="padding: 18px 18px;">
                        <div class="banner-item7-inner">
                                <a href="{{route('trial_request')}}">Cancel</a>
                        </div>
                        <div>
                            <ul>
                                <li style="display: inline-block;">
                                    <button type="submit" style="float: right !important;"
                                        class="continue-btn">Continue <i
                                            class="fas fa-long-arrow-alt-right"></i></button>
                                </li>
                            </ul>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    {{-- Step - 02 --}}
    <div class="banner2 banner-item5 hidden">
        <div class="banner-item21">
            <h2>Upload A Photo Of Ticket</h2>
            <div class="banner-item21-inner">
                <form id="step_2" method="POST" data-id="2" enctype="multipart/form-data">
                    <div class="row" id="ticket_images">
                        <div class="col-md-4 col-lg-2 hidden section1 img-tick">
                            <div class="banner-item21-inner2">
                                <div class="banner-item21-inner3">
                                    <div class="avatar-upload" style="height: 165px;width: 100%;">
                                        <div class="avatar-edit">
                                            <input type='file' name="ticket_photo1" id="ticket_photo1"
                                                accept=".png, .jpg, .jpeg, .webp" parsley-trigger="change" required />
                                            <label for="ticket_photo1"><span
                                                    id="click_upload">{{ __('Click to Upload')}}
                                                </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview1"
                                                style="background-image : url({{url('').'/uploads/placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6>For Ticket <span class="ticket_no_image1"> </span></h6>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-2 hidden section2 img-tick">
                            <div class="banner-item21-inner2">
                                <div class="banner-item21-inner3">
                                    <div class="avatar-upload" style="height: 165px;width: 100%;">
                                        <div class="avatar-edit">
                                            <input type='file' name="ticket_photo2" id="ticket_photo2"
                                                accept=".png, .jpg, .jpeg, .webp" required />
                                            <label for="ticket_photo2"><span
                                                    id="click_upload">{{ __('Click to Upload')}}
                                                </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview2"
                                                style="background-image : url({{url('').'/uploads/placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6>For Ticket <span class="ticket_no_image2"> </span></h6>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-2 hidden section3 img-tick">
                            <div class="banner-item21-inner2">
                                <div class="banner-item21-inner3">
                                    <div class="avatar-upload" style="height: 165px;width: 100%;">
                                        <div class="avatar-edit">
                                            <input type='file' name="ticket_photo3" id="ticket_photo3"
                                                accept=".png, .jpg, .jpeg, .webp" required />
                                            <label for="ticket_photo3"><span
                                                    id="click_upload">{{ __('Click to Upload')}}
                                                </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview3"
                                                style="background-image : url({{url('').'/uploads/placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6>For Ticket <span class="ticket_no_image3"> </span></h6>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-2 hidden section4 img-tick">
                            <div class="banner-item21-inner2">
                                <div class="banner-item21-inner3">
                                    <div class="avatar-upload" style="height: 165px;width: 100%;">
                                        <div class="avatar-edit">
                                            <input type='file' name="ticket_photo4" id="ticket_photo4"
                                                accept=".png, .jpg, .jpeg, .webp" required />
                                            <label for="ticket_photo4"><span
                                                    id="click_upload">{{ __('Click to Upload')}}
                                                </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview4"
                                                style="background-image : url({{url('').'/uploads/placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6>For Ticket <span class="ticket_no_image4"> </span></h6>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-2 hidden section5 img-tick">
                            <div class="banner-item21-inner2">
                                <div class="banner-item21-inner3">
                                    <div class="avatar-upload" style="height: 165px;width: 100%;">
                                        <div class="avatar-edit">
                                            <input type='file' name="ticket_photo5" id="ticket_photo5"
                                                accept=".png, .jpg, .jpeg, .webp" required />
                                            <label for="ticket_photo5"><span
                                                    id="click_upload">{{ __('Click to Upload')}}
                                                </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview5"
                                                style="background-image : url({{url('').'/uploads/placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6>For Ticket <span class="ticket_no_image5"> </span></h6>
                            </div>
                        </div>
                    </div>
                    <div class="banner-item6-inner">
                        <ul id="errors2">

                        </ul>
                    </div>
                    {{-- <br><br> --}}
                    <div class="col-md-12 banner-item7 step2uploadphoto" style="padding: 18px 18px;">
                        <div class="banner-item7-inner">
                                <a href="{{route('trial_request')}}">Cancel</a>
                        </div>
                        <div>
                            <ul>
                                <li class="col-md-12 backContinue">

                                    <a data-id="2" class="back-btn " style="margin-right: 10px"><i
                                            class="fas fa-long-arrow-alt-left" aria-hidden="true"></i>Back</a>

                                    {{-- <button type="button" style="float: left !important;" data-id="3" class="back-btn col-md-4" style="margin-right: 10px">Back
                                        <i class="fas fa-long-arrow-alt-left"></i></button> --}}
                                    <button type="submit"
                                        class="continue-btn ">Continue <i
                                            class="fas fa-long-arrow-alt-right"></i></button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Step - 03 --}}
    <div class="banner3 banner-item5 hidden">
        <form id="step_3" method="POST" data-id="3">
            <div class="banner-item9">
                <div class="banner-item9-inner">
                    <h2>03. Defendants Contact Details</h2>
                    <div class="banner-item9-inner2">
                        <span>
                            <input style="min-width: 95%" parsley-trigger="change" required type="text"
                                name="first_name" class="input-inner first_name" placeholder="First Name" />
                        </span>
                        <span>
                            <input style="min-width: 95%" parsley-trigger="change" required type="text" name="last_name"
                                class="input-inner last_name" placeholder="Last Name" />
                        </span>
                    </div>
                    <input parsley-trigger="change" required type="email" name="email" class="input-inner email"
                        placeholder="Email Address" />
                    <input parsley-trigger="change" data-parsley-type="digits" required type="text" name="phone_no" class="input-inner phone_no"
                        placeholder="Phone Number" />
                </div>
                <div class="banner-item9-inner">
                    <h2>04. Defendants Address</h2>
                    <input parsley-trigger="change" required type="text" name="address" class="input-inner address"
                        placeholder="Enter Address Line" />
                    <input parsley-trigger="change" required type="text" name="city" class="input-inner city"
                        placeholder="Enter City" />
                    <div class="banner-item9-inner2">

                            <span>
                                <input style="min-width: 95%" parsley-trigger="change" required type="text"
                                    name="province" class="input-inner province" placeholder="Enter Province" />
                            </span>
                            <span>
                                <input style="min-width: 100%" parsley-trigger="change" required type="text"
                                    name="postal_code" class="input-inner postal_code" placeholder="Postal Code" />
                            </span>

                    </div>
                    {{-- <div class="col-md-12 action-btns">
                        <div class="row banner-item6-inner">
                            <button type="button" data-id="3" class="back-btn col-md-4" style="margin-right: 10px">Back
                                <i class="fas fa-long-arrow-alt-left"></i></button>
                            <button type="submit" style="height: 59px;" class="continue-btn col-md-6">Continue <i
                                    class="fas fa-long-arrow-alt-right"></i></button>
                        </div>
                    </div> --}}
                </div>
            </div>
            {{-- <br><br> --}}
            <div class="col-md-12 banner-item7 step2uploadphoto" style="padding: 18px 18px;">
                <div class="banner-item7-inner">
                        <a href="{{route('trial_request')}}">Cancel</a>
                </div>
                <div>
                    <ul>
                        <li class="col-md-12 backContinue">

                            <a data-id="3" class="back-btn " style="margin-right: 10px"><i
                                    class="fas fa-long-arrow-alt-left" aria-hidden="true"></i>Back</a>

                        {{-- <li class="col-md-12 backContinue">

                            <a data-id="3"  style="margin-right: 10px"><i
                                    class="back-btn fas fa-long-arrow-alt-left" aria-hidden="true">Back</i></a> --}}

                              <button type="submit"
                                class="continue-btn ">Continue <i
                                    class="fas fa-long-arrow-alt-right"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
        <div class="banner-item6-inner">
            <ul id="errors3">

            </ul>
        </div>
    </div>

    {{-- Step - 04 --}}
    <div class="banner4 banner-item5 hidden" data-id="4">
        <div class="banner-item10">
            <div class="banner-item10-inner">
                <div class="banner-item10-inner2">
                    <h2>Ticket Information</h2>
                    <div class="banner-item9-inner5">
                        <h6>Offence Date</h6>
                        <h5 class="offence_date_label"></h5>
                    </div>
                    <div class="banner-item9-inner5">
                        <h6>Icon Code</h6>
                        <h5 class="icon_id_label"></h5>
                    </div>
                    <div id="total_tickets">


                    </div>
                </div>
                <div class="banner-item10-inner3">
                    <h2>Contact Information</h2>
                    <div class="banner-item10-inner4">
                        <div class="banner-item10-inner5">
                            <div class="banner-item9-inner5">
                                <h6>First Name</h6>
                                <h5 class="first_name_label"></h5>
                            </div>
                            <div class="banner-item9-inner5">
                                <h6>Last Name</h6>
                                <h5 class="last_name_label"></h5>
                            </div>
                            <div class="banner-item9-inner5">
                                <h6>Email</h6>
                                <h5 class="email_label"></h5>
                            </div>
                            <div class="banner-item9-inner5">
                                <h6>Phone</h6>
                                <h5 class="phone_no_label"></h5>
                            </div>
                        </div>
                        <div class="banner-item10-inner5">
                            <div class="banner-item9-inner5">
                                <h6>Address Line</h6>
                                <h5 class="address_label"></h5>
                            </div>
                            <div class="banner-item9-inner5">
                                <h6>City</h6>
                                <h5 class="city_label"></h5>
                            </div>
                            <div class="banner-item9-inner5">
                                <h6>Province</h6>
                                <h5 class="province_label"></h5>
                            </div>
                            <div class="banner-item9-inner5">
                                <h6>Postal Code</h6>
                                <h5 class="postal_code_label"></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-item22">
                    <div class="banner-item10-inner2 makeITHidden">
                            <h2>Ticket Information</h2>
                            <div class="banner-item9-inner5">
                                <h6>Offence Date</h6>
                                <h5 class="offence_date_label"></h5>
                            </div>
                            <div class="banner-item9-inner5">
                                <h6>Icon Code</h6>
                                <h5 class="icon_id_label"></h5>
                            </div>
                            <div id="total_tickets">


                            </div>
                        </div>
                <form id="step_4" method="post" data-id="4" style=" width:100%;    display: block !important;">
                    <div class="banner-item22-inner5">
                        <h2>Terms And Conditions</h2>
                        <div class="banner-item22-inner6">
                            <input type="checkbox" name="" id="test3" value="1" parsley-trigger="change" required>
                            <label for="test3">I have read, understood & accept the <a href="#">Terms and
                                    Conditions</a></label>
                        </div>
                    </div>
                    <hr>
                    <div class="banner-item22-inner">
                        <h2>Do you require an interpreter?</h2>
                        <div class="banner-item22-inner2">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" class="interpreter-check" value="no"
                                        checked=""> No
                                    <span></span>
                                </label>
                                <label style="padding: 0px 17px;" class="mt-radio">
                                    <input type="radio" name="optionsRadios" class="interpreter-check" value="yes"> Yes
                                    <span></span>
                                </label>
                            </div>
                            <div style="width: 215px;" class="banner-item22-inner4">
                                <select class="interpreter hidden" name="interpreter" parsley-trigger="change">
                                    <option value="">Choose a Language...</option>
                                    <option value="Afrikaans">Afrikaans</option>
                                    <option value="Albanian">Albanian</option>
                                    <option value="Amharic">Amharic (Ethiopian)</option>
                                    <option value="Arabic">Arabic</option>
                                    <option value="Armenian">Armenian</option>
                                    <option value="Assyrian">Assyrian</option>
                                    <option value="Azeri">Azeri</option>
                                    <option value="Bambara">Bambara</option>
                                    <option value="Basque">Basque</option>
                                    <option value="Bengali">Bengali</option>
                                    <option value="Bulgarian">Bulgarian</option>
                                    <option value="Burmese">Burmese</option>
                                    <option value="Catalan">Catalan</option>
                                    <option value="Cambodian">Cambodian</option>
                                    <option value="Cantonese">Cantonese</option>
                                    <option value="Chinese ">Chinese </option>
                                    <option value="Creole ">Creole </option>
                                    <option value="Chiu-Chow">Chiu-Chow</option>
                                    <option value="Cree ">Cree </option>
                                    <option value="Croation">Croation</option>
                                    <option value="Czech">Czech</option>
                                    <option value="Danish">Danish</option>
                                    <option value="Dari">Dari</option>
                                    <option value="Dutch">Dutch</option>
                                    <option value="English">English</option>
                                    <option value="Edo (Nigerian)">Edo (Nigerian)</option>
                                    <option value="Estonian">Estonian</option>
                                    <option value="Farsi">Farsi</option>
                                    <option value="Filipino (Tagalog)">Filipino (Tagalog)</option>
                                    <option value="Finnish">Finnish</option>
                                    <option value="Flemish">Flemish</option>
                                    <option value="French">French</option>
                                    <option value="Fulani (Gambia)">Fulani (Gambia)</option>
                                    <option value="Georgian">Georgian</option>
                                    <option value="German">German</option>
                                    <option value="Greek">Greek</option>
                                    <option value="Gujarati">Gujarati</option>
                                    <option value="Hebrew">Hebrew</option>
                                    <option value="Hindi">Hindi</option>
                                    <option value="Hungarian">Hungarian</option>
                                    <option value="Ibo (Nigerian)">Ibo (Nigerian)</option>
                                    <option value="Igbo(Nigerian)">Igbo(Nigerian)</option>
                                    <option value="Indonesian">Indonesian</option>
                                    <option value="Irish">Irish</option>
                                    <option value="Inuit">Inuit</option>
                                    <option value="Italian">Italian</option>
                                    <option value="Japanese">Japanese</option>
                                    <option value="Korean">Korean</option>
                                    <option value="Kurdish">Kurdish</option>
                                    <option value="Lao">Lao</option>
                                    <option value="Latvian">Latvian</option>
                                    <option value="Lebanese">Lebanese</option>
                                    <option value="Lithuanian">Lithuanian</option>
                                    <option value="Lingala">Lingala</option>
                                    <option value="Macedonian">Macedonian</option>
                                    <option value="Mandingo">Mandingo</option>
                                    <option value="Malay">Malay</option>
                                    <option value="Maltese">Maltese</option>
                                    <option value="Nepalese">Nepalese</option>
                                    <option value="Norwegian">Norwegian</option>
                                    <option value="Ojibway">Ojibway</option>
                                    <option value="Oromo">Oromo</option>
                                    <option value="Pashtou">Pashtou</option>
                                    <option value="Polish">Polish</option>
                                    <option value="Persian (Farsi)">Persian (Farsi)</option>
                                    <option value="Portuguese">Portuguese</option>
                                    <option value="Punjabi">Punjabi</option>
                                    <option value="Pushtu">Pushtu</option>
                                    <option value="Rahaween">Rahaween</option>
                                    <option value="Romanian">Romanian</option>
                                    <option value="Russian">Russian</option>
                                    <option value="Serbo-Croation">Serbo-Croation</option>
                                    <option value="Shanghai">Shanghai</option>
                                    <option value="Shona">Shona</option>
                                    <option value="Sign Language">Sign Language</option>
                                    <option value="Sinhala">Sinhala</option>
                                    <option value="Slovak">Slovak</option>
                                    <option value="Slovenian ">Slovenian </option>
                                    <option value="Somalian">Somalian</option>
                                    <option value="Spanish">Spanish</option>
                                    <option value="Swahili">Swahili</option>
                                    <option value="Swedish">Swedish</option>
                                    <option value="Tagalog (Filipino)">Tagalog (Filipino)</option>
                                    <option value="Taiwanese">Taiwanese</option>
                                    <option value="Tamil">Tamil</option>
                                    <option value="Thai">Thai</option>
                                    <option value="Tibetan">Tibetan</option>
                                    <option value="Tigrigna">Tigrigna</option>
                                    <option value="Turkish">Turkish</option>
                                    <option value="Twi">Twi</option>
                                    <option value="Ukiranian">Ukiranian</option>
                                    <option value="Urdu">Urdu</option>
                                    <option value="Vietnamese">Vietnamese</option>
                                    <option value="Wolof">Wolof</option>
                                    <option value="Yiddish">Yiddish</option>
                                    <option value="Yoruba">Yoruba</option>
                                    <option value="Bosnian">Bosnian</option>
                                    <option value="Chaldean">Chaldean</option>
                                    <option value="Hakka">Hakka</option>
                                    <option value="Oji-Cree">Oji-Cree</option>
                                    <option value="Swampy Cree">Swampy Cree</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    {{-- <br><br> --}}
                    <div class="col-md-12 banner-item7 step2uploadphoto" style="padding: 18px 18px;">
                        <div class="banner-item7-inner">
                            <a href="{{route('trial_request')}}">Cancel</a>
                        </div>
                        <div>
                            <ul>
                                <li style="display: inline-block;">
                                    <button type="submit" style="float: right !important;"
                                        class="continue-btn ">Finish <i
                                            class="fas fa-long-arrow-alt-right"></i></button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
                <div class="banner-item6-inner">
                    <ul id="errors4">

                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>


</div>
</div>
<!-- banner-area end -->
<!-- modal-area start -->
<div class="modal-area">
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter2" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" data-dismiss="modal">
                    <img src="{{url('')}}/web_assets/images/99.png" alt="" />
                </button>
                <div class="modal-item">
                    <img src="{{url('')}}/web_assets/images/17.png" alt="" />
                    <h2>Ticket Being Processed</h2>
                    <h3>E-mail confirmation will be sent to you in a couple of minutes</h3>
                    <p> Should you not receive confirmation, please E-mail us <span>Info@filetickets.ca</span></p>
                    <h6>Thank You</h6>
                    <a href="{{route('main_page')}}">Okay</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal-area end -->
{{-- OFFENCE DATA MODAL --}}
<!-- Modal -->
<div class="modal fade" id="offenceDateModal" tabindex="-1" role="dialog" aria-labelledby="offenceDateModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="offenceDateModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal2" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              ...
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal2">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </div>
      </div>
{{-- OFFENCE DATA MODAL --}}


@section('custom-script')
@include('web.trail-request-script.step-1')
@include('web.trail-request-script.step-2')
@include('web.trail-request-script.step-3')
@include('web.trail-request-script.step-4')

<script>
    $('form').parsley();
    $(document).ready(function(){
        $('.remove_ticket').click(function(e){
            e.preventDefault();
            var ticket = $('.tick').not('.hidden').length;
            console.log(ticket);
            $('.t'+ticket).addClass('hidden');
            // console.log("adfadfasdf");
            // $(this).css('display','block');
            // $(this).parent().parent().addClass('hidden');
        });

    });
    $('.interpreter-check').change(function () {
        var value = $(this).val();
        if (value == "yes") {
            $('.interpreter').removeClass('hidden');
            $('.interpreter').attr('required', 'required');
            $('.banner-item22-inner4 > ul').removeClass('hidden');
        } else {

            $('.interpreter').addClass('hidden');
            $('.interpreter').removeAttr('required');
            $('.interpreter').removeClass('parsley-error');
            $('.banner-item22-inner4 > ul').addClass('hidden');
        }
    });
    $("#datepicker").datepicker({
        startDate: '-40d', // controll start date like startDate: '-2m' m: means Month
        endDate: new Date()
    });
    $('.back-btn').click(function () {
        var section = $(this).data('id');
        console.log('banner' + section);
        $('.banner' + section).addClass('hidden');
        $('.banner-item4-inner2').removeClass('active');
        $('.step' + section + ' > span').removeClass('active');
        $('.step' + section + ' > h6').removeClass('active');
        var prev_section = section - 1;
        if (section == 2) {
            $('.banner-item4-inner2').removeClass('active' + section);
            // $('.banner-item4-inner2').addClass('active');
        } else if (section > 2) {
            $('.banner-item4-inner2').removeClass('active' + section);
            $('.banner-item4-inner2').addClass('active' + prev_section);
        }
        $('.banner' + prev_section).removeClass('hidden');
        $('.step' + prev_section + ' > span').addClass('active');
        $('.step' + prev_section + ' > h6').addClass('active');

    });

    function hideBeforeActiveSection(section) {

        $('.banner' + section).addClass('hidden');
        if (section == 1) {
            $('.banner-item4-inner2').addClass('active');
        } else {
            $('.banner-item4-inner2').addClass('active' + section);
        }
        $('.step' + section + ' > span').removeClass('active');
        $('.step' + section + ' > h6').removeClass('active');

    }

    function hideAfterActiveSection(section) {
        $('.banner' + section).removeClass('hidden');
        $('.step' + section + ' > span').addClass('active');
        $('.step' + section + ' > h6').addClass('active');
    }

    function readURL(input, number) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview' + number).hide();
                $('#imagePreview' + number).fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#ticket_photo1").change(function () {
        readURL(this, 1);
    });
    $("#ticket_photo2").change(function () {
        readURL(this, 2);
    });
    $("#ticket_photo3").change(function () {
        readURL(this, 3);
    });
    $("#ticket_photo4").change(function () {
        readURL(this, 4);
    });
    $("#ticket_photo5").change(function () {
        readURL(this, 5);
    });

</script>
@endsection
@endsection
