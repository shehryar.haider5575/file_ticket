@extends('web.layouts.master')
@section('content')
<div class="banner-item2">
    <div class="banner-item2-inner">
        <img src="{{url('')}}/web_assets/images/13.png" alt="" />
        <h2>Select an <span>option</span></h2>
        <img src="{{url('')}}/web_assets/images/14.png" alt="" />
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-3">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                        <img src="{{url('')}}/web_assets/images/pay_ticket.svg" alt="" style="margin-bottom:10px" />
                </div>
                <div>
                    <h3>pay Ticket</h3>
                    <ul>
                        <li onClick="changeClass(this)" data-toggle="collapse" href="#collapseMore1" role="button" aria-expanded="false" aria-controls="collapseMore1"> <p>View More Info <i class="fas fa-angle-down"></i></p> </li>
                        <li class="collapse" id="collapseMore1">You just need to put inside the $(document).ready() your code, and set in your css the position: relative; top:0; to your div who will be focus.</li>
                        <li><a href="{{route('pay_step')}}">Pay Ticket </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                        <img src="{{url('')}}/web_assets/images/pay_ticket.svg" alt="" style="margin-bottom:10px" />
                </div>
                <div>
                    <h3>Early Resolution</h3>
                    <ul>
                        <li onClick="changeClass(this)" data-toggle="collapse" href="#collapseMore2" role="button" aria-expanded="false" aria-controls="collapseMore2"> <p>View More Info <i class="fas fa-angle-down"></i></p> </li>
                        <li class="collapse" id="collapseMore2">You just need to put inside the $(document).ready() your code, and set in your css the position: relative; top:0; to your div who will be focus.</li>
                        <li><a href="{{route('early_resolution')}}">Early Resolution <i
                                    class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                        <img src="{{url('')}}/web_assets/images/pay_ticket.svg" alt="" style="margin-bottom:10px" />
                </div>
                <div>
                    <h3>Trial Request</h3>
                    <ul>
                        <li onClick="changeClass(this)" data-toggle="collapse" href="#collapseMore3" role="button" aria-expanded="false" aria-controls="collapseMore3"> <p>View More Info <i class="fas fa-angle-down"></i></p> </li>
                        <li class="collapse" id="collapseMore3">You just need to put inside the $(document).ready() your code, and set in your css the position: relative; top:0; to your div who will be focus.</li>
                        <li><a href="{{route('trial_request')}}">Trial Request <i class="fas fa-angle-down"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="banner-item3">
                <div class="banner-item3-inner">
                        <img src="{{url('')}}/web_assets/images/pay_ticket.svg" alt="" style="margin-bottom:10px" />
                </div>
                <div>
                    <h3>Need Help?</h3>
                    <ul>
                        <li onClick="changeClass(this)" data-toggle="collapse" href="#collapseMore4" role="button" aria-expanded="false" aria-controls="collapseMore4"> <p>View More Info <i class="fas fa-angle-down"></i></p> </li>
                        <li class="collapse" id="collapseMore4">You just need to put inside the $(document).ready() your code, and set in your css the position: relative; top:0; to your div who will be focus.</li>
                        <li><a href="#">Contact us <i class="fas fa-angle-down"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- banner-area end -->
@endsection
<script> 
function changeClass(e) {
    if( e.lastElementChild.children[0].classList.contains('fa-angle-down') ){
        e.lastElementChild.children[0].classList.remove('fa-angle-down');
        e.lastElementChild.children[0].classList.add('fa-angle-up');
    }else{
        e.lastElementChild.children[0].classList.add('fa-angle-down');
        e.lastElementChild.children[0].classList.remove('fa-angle-up');
    }
       }
</script>