<!-- header-area start -->
<header class="header-area">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="{{route('main_page')}}">
            <img src="{{url('')}}/web_assets/images/Logo.png" alt="" />
            </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
                <div class="hamburger hamburger--spring">
                    <div class="hamburger-box">
                      <div class="hamburger-inner"></div>
                    </div>
                </div>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                      <li class="nav-item">
                        <a class="nav-link active" href="#">File TIcket</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#">Pay Ticket</a>
                    </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#">About Us</a>
                    </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#">Testimonials</a>
                    </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#">FAQ</a>
                    </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#">Contact Us</a>
                    </li>
                      <li class="nav-item">
                          <a class="nav-link" href="#">Francais</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!-- header-area end -->
