<!DOCTYPE html>
<html lang="en-US">
	<head>
		<!-- Meta setup -->
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="">
		<meta name="decription" content="">
		<meta name="designer" content="Asad Kabir">

		<!-- Title -->
		<title>Welcome</title>

		<!-- Fav Icon -->
		<link rel="icon" href="{{url('')}}/web_assets/images/favicon.ico" />

		<!-- Include Bootstrap -->
		<link rel="stylesheet" href="{{url('')}}/web_assets/css/bootstrap.css" />

		<!-- Include owlcarousel -->
		<link rel="stylesheet" href="{{url('')}}/web_assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="{{url('')}}/web_assets/css/owl.theme.default.min.css">

		<!-- Main StyleSheet -->
		<link rel="stylesheet" href="{{url('')}}/web_assets/style.css" />

		<!-- Responsive CSS -->
		<link rel="stylesheet" href="{{url('')}}/web_assets/css/responsive.css" />
		{{-- Image-Upload --}}
		<link rel="stylesheet" href="{{url('')}}/dash-assets/global/css/image-upload.css">

        <link href="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        @yield('page-script')
  		<style>
            .datepicker-days{
                display: block !important;
            }
            .datepicker-months, .datepicker-years{
                display: none !important;
            }
            form{
				display: contents !important;
			}
            .fas.fa-long-arrow-alt-left{
                left: 0% !important;
            }
			.hidden{
				display: none !important;
			}
            .mt-checkbox-inline, .mt-radio-inline {
                    padding: 10px 0;
                }
            .back-btn{
				font-size: 18px !important;
				font-weight: 600 !important;
                cursor: pointer !important;
				color: #050000 !important;
				display: flex !important;
				align-items: center !important;
				justify-content: space-between !important;
				width: 300px !important;
                height: 60px !important;
				padding-left: 80px !important;
    			padding-right: 35px !important;
				background: #f1f9eb !important;
				border-radius: 10px !important;
				transition: 0.2s all ease !important;
				-webkit-transition: 0.2s all ease !important;
			}
			.back-btn i {
				right: 50% !important;
			}
			.action-btns{
				margin-top: 75px;
			}
			.parsley-error {
				border-color: #ff5722 !important;
				text-align: left;
			}
			.parsley-required {
				color: #ff5722 !important;
				text-align: left;
			}
            .continue-btn {
                background: #79C142 !important;

            }
            .parsley-type {
                color: #ff0000d1;
			}
			@media screen and (min-width: 768px) and (max-width:991px){
					.back-btn, .continue-btn{
					font-size: 18px !important;
					width: 200px !important;
					
					}
			}

		</style>
	</head>
	<body >
        {{-- Header --}}
        @include('web.layouts.header')
        <!-- banner-area start -->
		<div class="banner-area" >
			<div class="container">
					@if(Route::current()->getName() == 'main_page')
					@include('web.include.slider')
				@endif
				@yield('content')
			</div>
		</div>
		@include('web.include.step-section')
		@include('web.include.intro-section')
		@include('web.include.about-section')
		@include('web.include.testimonial-section')
		@include('web.include.contact-section')
        @include('web.layouts.footer')


	
		<!-- Main jQuery -->
    <script src="{{url('')}}/web_assets/js/jquery-3.4.1.min.js"></script>

    <!-- Bootstrap Propper jQuery -->
    <script src="{{url('')}}/web_assets/js/popper.js"></script>

    <!-- Bootstrap jQuery -->
    <script src="{{url('')}}/web_assets/js/bootstrap.js"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/moment.min.js" type="text/javascript"></script>

    <!-- owlcarousel jQuery -->
    <script src="{{url('')}}/web_assets/js/owl.carousel.min.js"></script>
  	<!-- END CORE PLUGINS -->
	<script src="{{url('')}}/dash-assets/plugins/axios/axios.min.js"></script>

    <!-- ziehharmonika jQuery -->
    <script src="{{url('')}}/web_assets/js/ziehharmonika.js"></script>

    <!-- Fontawesome Script -->
    <script src="https://kit.fontawesome.com/7749c9f08a.js"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{url('')}}/dash-assets/global/plugins/moment.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="{{url('')}}/dash-assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{url('')}}/dash-assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{url('')}}/dash-assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <!-- Custom jQuery -->
    <script src="{{url('')}}/web_assets/js/scripts.js"></script>
	<script type="text/javascript" src="{{url('')}}/dash-assets/plugins/parsleyjs/parsley.min.js"></script>
	@yield('custom-script')
    <!-- Scroll-Top button -->
    <a href="#" class="scrolltotop"><i class="fas fa-angle-up"></i></a>
	<script>
		$(document).ready(function () {
			$('form').parsley();


		});



			// SCROLL SPY¿÷
			$(document).ready(function(){    
		$("#takeContactSection").click(function() {
		  $('html,body').animate({
			scrollTop: $("#contactSectiontop").offset().top}, 'slow');
		});
				});
	</script>
		
    </body>
</html>
