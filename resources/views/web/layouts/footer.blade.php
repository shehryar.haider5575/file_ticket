<!-- footer-area start -->
<footer class="footer-area">
    <div class="container">
        <div class="footer-item">
            <div class="footer-item-inner">
                <ul>
                    <li><a href="#">Terms and Conditions </a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
                <p>©File Tickets 2020 All rights reserved</p>
            </div>
            <div class="footer-item-inner2">
                <ul>
                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- footer-area end -->
