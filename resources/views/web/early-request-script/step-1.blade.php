<script>
$('#step_1').submit(function(e){

    e.preventDefault();
    var $this = $(this);
    var icon_id   = $('.icon_id').val();
    var datefiled = $('.datefiled').val();
    var section   = Number($this.data('id'));
    var ticket_no = new Array();
    var valid = false;
    if((icon_id != null) && (datefiled != null)){
        $(".section1 ,.section2 ,.section3 ,.section4,.section5").addClass('hidden');

        $('input[name^=ticket_no]').map(function(idx, elem) {
            if($(elem).val() != null && $(elem).val() != ""){
                var incre = ++idx;
                ticket_no[idx] = $(elem).val();
                console.log(ticket_no[idx].length);
                if(ticket_no[idx].length != 8){
                    valid = true;
                }
                $('.section'+incre).removeClass('hidden');
                $('.ticket_no_image'+incre).html($(elem).val());

            }
        }).get();
        if(!valid){
            axios
                .post('{{route("early_step_1")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'patch',
                    icon_id: icon_id,
                    datefiled: datefiled,
                    ticket_no: ticket_no,
                })
                .then(function (responsive) {
                    hideBeforeActiveSection(section);
                    section = Number($this.data('id')) + 1;
                    hideAfterActiveSection(section);
                })
                .catch(function (error) {
                    console.log(error.response.data.message);
                    $('#errors1').append(`
                        <li style='list-style: arabic-indic;color:#ff5722;margin-top:10px;'>`+error.response.data.message+`</li>
                    `);
                });
        }
    }
});
</script>
