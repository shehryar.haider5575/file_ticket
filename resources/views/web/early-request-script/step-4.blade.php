<script>
    	$('#step_4').submit(function(e){

        e.preventDefault();
        var $this = $(this);
        var section 	    = 	Number($this.data('id'));

        var first_name 		= 	$('.first_name').val();
        var last_name 		= 	$('.last_name').val();
        var email 			= 	$('.email').val();
        // var card_no 		= 	$('.card_no').val();
        // var expiry_date 	= 	$('.expiry_date').val();
        var phone_no 		= 	$('.phone_no').val();
        var city 			= 	$('.city').val();
        var postal_code 	= 	$('.postal_code').val();
        var province 		= 	$('.province').val();
        var address 		= 	$('.address').val();
        var icon_id   		= 	$('.icon_id').val();
        var datefiled 		= 	$('.datefiled').val();
        var icon_id   		= 	$('.icon_id').val();
        var datefiled 		= 	$('.datefiled').val();
        var interpreter 	= 	$('.interpreter').val();
        var interpreter_check 	= 	$('.interpreter-check').val();

        var terms           =   $('#test3').val();
        var formData 		= 	new FormData();

        formData.append("_token", "{{csrf_token()}}");
        formData.append('first_name',first_name);
        formData.append('last_name',last_name);
        formData.append('email',email);
        // formData.append('card_no',card_no);
        // formData.append('expiry_date',expiry_date);
        formData.append('phone',phone_no);
        formData.append('city',city);
        formData.append('postal_code',postal_code);
        formData.append('province',province);
        formData.append('address',address);
        formData.append('icon_code',icon_id);
        formData.append('date',datefiled);
        formData.append('interpreter',interpreter);

        var attachment = '';
        $('input[name^=ticket_no]').map(function(idx, elem) {
            // return $(elem).val();
            if($(elem).val() != null && $(elem).val() != ""){
                var incre = ++idx;
                attachment = document.querySelector('#ticket_photo'+incre);

                formData.append('ticket_no[]',$(elem).val());
                if(attachment.files[0]){
                    formData.append('ticket_photo[]',attachment.files[0]);
                }

            }
        }).get();
        if(interpreter_check == "no"){
            if(terms != "" && terms != null){
                axios.post('{{route('early_step_4',"Early_Request")}}', formData, {
                    headers: {
                    'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function (responsive) {
                    $('#exampleModalCenter2').modal('show');
                })
                .catch(function (error) {
                    console.log(error);
                });

            }
        }else{
            if((interpreter != "" && interpreter != null) && (terms != "" && terms != null)){
                axios.post('{{route('early_step_4',"Early_Request")}}', formData, {
                    headers: {
                    'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function (responsive) {
                    $('#exampleModalCenter2').modal('show');
                })
                .catch(function (error) {
                    console.log(error);
                });

            }
        }
    });

</script>
