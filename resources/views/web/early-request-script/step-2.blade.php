<script>
    $('#step_2').submit(function(e){
        e.preventDefault();
        var $this 		= $(this);
        var section 	= Number($this.data('id'));	

        var formData 	= new FormData();
        var attachment = '';

        formData.append("_token", "{{csrf_token()}}");
        $('input[name^=ticket_no]').map(function(idx, elem) {
            // return $(elem).val();
            if($(elem).val() != null && $(elem).val() != ""){
                var incre = ++idx;
                attachment = document.querySelector('#ticket_photo'+incre);
                if(attachment.files[0]){
                    formData.append('ticket_photo[]',attachment.files[0]);
                }

            }
        }).get();
        console.log(attachment.files[0]);
        if((attachment.files[0] != null || attachment.files[0] != undefined)){ 

            axios.post('{{route('early_step_2')}}', formData, {
                headers: {
                'Content-Type': 'multipart/form-data'
                }
            })	
            .then(function (responsive) {
                hideBeforeActiveSection(section);
                section = Number($this.data('id')) + 1;
                hideAfterActiveSection(section);
            })
            .catch(function (error) {
                console.log(error);
                $('#errors2').append(`
                    <li style='list-style: arabic-indic;color:#ff5722;margin-top:10px;'>`+error.response.data.message+`</li>
                `);
            });
        }
    });
</script>