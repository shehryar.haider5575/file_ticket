<script>
    $('#step_3').submit(function(e){
			e.preventDefault();
			var $this = $(this);
			var section 	    = 	Number($this.data('id'));
			var first_name 		= 	$('.first_name').val();
			var last_name 		= 	$('.last_name').val();
			var email 			= 	$('.email').val();
			var phone_no 		= 	$('.phone_no').val();
			var city 			= 	$('.city').val();
			var postal_code 	= 	$('.postal_code').val();
			var province 		= 	$('.province').val();
			var address 		= 	$('.address').val();
			var icon_id   		= 	$('.icon_id').val();
			var datefiled 		= 	$('.datefiled').val();
        
            if((first_name != null && first_name != "") && (last_name != null && last_name != "")   && (email != null && email != "")  &&  (phone_no != null && phone_no != "") && (city != null && city != "") && (postal_code != null && postal_code != "") && (province != null && province != "") && (address != null && address != "")){ 

            axios
				.post('{{route("early_step_3")}}', {
					_token		: '{{csrf_token()}}',
					_method		: 'patch',
					first_name 	: first_name,
					last_name 	: last_name,
					email 		: email,
					// card_no 	: card_no,
					// expiry_date : expiry_date,
					province 	: province,
					address 	: address,
					phone_no 	: phone_no,
					postal_code : postal_code,
					city 		: city,
					address 	: address,
				})
				.then(function (responsive) {
					$('.offence_date_label').html(datefiled);
					$('.icon_id_label').html(icon_id);
					$('.first_name_label').html(first_name);
					$('.last_name_label').html(last_name);
					$('.email_label').html(email);
					$('.phone_no_label').html(phone_no);
					$('.city_label').html(city);
					$('.postal_code_label').html(postal_code);
					$('.province_label').html(province);
					$('.address_label').html(address);
					$('input[name^=ticket_no]').map(function(idx, elem) {
						if($(elem).val() != null && $(elem).val() != ""){
							var incre = ++idx;
							$('#total_tickets').append(`
								<div class="banner-item9-inner5">
									<h6>Ticket # `+incre+`</h6>
									<h5>`+$(elem).val()+`</h5>
								</div>
							`);
						}
					}).get();

					hideBeforeActiveSection(section);
					section = Number($this.data('id')) + 1;
					hideAfterActiveSection(section);
				})
				.catch(function (error) {
					console.log(error);
				});
            }
    });
</script>