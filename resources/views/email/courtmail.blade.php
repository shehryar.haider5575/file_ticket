@component('mail::message')
# You Received An Application For A Ticket  From {{ config('app.name') }}
# Ticket No:
{{$ticket}}
# Name:
{{$name}}

Please See Attach File .
@component('mail::button', ['url' => env('APP_URL')])
For Details Visit Our Website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
