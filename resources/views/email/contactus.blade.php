@component('mail::message')
# Contact Us
## {{"Name: "}} {{$name}}

## {{"Subject: "}}
{{$subject}}
## {{"Message: "}}
{{$message}}
## {{"From: "}}
{{$email}}
@component('mail::button', ['url' => env('APP_URL')])
Vist Website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
