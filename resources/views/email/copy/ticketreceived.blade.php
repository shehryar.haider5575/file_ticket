@component('mail::message')
# Ticket Received

Icon Code: {{$icon}}<br><br>
Ticket 1 : {{$ticketno1}}<br><br>
@if($ticketno2)
Ticket 2 : {{$ticketno2}}
@endif

@if($ticketno3)
Ticket 3 : {{$ticketno3}}
@endif

@if($ticketno4)
Ticket 4 : {{$ticketno4}}
@endif

@if($ticketno5)
Ticket 5 : {{$ticketno5}}
@endif

## we will contact you as soon as possible
@component('mail::button', ['url' => env('APP_URL')])
Visit Our Website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
