@component('mail::message')
{{-- {{dd($mail_items)}} --}}
# Ticket Received
Icon Code: {{$mail_items['icon']}}<br><br>
@if ($mail_items['type'] == 'payticket')
    {{$mail_items['ticket_type']}} Ticket # : {{$mail_items['tickets']}}<br><br>
@else
    @foreach ($mail_items['tickets'] as $key => $ticket)
    Ticket {{++$key}} : {{$ticket}}<br><br>
    @endforeach
@endif

{{-- @if($ticket_no1)
Ticket 2 : {{$ticket_no1}}
@endif
@if($ticket_no2)
Ticket 3 : {{$ticket_no2}}
@endif
@if($ticket_no3)
Ticket 4 : {{$ticket_no3}}
@endif
@if($ticket_no4)
Ticket 5 : {{$ticket_no4}}
@endif --}}

<p style="font-size: 12px; color: #000;"><b>The above noted Ticket(s) are being processed & filed with Court Services.<br>
Confirmation will be sent to you via E-mail once it’s been served & received.</b></p>
<b><u style="font-size: 12px; color: #000;">‘Guide For Defendants In Provincial Offences Cases ’ </u><u>  <a href="https://www.ontariocourts.ca/ocj/files/guides/guide-provincial-offences.pdf">LINK</a> </u></b>
<br>
<p style="font-size: 10px; color: #000;">This Guide provides defendants with general information about the court process for provincial offences cases. It does not cover every circumstance that might arise in your case. This is <b><u>NOT</u></b> legal advice from FileTickets.ca</p>
<b><u style="font-size: 12px; color: #000;">Legal Consultation </u></b>
<p style="font-size: 10px; color: #000;">Once your ticket(s) is filed by FileTickets.ca, we will have a local Licensed Legal Representative provide you a Free Consultation via telephone so you understand the Court process.</p>
<p style="font-size: 10px; color: #000;">There is <b>NO</b> obligation to hire a Legal Representative. You may decline the Consultation and seek your own advice from any Paralegal or Lawyer.</p>
<p style="font-size: 10px; color: #000;">You are strongly urged to get legal advice from a Lawyer or Paralegal about your legal options and the possible penalties you could face. If you plan to represent yourself, consider:</p>
• <span style="font-size: 10px; color: #000;">The charge you are facing,</span><br>
• <span style="font-size: 10px; color: #000;">The complexity of the case,</span><br>
• <span style="font-size: 10px; color: #000;">Your understanding of the legal process and the issues, and</span> <br>
• <span style="font-size: 10px; color: #000;">The risk of a substantial fine, jail time or other penalty that would have significant<br> personal impact (for example, driving demerit points, driver’s licence suspension). </span>
<br>

<br>
<b><u style="font-size:12px; color: #000;">Notice Of Trial</u></b>

<span style="font-size:10px; color: #000;">The Court Office may send you a <b><u>‘Notice of Trial’ Via Email or by regular mail.</u></b>
Please ensure that your information stays up to date and is accessible. Should your contact information change please <u><b>notify the COURT directly.</b> </u><span>

<p style="color: #000;">Thanks,</p>

<p style="color: #000;">{{ config('app.name') }}</p>
@component('mail::button', ['url' => 'https://www.filetickets.ca/'])
Visit Our Website
@endcomponent
<span>
<u style="color: blue; font-size:10px;"><b>Our Service</b></u>
<p style="color: blue; font-size: 8px">FileTickets.ca is an independent Process server company & is <u><b>NOT</b></u> affiliated or a subsidiary company to the Government of Canada or Province of Ontario.
Our company does <u><b>NOT</b></u> provide Legal advice and is <u><b>NOT</b></u> a Legal Services Company.
For information or Frequently Asked Questions Please visit our Website</p></span>

@endcomponent
