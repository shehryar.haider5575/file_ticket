@component('mail::message')
# Your Ticket {{$ticket}} Has Been Filed On {{$datefiled}}

# Comments:
{{$comments}}

The court will send you trial date etc. also a legal services provider will contact you for free consultation.
@component('mail::button', ['url' => env('APP_URL')])
Visit Out Website
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
