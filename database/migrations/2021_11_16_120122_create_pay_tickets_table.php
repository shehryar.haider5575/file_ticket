<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('offence_date');
            $table->string('icon_code',4);
            $table->string('ticket_no',6);
            $table->string('card_holder_name');
            $table->string('card_no');
            $table->string('card_expiry_month');
            $table->string('card_expiry_year');
            $table->string('cvv');
            $table->string('email');
            $table->string('postal_code');
            $table->double('fine_amount');
            $table->double('convenience_amount');
            $table->double('total_amount');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_tickets');
    }
}
