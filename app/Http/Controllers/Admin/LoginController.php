<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Hash;

class LoginController extends Controller
{

    public function loginView(){
        return view('admin.login');
    }

    public function registerView(){
        
        return view('admin.register');
    }

    public function logout(Request $request){
        
        Auth::guard('admin')->logout();
        return back();
    }

    public function login(Request $request){

        $validator = Validator::make($request->all(),[
            "email"     => 'required|email|exists:users,email',
            "password"  => "required"
        ]);
        if($validator->fails()){
            return back()->with('error','Invalid Credentials');
        }
        $userdata = array(
            'password'  => $request->password,
            'email'     => $request->email
        );
        // return $userdata;
        // dd(Auth::guard('admin')->login(User::where('email', $request->email)->first()));
        
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            Auth::guard('admin')->login(User::where('email', $request->email)->first());
            
            return redirect()->route("home");
        }
        return back()->with('error','Invalid Credentials');
    }

    public function register(Request $request){
        
        $data = $request->validate([
            "name"      =>  'required|string',
            "email"     =>  'required|email|unique:users,email',
            "password"  =>  "required|confirmed"
        ]);
        $data['password'] = Hash::make($request->password);
        
        $user = User::create($data);
        event(new \Illuminate\Auth\Events\Registered($user));
        Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password]);

        return redirect()->route('home');
    }
}
