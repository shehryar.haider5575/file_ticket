<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Contact;
use App\Models\Tickets;
use App\Models\IconList;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Mail\TicketFiled;
use App\Mail\TicketReceived;
use App\Mail\ContactUs;
use App\Mail\CourtFiled;
use App\Mail\DiscFiled;
use App\Exports\UsersExport;
use App\Exports\UsersHistoryExport;
use App\Exports\UsersHistoryPayTicketExport;
use Carbon\Carbon;
use DataTables;
use Storage;
use PDF;
use File;
class TrialRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($type)
    {
        switch ($type) {
            case 'Early_Request':
                return view('admin.early_request.early_request');
                break;
            case 'Trial_Request':
                return view('admin.tral_request.trial_request');

                break;

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable($type)
    {
        // return $type;
        $trial_request = Contact::with('trial_request_tickets')->whereHas('trial_request_tickets',function($query){
            return $query->where('completed',0);
        })->where('type',$type)->orderBy('created_at','desc');
        // $trial_request = Contact::with('trial_request_tickets')->where('type',$type)->orderBy('created_at','desc');
        return DataTables::of($trial_request)->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function potentialDatatable($type)
    {
        $trial_request = Contact::with('trial_request_tickets')->where([['type',$type],['potential_status',1]])->orderBy('created_at','desc');
        return DataTables::of($trial_request)->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function callbackDatatable($type)
    {
        $trial_request = Contact::with('trial_request_tickets')->where([['type',$type],['client_status',1]])->orderBy('created_at','desc');
        return DataTables::of($trial_request)->make();
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trialHistory($type)
    {
        switch ($type) {
            case 'Early_Request':
                return view('admin.early_request_history.early_request_history');
                break;
            case 'Trial_Request':
                return view('admin.tral_request_history.trial_request_history');

                break;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historyDatatable($type)
    {
        // $trial_history_request = Contact::with('trial_request_tickets')->where('type',$type)->whereHas('trial_request_tickets',function($query){
        //     $query->whereNotNull('emaildateticket');

        // })->get();
        $trial_history_request = Contact::with('trial_request_tickets')->whereHas('trial_request_tickets',function($query){
            return $query->where('completed',1);
        })->where('type',$type)->orderby('id','desc')->get();
        return DataTables::of($trial_history_request)->make();

    }

    /**
     * Display a listing of the resource.
     * @return RenderableTicketReceived
    */
    public function closeTicket($type,Contact $contact){

        $file_not_completed = Tickets::whereIn('id',$contact->trial_request_tickets->pluck('id')->toArray())->get();
        if(!empty($file_not_completed))
        {
            return redirect()->back()->with('error','cant close, please complete ticket details');
        }
        foreach($contact->trial_request_tickets as $ticket){
            $ticket->update(['completed'=>1,'datefiled'=>\Carbon\Carbon::now()]);
        }

        return redirect()->route('trial_request_dashboard',$type)->with('fileclose', 'File CLose!');
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function view($type, Contact $contact)
    {
        switch ($type) {
            case 'Early_Request':
                return view('admin.early_request.early_request_detail',compact('contact'));
                break;
            case 'Trial_Request':
                return view('admin.tral_request.trial_request_detail',compact('contact'));
                break;

        }
    }

    public function editticket($type, Contact $contact){

        $iconlist= IconList::get();

        switch ($type) {
            case 'Early_Request':
                return view('admin.early_request.edit_early_request',compact('contact','iconlist'));
                break;
            case 'Trial_Request':
                return view('admin.tral_request.edit_trial_request',compact('contact','iconlist'));
                break;

        }

    }

    public function ticketimageupdate($type, Tickets $ticket ,Request $request){

        $data = [
            'ticket_no' => $request->ticket_no,
        ];
        if ($request->images) {
            Storage::disk('file')->delete($ticket->images_name);
            $data['images_name'] = Storage::disk('file')->putFile('',$request->images);
        }
        $ticket->update($data);
        return redirect()->back()->with('success','File record has updated!...');

    }

    public function updateticket($type, Tickets $ticket ,Request $request){

        $data = $request->all();
        if($request->images){
            Storage::disk('file')->delete($ticket->images_name);
            $data['images_name'] = Storage::disk('file')->putFile('',$request->images);

        }
        $ticket->update($data);
        return redirect()->route('trial_request_dashboard',$type)->with('success','File record has updated!...');
    }

    public function updateContact($type, Contact $contact ,Request $request){

        $contact->update($request->all());
        return redirect()->back()->with('success','File record has updated!...');
    }

    public function clientStatus(Request $request){

        $contact = Contact::where('id',$request->id)->first();
        // return $request->all();
        if($request->type == 'callback'){
            $contact->update(['client_status'=>$request->status]);
        }else{
            $contact->update(['potential_status'=>$request->status]);
        }
        // $contact->update(['client_status'=>$request->status]);

        return response()->json([
            'success' => 'Status Updated Successfully'
        ],200);
    }

    public function mailToCourt($type, Tickets $ticket)
    {
        // dd("dasdfasdfafd");

        $icon=Tickets::findOrFail($ticket->id)->contact->icon;

        $mailcode=$icon->icon_code;
        // die();
        $iconmail= IconList::where('icon_code',$mailcode)->first();

        if(isset($iconmail)){
            $icon_list=IconList::where('icon_code',$icon->icon_code)->first();

            if($icon_list->form_used=='RMIF'){

                $wordFile=new TemplateProcessor(public_path('').'/word/secondform.docx');

            }elseif($icon_list->form_used=='NIA-2'){
                $wordFile=new TemplateProcessor(public_path('').'/word/NIA EDIT update 2.docx');

            }else{
                $wordFile=new TemplateProcessor(public_path('').'/word/Notice of Intention to Appear.docx');
            }
            // return $wordFile;
            $ticket->emaildateticket=Carbon::now()->toDateTimeString();
            $ticket->save();
            $ticket->contact->emaildate=Carbon::now()->toDateTimeString();
            $ticket->contact->save();
            
            // $wordFile=new TemplateProcessor('word/Notice of Intention to Appear.docx');
            $name=$ticket->contact->first_name." ".$ticket->contact->last_name;
            $wordFile->setValue('name',$name );
            $wordFile->setValue('address',$ticket->contact->address);
            $wordFile->setValue('province',$ticket->contact->province);
            $wordFile->setValue('postal_code',$ticket->contact->postal_code);
            $wordFile->setValue('ticket_no',$ticket->ticket_no);
            $wordFile->setValue('icon_code',$mailcode);
            $wordFile->setValue('date',$icon->date);
            $wordFile->setValue('email',$ticket->contact->email);
            $wordFile->setValue('city',$ticket->contact->city);
            $wordFile->setValue('phone',$ticket->contact->phone);
            $wordFile->setValue('currentDate', date("Y-m-d"));
            $wordFile->setImageValue('ticketimage',array('path' =>public_path().'/uploads/file/'.$ticket->images_name , 'width' => 600, 'height' => 900, 'ratio' => true));
            $wordFile->setValue('interpreter',$icon->interpreter);
            $fileName='attachfile';
            $wordFile->saveAs($fileName.'.docx');
            $subject='Trail request:'.$ticket->contact->first_name.$ticket->contact->last_name.' #'.$icon->icon_code.'-'.$ticket->ticket_no;
            $tickete=$ticket->ticket_no;
            
            $icon_mail=$iconmail->email;
            $contact=$icon_mail;
            $datefiled = '123';
            $image = $ticket->images_name;
            // $mail->setDateFiled('123');
            // $mail->setName($name);
            // $mail->setImagePath('new');
            // dd($contact);
            // Shehryar Haider
            
            // Mail::to(env('SEND_MAIL_TO'))->cc('Records@filetickets.ca')->send($mail);
            
            // dd($ticket);
            
            // dd($tickete);
            Mail::to(env('SEND_MAIL_TO'))->cc($ticket->contact->email)->bcc('dannyfreitas@live.com')->send(new CourtFiled($tickete,$subject));
            // dd(env('SEND_MAIL_TO'));
            // $email = Mail::to(env('SEND_MAIL_TO'))->send(new CourtFiled($ticket,$subject));
            // $email = Mail::to($icon_mail)->send(new CourtFiled($ticket,$subject));

            $doco='attachfile.docx';
            // dd($doco);
            if (File::exists($doco)) {
                unlink($doco);

            }
            
            return redirect()->back()->with('emailsent', 'Mail Sent To Court!');

        }
        else{
            return redirect()->back()->with('emaildidntsent',"Mail Didn't Sent!");
        }
    }

    public function wordExport($type, Tickets $ticket){

        $icon_list=IconList::where('icon_code',$ticket->contact->icon_code)->first();

        if($icon_list->form_used=='RMIF'){
            $wordFile=new TemplateProcessor('word/secondform.docx');

        }elseif($icon_list->form_used=='NIA-2'){
            $wordFile=new TemplateProcessor('word/NIA EDIT update 2.docx');

        }else{
            $wordFile=new TemplateProcessor('word/Notice of Intention to Appear.docx');
        }

        $name=$ticket->contact->first_name." ".$ticket->contact->last_name;
        $wordFile->setValue('name',$name );
        $wordFile->setValue('address',$ticket->contact->address);
        $wordFile->setValue('province',$ticket->contact->province);
        $wordFile->setValue('email',$ticket->contact->email);
        $wordFile->setValue('postal_code',$ticket->contact->postal_code);
        $wordFile->setValue('ticket_no',$ticket->ticket_no);
        $wordFile->setValue('icon_code',$icon_list->icon_code);
        $wordFile->setValue('date',$ticket->contact->date);
        $wordFile->setValue('email',$ticket->contact->email);
        $wordFile->setValue('city',$ticket->contact->city);
        $wordFile->setValue('phone',$ticket->contact->phone);
        $wordFile->setValue('currentDate', date("Y-m-d"));
        $test=public_path().'/uploads/file/'.$ticket->images_name;


        if (File::exists($test)){
            $wordFile->setImageValue('ticketimage',array('path' =>public_path().'/uploads/file/'.$ticket->images_name , 'width' => 600, 'height' => 900, 'ratio' => true));

        }else{
            $wordFile->setImageValue('ticketimage',array('path' =>public_path().'/uploads/file/FILETICKETS4-05.png' , 'width' => 600, 'height' => 900, 'ratio' => true));
        }

        $wordFile->setValue('interpreter',$ticket->contact->interpreter);
        $fileName=$ticket->contact->first_name.'-'.$ticket->contact->last_name.'-'.$ticket->contact->icon_code.'-'.$ticket->ticket_no;
        $wordFile->saveAs($fileName.'.docx');
        return response()->download($fileName.'.docx')->deleteFileAfterSend(true);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
    */
    public function updateTrialRequestTicketDetail($type, Request $request, Tickets $ticket){

        $data = [
            'personal_comments' => $request->pcomments,
        ];

        $ticket->update($data);
        return redirect()->route('trial_request_dashboard.view',[$type,$ticket->contact_id]);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
    */
    public function destroyticket($type, Tickets $ticket){

        if (isset($ticket->images_name) && !empty($ticket->images_name)) {
            Storage::disk('file')->delete($ticket->images_name);
        }
        if (isset($ticket->attached_image) && !empty($ticket->attached_image)) {
            Storage::disk('file')->delete($ticket->attached_image);
        }
        $ticket->delete();
        return redirect()->back();
    }

    public function exportHistory($type, $entries){

        $name='History_'.date('M-d-Y_hia').'.xlsx';
        return Excel::download(new UsersHistoryExport($type,$entries), $name);
    }

    public function exportPayTicketHistory($type, $entries){

        $name='History_'.date('M-d-Y_hia').'.xlsx';
        return Excel::download(new UsersHistoryPayTicketExport($type,$entries), $name);
    }

    public function mailToDisc($ticket_id)
    {
        // $icon=Tickets::findOrFail($ticket_id)->icon;
        $ticket=Tickets::findOrFail($ticket_id);
        $icon=$ticket->contact->icon_code;
        $contact= $ticket->contact;
        // dd($icon,$contact,$ticket);

        $mailcode=$contact->icon_code;
        $iconmail=\App\Models\IconList::where('icon_code',$mailcode)->first();
        if(isset($iconmail)){
            // $icon_list=icon_list::where('icon_code',$contact->icon_code)->first();
            if($iconmail->form_used=='NIA'){
                $wordFile=new TemplateProcessor('word/DISC.docx');
            }else{
                $wordFile=new TemplateProcessor('word/DISC.docx');
            }
            $contact->emaildate=Carbon::now()->toDateTimeString();
            $contact->save();
            // $wordFile=new TemplateProcessor('word/Notice of Intention to Appear.docx');
            $name=$contact->first_name." ".$contact->last_name;
            $wordFile->setValue('name',$name );
            $wordFile->setValue('address',$contact->address);
            $wordFile->setValue('province',$contact->province);
            $wordFile->setValue('postal_code',$contact->postal_code);
            $wordFile->setValue('ticket_no',$ticket->ticket_no);
            $wordFile->setValue('icon_code',$ticket->icon_code);
            $wordFile->setValue('date',$contact->date);
            $wordFile->setValue('email',$contact->email);
            $wordFile->setValue('city',$contact->city);
            $wordFile->setValue('phone',$contact->phone);
            $wordFile->setValue('currentDate', date("Y-m-d"));
            $wordFile->setImageValue('ticketimage',array('path' =>public_path().'/uploads/file/'.$ticket->images_name , 'width' => 600, 'height' => 900, 'ratio' => true));
            $wordFile->setValue('interpreter',$contact->interpreter);
            // $fileName='attachfile';
            $fileName=$contact->first_name.'-'.$contact->last_name.'#'.$contact->icon_code.'-'.$ticket->ticket_no;
            $wordFile->saveAs($fileName.'.docx');
            $subject='DISCLOSURE REQUEST:'.$contact->first_name.$contact->last_name.' #'.$contact->icon_code.'-'.$ticket->ticket_no;
            $ticket=$ticket->ticket_no;
            $icon_mail=$iconmail->email;
            $contact=$icon_mail;
            // $mail = new DiscFiled($ticket,$subject);
            // $mail->setDateFiled($fileName);
            // $mail->setName($name);
            // $mail->setImagePath($fileName);

            // Mail::to('danny@gtatraffictickets.com')->send($mail);
            Mail::to(env('SEND_MAIL_TO'))->send(new DiscFiled($ticket,$subject,$fileName,$name));
            // Mail::to($icon_mail)->send(new DiscFiled($ticket,$subject,$fileName,$name));

            // Mail::to('shehryarhaider0316@gmail.com')->send($mail);
            // Mail::to('mrayazsiyal@gmail.com')->send($mail);
            $doco=$fileName.'.docx';
            if (File::exists($doco)) {
                unlink($doco);

            }
            return redirect()->back()->with('emailsent', 'DISC Request Sent!');

        }
        else{
            return redirect()->back()->with('emaildidntsent',"DISC Request Didn't Sent!");
        }



    }

    public function filedTicketMail(Request $request,$id)
    {
        $ticket= Tickets::findOrFail($id);
        $icon=$ticket->contact;

        $contact=$ticket->contact;

        $mailcode=$icon->icon_code;

        $iconmail=IconList::where('icon_code',$mailcode)->first();

        if(\request('completed')==1){

            if(\request('date')){

                if(\request('comments')){

                    if ($request->hasFile('images')) {

                        $image = $request->file('images');
                        $imageName=time().$image->getClientOriginalName();
                        $image->move(public_path()."/uploads/email/", $imageName);
                        $ticket->datefiled=\request('date');
                        $ticket->comments=\request('comments');
                        // $ticket->personal_comments=\request('pcomments');
                        $ticket->completed=\request('completed');
                        $ticket->datefiled=\Carbon\Carbon::now();
                        $ticket->attached_image = $imageName;
                        $ticket->save();

                        $mail = new TicketFiled($ticket->ticket_no);
                        $mail->setDateFiled(\request('date'));
                        $mail->setComments(\request('comments'));
                        $mail->setImagePath($imageName);
                        Mail::to(env('SEND_MAIL_TO'))->send($mail);
                        // Mail::to($contact->email)->send($mail);
                        return redirect()->back()->with('emailsent', 'Mail Sent!');
                    }

                    return redirect()->back()->with('emailattachment', 'Attachment Required!');
                }
                return redirect()->back()->with('emailcomments', 'Comments Required!');
            }
            return redirect()->back()->with('emaildate', 'Date Required!');
        }
        return redirect()->back()->with('emaildidntsent',"Mail Didn't Sent!");
    }
}
