<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use DataTables;
use Auth;
use App\Models\PayTicket;
use App\Models\IconList;
use App\Models\Contact;
use App\Models\Tickets;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpWord\TemplateProcessor;
use App\Mail\TicketFiled;
use App\Mail\TicketReceived;
use App\Mail\ContactUs;
use App\Mail\CourtFiled;
use App\Mail\DiscFiled;
use App\Exports\UsersExport;
use App\Exports\UsersHistoryExport;
use App\Exports\UsersHistoryPayTicketExport;
use Carbon\Carbon;
use Storage;
use PDF;
use File;

class PayTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index($type)
    {
        $data = [
            'type'  =>  $type,
        ];
        return view('admin.payticket.payticket',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatable($type)
    {
        $tickets_request = PayTicket::where('type','LIKE','%'.$type.'%')->where('completed',0)->orderBy('created_at','desc');
        return DataTables::of($tickets_request)->make();
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function history($type)
    {
        $data = [
            'type'  =>  $type,
        ];
        return view('admin.payticket_history.payticket_history',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function historyDatatable($type)
    {
        $tickets_request = PayTicket::where('type','LIKE','%'.$type.'%')->where('completed',1)->orderBy('created_at','desc');
        return DataTables::of($tickets_request)->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function potentialDatatable($type)
    {
        $tickets_request = PayTicket::where([['type','LIKE','%'.$type.'%'],['potential_status',1]])->orderBy('created_at','desc');
        return DataTables::of($tickets_request)->make();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function callbackDatatable($type)
    {
        $tickets_request = PayTicket::where([['type','LIKE','%'.$type.'%'],['client_status',1]])->orderBy('created_at','desc');
        return DataTables::of($tickets_request)->make();
    }

    public function clientStatus(Request $request){

        $contact = PayTicket::where('id',$request->id)->first();
        // return $request->all();
        if($request->type == 'callback'){
            $contact->update(['client_status'=>$request->status]);
        }else{
            $contact->update(['potential_status'=>$request->status]);
        }

        return response()->json([
            'success' => 'Status Updated Successfully'
        ],200);
    }

    /**
     * Display a listing of the resource.
     * @return RenderableTicketReceived
    */
    public function closeTicket($type,PayTicket $payticket){

        $file_not_completed = $payticket->whereNull(['comments','attached_image','datefiled'])->get();
        if(!empty($file_not_completed))
        {
            return redirect()->back()->with('error','cant close, please complete ticket details');
        }
        $payticket->update(['completed'=>1]);

        return redirect()->route('pay_ticket',$type)->with('fileclose', 'File CLose!');
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function view($type, PayTicket $payticket)
    {
        return view('admin.payticket.payticket_detail',compact('type','payticket'));
    }

    public function edit($type, PayTicket $payticket){

        $iconlist= IconList::get();

        return view('admin.payticket.edit_payticket',compact('iconlist','type','payticket'));
    }

    public function ticketimageupdate($type, PayTicket $payticket ,Request $request){

        $data = [
            'ticket_no' => $request->ticket_no,
        ];
        if ($request->images) {
            Storage::disk('file')->delete($payticket->images_name);
            $data['images_name'] = Storage::disk('file')->putFile('',$request->images);
        }
        $payticket->update($data);
        return redirect()->back()->with('success','File record has updated!...');

    }

    public function updateticket($type, PayTicket $payticket ,Request $request){

        $payticket->update($request->all());
        return redirect()->route('trial_request_dashboard',$type)->with('success','File record has updated!...');
    }

    public function mailToCourt($type, PayTicket $payticket)
    {
        $icon=$payticket->icon;

        $mailcode=$icon->icon_code;
        // die();
        $iconmail= IconList::where('icon_code',$mailcode)->first();

        if(isset($iconmail)){
            $icon_list=IconList::where('icon_code',$icon->icon_code)->first();

            if($icon_list->form_used=='RMIF'){

                $wordFile=new TemplateProcessor(public_path('').'/word/secondform.docx');

            }elseif($icon_list->form_used=='NIA-2'){
                $wordFile=new TemplateProcessor(public_path('').'/word/NIA EDIT update 2.docx');

            }else{
                $wordFile=new TemplateProcessor(public_path('').'/word/Notice of Intention to Appear.docx');
            }
            $payticket->emaildateticket=Carbon::now()->toDateTimeString();
            $payticket->save();
            $payticket->emaildate=Carbon::now()->toDateTimeString();
            $payticket->save();
            // $wordFile=new TemplateProcessor('word/Notice of Intention to Appear.docx');
            $name=$payticket->card_holder_name;
            $wordFile->setValue('name',$name );
            $wordFile->setValue('postal_code',$payticket->postal_code);
            $wordFile->setValue('ticket_no',$payticket->ticket_no);
            $wordFile->setValue('icon_code',$mailcode);
            $wordFile->setValue('date',$icon->offence_date);
            $wordFile->setValue('email',$payticket->email);
            $wordFile->setValue('currentDate', date("Y-m-d"));
            $wordFile->setImageValue('ticketimage',array('path' =>public_path().'/uploads/file/'.$payticket->images_name , 'width' => 600, 'height' => 900, 'ratio' => true));
            $fileName='attachfile';
            $wordFile->saveAs($fileName.'.docx');
            $subject='Trail request:'.$payticket->card_holder_name.' #'.$icon->icon_code.'-'.$payticket->ticket_no;
            $paytickete=$payticket->ticket_no;

            $icon_mail=$iconmail->email;
            $contact=$icon_mail;
            $datefiled = '123';
            $image = 'new';
            // $mail->setDateFiled('123');
            // $mail->setName($name);
            // $mail->setImagePath('new');
            // dd($contact);
            // Shehryar Haider

            // Mail::to(env('SEND_MAIL_TO'))->cc('Records@filetickets.ca')->send($mail);

// dd($payticket);
            Mail::to(env('SEND_MAIL_TO'))->cc($payticket->email)->send(new CourtFiled($paytickete,$subject));
            // dd(env('SEND_MAIL_TO'));
            // $email = Mail::to(env('SEND_MAIL_TO'))->send(new CourtFiled($ticket,$subject));
            // $email = Mail::to($icon_mail)->send(new CourtFiled($ticket,$subject));

            $doco='attachfile.docx';
            // dd($doco);
            if (File::exists($doco)) {
                unlink($doco);

            }
            return redirect()->back()->with('emailsent', 'Mail Sent To Court!');

        }
        else{
            return redirect()->back()->with('emaildidntsent',"Mail Didn't Sent!");
        }
    }

    public function wordExport($type, PayTicket $payticket){

        $icon_list=IconList::where('icon_code',$payticket->icon_code)->first();

        if($icon_list->form_used=='RMIF'){
            $wordFile=new TemplateProcessor('word/secondform.docx');

        }elseif($icon_list->form_used=='NIA-2'){
            $wordFile=new TemplateProcessor('word/NIA EDIT update 2.docx');

        }else{
            $wordFile=new TemplateProcessor('word/Notice of Intention to Appear.docx');
        }

        $name=$payticket->card_holder_name;
        $wordFile->setValue('name',$name );
        $wordFile->setValue('email',$payticket->email);
        $wordFile->setValue('postal_code',$payticket->postal_code);
        $wordFile->setValue('ticket_no',$payticket->ticket_no);
        $wordFile->setValue('icon_code',$icon_list->icon_code);
        $wordFile->setValue('date',$payticket->offence_date);
        $wordFile->setValue('email',$payticket->email);
        $wordFile->setValue('currentDate', date("Y-m-d"));
        $test=public_path().'/uploads/file/'.$payticket->images_name;


        if (File::exists($test)){
            $wordFile->setImageValue('ticketimage',array('path' =>public_path().'/uploads/file/'.$payticket->images_name , 'width' => 600, 'height' => 900, 'ratio' => true));

        }else{
            $wordFile->setImageValue('ticketimage',array('path' =>public_path().'/uploads/file/FILETICKETS4-05.png' , 'width' => 600, 'height' => 900, 'ratio' => true));
        }

        // $wordFile->setValue('interpreter',$payticket->interpreter);
        $fileName=$payticket->card_holder_name.'-'.$payticket->icon_code.'-'.$payticket->ticket_no;
        $wordFile->saveAs($fileName.'.docx');
        return response()->download($fileName.'.docx')->deleteFileAfterSend(true);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
    */
    public function updateTrialRequestTicketDetail($type, Request $request, PayTicket $payticket){

        $data = [
            'personal_comments' => $request->pcomments,
        ];

        $payticket->update($data);
        return redirect()->route('pay_ticket.view',[$type,$payticket->id]);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
    */
    public function destroyticket($type, PayTicket $payticket){

        if (isset($payticket->images_name) && !empty($payticket->images_name)) {
            Storage::disk('file')->delete($payticket->images_name);
        }
        if (isset($payticket->attached_image) && !empty($payticket->attached_image)) {
            Storage::disk('file')->delete($payticket->attached_image);
        }
        $payticket->delete();
        return redirect()->back();
    }

    public function exportHistory(){

        $name='History_'.date('M-d-Y_hia').'.xlsx';
        return Excel::download(new UsersHistoryExport, $name);
    }

    public function mailToDisc($ticket_id)
    {
        // $icon=Tickets::findOrFail($ticket_id)->icon;
        $ticket=PayTicket::findOrFail($ticket_id);
        $icon=$ticket->icon_code;
        $contact= $ticket;
        // dd($icon,$contact,$ticket);

        $mailcode=$contact->icon_code;
        $iconmail=\App\Models\IconList::where('icon_code',$mailcode)->first();
        if(isset($iconmail)){
            // $icon_list=icon_list::where('icon_code',$contact->icon_code)->first();
            if($iconmail->form_used=='NIA'){
                $wordFile=new TemplateProcessor('word/DISC.docx');
            }else{
                $wordFile=new TemplateProcessor('word/DISC.docx');

            }
            $contact->emaildate=Carbon::now()->toDateTimeString();
            $contact->save();
            // $wordFile=new TemplateProcessor('word/Notice of Intention to Appear.docx');
            $name=$contact->card_holder_name;
            $wordFile->setValue('name',$name );
            $wordFile->setValue('postal_code',$contact->postal_code);
            $wordFile->setValue('ticket_no',$ticket->ticket_no);
            $wordFile->setValue('icon_code',$ticket->icon_code);
            $wordFile->setValue('date',$contact->offence_date);
            $wordFile->setValue('email',$contact->email);
            $wordFile->setValue('currentDate', date("Y-m-d"));
            $wordFile->setImageValue('ticketimage',array('path' =>public_path().'/uploads/file/'.$ticket->images_name , 'width' => 600, 'height' => 900, 'ratio' => true));
            // $wordFile->setValue('interpreter',$contact->interpreter);
            // $fileName='attachfile';
            $fileName=$contact->card_holder_name.'#'.$contact->icon_code.'-'.$ticket->ticket_no;
            $wordFile->saveAs($fileName.'.docx');
            $subject='DISCLOSURE REQUEST:'.$contact->card_holder_name.' #'.$contact->icon_code.'-'.$ticket->ticket_no;
            $ticket=$ticket->ticket_no;
            $icon_mail=$iconmail->email;
            $contact=$icon_mail;
            // $mail = new DiscFiled($ticket,$subject);
            // $mail->setDateFiled($fileName);
            // $mail->setName($name);
            // $mail->setImagePath($fileName);

            // Mail::to('danny@gtatraffictickets.com')->send($mail);
            Mail::to($icon_mail)->send(new DiscFiled($ticket,$subject,$fileName,$name));

            // Mail::to('shehryarhaider0316@gmail.com')->send($mail);
            // Mail::to('mrayazsiyal@gmail.com')->send($mail);
            $doco=$fileName.'.docx';
            if (File::exists($doco)) {
                unlink($doco);

            }
            return redirect()->back()->with('emailsent', 'DISC Request Sent!');

        }
        else{
            return redirect()->back()->with('emaildidntsent',"DISC Request Didn't Sent!");
        }



    }

    public function filedTicketMail(Request $request,$id)
    {
        $ticket= PayTicket::findOrFail($id);
        $icon=PayTicket::findOrFail($id);

        // $contact=IconList::findOrFail($icon->id)->contact;

        $mailcode=$icon->icon_code;

        $iconmail=IconList::where('icon_code',$mailcode)->first();

        if(\request('completed')==1){

            if(\request('date')){

                if(\request('comments')){

                    if ($request->hasFile('images')) {

                        $image = $request->file('images');
                        $imageName=time().$image->getClientOriginalName();
                        $image->move(public_path()."/uploads/email/", $imageName);
                        $ticket->offence_date=\request('date');
                        $ticket->comments=\request('comments');
                        $ticket->personal_comments=\request('pcomments');
                        $ticket->completed=\request('completed');
                        $ticket->attached_image = $imageName;
                        $ticket->save();

                        $mail = new TicketFiled($ticket->ticket_no);
                        $mail->setDateFiled(\request('date'));
                        $mail->setComments(\request('comments'));
                        $mail->setImagePath($imageName);

                        Mail::to($ticket->email)->send($mail);
                        return redirect('/historytickets/'.$icon->id)->with('emailsent', 'Mail Sent!');
                    }

                    return redirect('/tickets/'.$icon->id)->with('emailattachment', 'Attachment Required!');
                }
                return redirect('/tickets/'.$icon->id)->with('emailcomments', 'Comments Required!');
            }
            return redirect('/tickets/'.$icon->id)->with('emaildate', 'Date Required!');
        }
        return redirect('/tickets/'.$icon->id)->with('emaildidntsent',"Mail Didn't Sent!");
    }

}
