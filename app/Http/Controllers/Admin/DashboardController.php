<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\User;
use App\Models\Contact;
use App\Models\PayTicket;
use DataTables;
use Storage;
use Auth;
use Hash;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function profile()
    {
        $data = [
            'user'    =>  Auth::guard('admin')->user(),
        ];
        return view('admin.profile.setting',$data);
    }

    /**
     *  Profile Update of Specified Resource
     * @return Rederable
    */
    public function setting(Request $request)
    {
        $data = $request->validate([
            'name'              => 'required|string|max:100',
            'avatar'            => 'nullable|max:3000|mimes:png,jpg,jpeg,webp',
            'email'             => "required|max:255|unique:users,email,".Auth::guard('admin')->user()->id,
            "dob"               => 'required|date_format:Y-m-d',
            'current_password'  => 'required_if:change_password,1',
            'password'          => 'required_if:change_password,1|confirmed|min:8',
        ]);

        $user   =   Auth::guard('admin')->user();
        $now    =   \Carbon\Carbon::parse(now());
        $dob    =   \Carbon\Carbon::parse($request->dob);
        $age    =   $dob->diffInYears($now);

        // Check Age Validity
        if($age < 18){
            return back()->with('error','User must be at least 18 years old to register into the system, please enter a valid DOB');
        }
        if($request->change_password == 1 && !Hash::check($request->current_password, $user->password))
        {
            return redirect()->back()->withErrors(['current_password'=>'your current password does not match'])->withInput($request->all());

        }

        if($request->avatar){

            if(!empty($user->avatar)){
                Storage::disk('uploads')->delete($user->avatar);
            }
            $data['avatar'] = Storage::disk('uploads')->putFile('',$request->avatar);
        }
        if($request->password)
        {
            $data['password'] = Hash::make($request->password);
            activityLogs('Password Changed');
        }

        $user->update($data);
        activityLogs('Personal Profile Update');
        return redirect()->back()->with('success','Profile Updated Successfully');
    }

    public function updatePersonalComments(Request $request){
        $request->validate([
            'personal_comments' =>  'required',
            'type'              =>  'in:contact,payticket',
            'id'                =>  'required',
        ]);
        // return $request->all();
        if($request->type == 'contact'){
            $contact = Contact::find($request->id);
            $contact->update(['personal_comments'=>$request->personal_comments]);
        }
        if($request->type == 'payticket'){
            $payticket = PayTicket::find($request->id);
            $payticket->update(['personal_comments'=>$request->personal_comments]);
        }
        return redirect()->back()->with('comment_update','Personal Comments Updated Successfully');
    }
}
