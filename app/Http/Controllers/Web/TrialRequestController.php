<?php

namespace App\Http\Controllers\Web;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\IconList;
use App\Models\Contact;
use App\Models\Tickets;
use Storage;
use App\Mail\TicketReceived;
use Mail;
use PhpOffice\PhpWord\TemplateProcessor;
use File;

class TrialRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trialRequest()
    {
        $data = [
            'icons' => IconList::all()->sortByDesc("icon_code"),
        ];

        return view('web.trial_request',$data);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function earlyRequest()
    {
        $data = [
            'icons' => IconList::all()->sortByDesc("icon_code"),
        ];
        return view('web.early_request',$data);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trialStep1(Request $request)
    {

        $data = $request->validate([
            'icon_id'       =>  'required|exists:icon_lists,icon_code|min:4|max:4',
            'ticket_no'     =>  'required|array',
        ]);
        $response['success'] = 'Trial Step 1 Successfully.';
        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trialStep2(Request $request)
    {
        $data = $request->validate([
            'ticket_photo'     =>  'required|array',
        ]);
        $response['success'] = 'Trial Step 2 Successfully.';
        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trialStep3(Request $request)
    {
        $data = $request->validate([
            'first_name'    =>  'required|min:2|max:20',
            'last_name'     =>  'required|min:2|max:20',
            'email'         =>  'required|string|email|max:255',
            'phone_no'      =>  'required|numeric',
            'address'       =>  'required|string|max:500',
            'city'          =>  'required|min:2|max:50',
            'province'      =>  'required|min:2|max:50',
            'postal_code'   =>  'required',
        ]);
        $response['success'] = 'Trial Step 3 Successfully.';
        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trialStep4(Request $request, $type)
    {
        $contact_data = $request->validate([
            'first_name'    =>  'required|min:2|max:20',
            'last_name'     =>  'required|min:2|max:20',
            'email'         =>  'required|string|email|max:255',
            'phone'         =>  'required|numeric',
            'address'       =>  'required|string|max:500',
            'city'          =>  'required|min:2|max:50',
            'province'      =>  'required|min:2|max:50',
            'postal_code'   =>  'required',
            'interpreter'   =>  'required_if:interpreter,yes',
            'date'          =>  'required|date',
            'icon_code'     =>  'required|exists:icon_lists,icon_code|min:4|max:4',
        ]);
        $contact_data['date'] = \Carbon\Carbon::parse($request->date)->format('Y-m-d');
        $contact_data['type'] =  $type;

        $contact = Contact::create($contact_data);

        foreach ($request->ticket_no as $key => $ticket_no) {

            $images_name    = Storage::disk('file')->putFile('',$request->ticket_photo[$key]);
            $ticket_data    = [
                'ticket_no'      =>  $ticket_no,
                'images_name'    =>  $images_name,
                'contact_id'     =>  $contact->id,
                'type'           =>  $type,
            ];
            Tickets::create($ticket_data);
           
            $wordFile=new TemplateProcessor('word/Ticket Received.docx');
            $wordFile->setValue('ticket_no',$ticket_no);
            $wordFile->setValue('icon_code',$request->icon_code);
            $fileName=$request->first_name.'-'.$request->last_name.'#'.$request->icon_code.'-'.$ticket_no;
            $wordFile->saveAs($fileName.'.docx');    
            $subject = 'Ticket Received';
            $mail_items = [
                'tickets'   =>  [$key => $ticket_no],
                'icon'      =>  $request->icon_code,
                'type'      =>  'traffic',
            ];
            $doco='attachfile.docx';
            // dd($doco);
            if (File::exists($doco)) {
                unlink($doco);
    
            }
        }
        $email = Mail::to($contact_data['email'])->send(new TicketReceived($mail_items,$subject));

        $response['success'] = 'Trial Step 4 Successfully.';
        return response()->json($response, 200);
    }

}
