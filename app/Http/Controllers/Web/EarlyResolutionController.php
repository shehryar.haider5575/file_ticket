<?php

namespace App\Http\Controllers\Web;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\IconList;

class EarlyResolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function payStep()
    {
        $data = [
            'icons' => IconList::all()->sortByDesc("icon_code"),
        ];

        return view('web.payticket',$data);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function payStep1(Request $request)
    {
        $response['success'] = 'Step 1 Cached Successfully.';
        return response()->json($response, 200);
        cache()->set('ticket_no',$request->ticket_no);
        cache()->set('datefiled',$request->datefiled);
        cache()->set('icon_id',$request->icon_id);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function payStep2(Request $request)
    {
        $response['success'] = 'Step 2 Cached Successfully.';
        return response()->json($response, 200);
        cache()->set('ticket_no',$request->ticket_no);
        cache()->set('datefiled',$request->datefiled);
        cache()->set('icon_id',$request->icon_id);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function payStep3(Request $request)
    {
        $response['success'] = 'Step 3 Cached Successfully.';
        return response()->json($response, 200);
        cache()->set('ticket_no',$request->ticket_no);
        cache()->set('datefiled',$request->datefiled);
        cache()->set('icon_id',$request->icon_id);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function payStep4(Request $request)
    {
        $response['success'] = 'Step 4 Cached Successfully.';
        return response()->json($response, 200);
        cache()->set('ticket_no',$request->ticket_no);
        cache()->set('datefiled',$request->datefiled);
        cache()->set('icon_id',$request->icon_id);
    }

}
