<?php

namespace App\Http\Controllers\Web;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\IconList;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('web.index');
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function payStep()
    {
        $data = [
            'icons' => IconList::all()->sortByDesc("icon_code"),
        ];

        return view('web.step-1',$data);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function earlyStep()
    {
        return view('web.index');
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trialStep()
    {
        $data = [
            'icons' => IconList::all()->sortByDesc("icon_code"),
        ];

        return view('web.trial_request',$data);
    }
}
