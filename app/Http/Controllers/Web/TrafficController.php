<?php

namespace App\Http\Controllers\Web;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\IconList;
use App\Models\Contact;
use App\Models\Tickets;
use Storage;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use App\Mail\TicketReceived;
use Mail;
use Crypt;
use App\Models\PayTicket;
use File;
use PhpOffice\PhpWord\TemplateProcessor;

class TrafficController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trafficTicket($type)
    {
        $data = [
            'icons' => IconList::all()->sortByDesc("icon_code"),
            'type'  => $type
        ];
        return view('web.trafficticket',$data);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function earlyRequest()
    {
        $data = [
            'icons' => IconList::all()->sortByDesc("icon_code"),
        ];
        return view('web.early_request',$data);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trafficStep1(Request $request, $type)
    {

        $request->validate([
            'icon_id'           =>  'required|exists:icon_lists,icon_code',
            'ticket_no'         =>  'required',
            'datefiled'         =>  'required',
            'bussiness_name'    =>  'required',
        ]);

        switch ($type) {
            case 'traffic':
                $ticket_type = 'noticeOfFine';
                break;

            case 'parking':
                $ticket_type = 'noticeOfFine';
                break;

            case 'speed-camera':
                $ticket_type = 'speedCamera';
                break;

            case 'red-light-camera':
                $ticket_type = 'redLight';
                break;
        }

        $data = [
            "iconLocation"          => $request->icon_id,
            "ticketType"            => $ticket_type,
            "offenceNumber"         => $request->ticket_no,
            "lastOrBusinessName"    => $request->bussiness_name,
            "dateOffence"           => \Carbon\Carbon::parse($request->datefiled)->format('Y/m/d')
        ];

        $client     =   new \GuzzleHttp\Client();
        $URI = 'http://74.208.108.24:7001/dev/api';
        $full = json_encode($data);
        try {
            $response = $client->request('POST',$URI, [
                'body'          => $full,
                'headers'       => [
                    'Content-Type'  => 'application/json',
                    'Accept'  => 'application/json',
                ]
            ]);
            $res_data = $response->getBody();
            // dd($res_data);
            $res_data = json_decode($res_data);
            return response()->json(['success'=>$res_data->data],200);

        } catch (\Exception $exception) {
            $e = $exception->getMessage();
            // dd($e);
            $res1 = explode('data',$exception->getMessage());
            $res2 = explode('error',$res1[1]);
            $error = str_replace( array( '\'', '"',':','}',',' , ';', '<', '>' ), ' ',$res2[1]);
            return response()->json(['error'=>$error],499);
        }
        // dd("assdfasdf");
        $res_data = [
            'fine_amount'       =>  $res_data->data->TicketAmount,
            'convenience_fee'   =>  '5',
            'total'             =>  $res_data->data->TicketAmount,
        ];

        $response['success'] =  'Traffic Request Step 1 Successfully.';
        $response['data']    =  $res_data;

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trafficStep2(Request $request)
    {
        $data = $request->validate([
            'fine_amount'       =>  'required',
            'convince_fee'      =>  'required',
            'total_payable'     =>  'required',
        ]);
        $response['success'] = 'Traffic Request Step 2 Successfully.';
        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function trafficStep3(Request $request)
    {
        $data = $request->validate([
            'card_holder_name'    =>  'required|min:2|max:20',
            'email'               =>  'required|string|email|max:255',
            'postal_code'         =>  'required',
            'card_no'             =>  'required',
            'expiry_date'         =>  'required',
        ]);
        $response['success'] = 'Traffic Request 3 Successfully.';
        return response()->json($response, 200);
    }

    public function chargeCreditCard(Request $request, $type)
    {
        $data = $request->validate([
            'card_holder_name'    =>  'required|min:2|max:50',
            'email'               =>  'required|string|email|max:255',
            'icon_code'           =>  'required|exists:icon_lists,icon_code',
            'card_no'             =>  'required',
            'expiry_date'         =>  'required',
            'total_payment'       =>  'required',
            'fine_amount'         =>  'required',
            'convenience_amount'  =>  'required',
            'postal_code'         =>  'required',
            'phone'               =>  'required',
            'offence_date'        =>  'required',
            'ticket_no'           =>  'required',
            'type'                =>  'required',

        ]);
        // return $request->all();
        $card_date = explode('/',$request->expiry_date);
        // dd($card_date);
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(config('services.authorize.login'));
        $merchantAuthentication->setTransactionKey(config('services.authorize.key'));
        $refId      = 'ref'.time();// Create the payment data for a credit card

        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($request->card_no);

        // $creditCard->setExpirationDate( "2038-12");
        $expiry = $card_date[0] . '-' . $card_date[1];
        $creditCard->setExpirationDate($expiry);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);// Create a transaction
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount(trim(str_replace('$','',$data['total_payment'])));
        $transactionRequestType->setPayment($paymentOne);
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
// return $request->all();
        // $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        // if ($response != null)
        // {
        //     $tresponse = $response->getTransactionResponse();
        //     if (($tresponse != null) && ($tresponse->getResponseCode()=="1"))
        //     {

        //         echo "Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() . "\n";
        //         echo "Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";
        //         dd($data);
        //     }
        //     else
        //     {
        //         echo "Charge Credit Card ERROR :  Invalid response\n";
        //     }
        // }
        // else
        // {
        //     echo  "Charge Credit Card Null response returned";
        // }
        $data['offence_date']       = \Carbon\Carbon::parse($data['offence_date'])->format('Y-m-d');
        $data['total_amount']       = trim(str_replace('$','',$data['total_payment']));
        $data['fine_amount']        = trim(str_replace('$','',$data['fine_amount']));
        $data['convenience_amount'] = trim(str_replace('$','',$data['convenience_amount']));
        $data['card_no']            = Crypt::encrypt($data['card_no']);
        $data['card_expiry_month']  = Crypt::encrypt($card_date[0]);
        $data['card_expiry_year']   = Crypt::encrypt($card_date[1]);
        $data['type']               = strtoupper($data['type']);

        $payticket                  = PayTicket::create($data);
        $subject = 'Ticket Received';
        $mail_items = [
            'tickets'           =>  $payticket->ticket_no,
            'icon'              =>  $payticket->icon_code,
            'type'              =>  'payticket',
            'ticket_type'       =>  strtoupper(str_replace('-',' ', $data['type'])),
        ];

        $wordFile=new TemplateProcessor('word/Ticket Received.docx');
        $wordFile->setValue('ticket_no',$data['ticket_no']);
        $wordFile->setValue('icon_code',$data['icon_code']);
        $fileName=$data['card_holder_name'].'#'.$data['icon_code'].'-'.$payticket->ticket_no;
        $wordFile->saveAs($fileName.'.docx');    
        $doco='attachfile.docx';
        // dd($doco);
        if (File::exists($doco)) {
            unlink($doco);

        }
        $email = Mail::to($data['email'])->send(new TicketReceived($mail_items,$subject));

        $response['success']        = 'Successfully.';
        $response['payticket']      = $payticket;
        return response()->json($response, 200);
    }
}
