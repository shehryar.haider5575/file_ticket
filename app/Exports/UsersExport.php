<?php

namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class UsersExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $sql="select contacts.first_name,contacts.last_name,contacts.email,contacts.phone,contacts.address,contacts.city,contacts.province,contacts.postal_code,icons.icon_code,icons.date as offenct_date,icons.interpreter,tickets.emaildateticket,GROUP_CONCAT(tickets.ticket_no) AS tickets_nos from contacts JOIN icons on contacts.id=icons.contact_id join tickets on icons.id=tickets.icon_id where tickets.completed like '0' GROUP BY contacts.id";
        $details=collect($contacts=DB::select($sql,[]));
        return view('excel',[
            'details'=>$details,
        ]);
        // TODO: Implement view() method.
    }
}
