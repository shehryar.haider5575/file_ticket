<?php

namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use App\Models\Contact;

class UsersHistoryExport implements FromView
{
    public $entries;
    public $type;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($type, $entries)
    {
        $this->entries=$entries;
        $this->type=$type;
    }

    public function view(): View
    {
        // dd($this->entries);
        $details = Contact::with('trial_request_tickets')->where('type',$this->type)->paginate($this->entries);
        //    $sql="select contacts.first_name,contacts.last_name,contacts.email,contacts.phone,contacts.address,contacts.city,contacts.province,contacts.postal_code,icons.icon_code,icons.date as offenct_date,icons.interpreter,GROUP_CONCAT(tickets.ticket_no) AS tickets_nos from contacts JOIN icons on contacts.id=icons.contact_id join tickets on icons.id=tickets.icon_id where tickets.completed like '1' GROUP BY contacts.id";
        //     $details=collect($contacts=DB::select($sql,[]));
        return view('admin.excel',[
            'details'=>$details,
        ]);
        // TODO: Implement view() method.
    }
}
