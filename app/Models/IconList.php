<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IconList extends Model
{
    protected $table = 'icon_lists';
    protected $fillable = ['icon_number','email'];
}
