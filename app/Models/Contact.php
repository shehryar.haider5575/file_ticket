<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $fillable = ['first_name','last_name','email','phone','address','city','province','postal_code','icon_code','date','interpreter','type','emaildate','client_status','potential_status','personal_comments'];

    /**
     * Get all of the tickets for the Contact
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trial_request_tickets()
    {
        return $this->hasMany(Tickets::class, 'contact_id', 'id')->latest();
    }

    /**
     * Get the icon associated with the Contact
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function icon()
    {
        return $this->hasOne(IconList::class, 'icon_code', 'icon_code');
    }

}
