<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $table = 'tickets';
    protected $fillable = ['contact_id','ticket_no','images_name','attached_image','comments','personal_comments','datefiled','completed','emaildateticket','type','completed_at'];

    /**
     * Get the contact associated with the Tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contact()
    {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }
}
