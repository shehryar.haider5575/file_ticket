<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayTicket extends Model
{
    protected $table = 'pay_tickets';
    protected $fillable = ['offence_date','icon_code','ticket_no','card_holder_name','card_no','card_expiry_month','card_expiry_year','cvv','email','postal_code','fine_amount','convenience_amount','total_amount','attached_image','comments','personal_comments','completed','emaildateticket','type','client_status','potential_status','datefiled'];

    /**
     * Get the contact associated with the Tickets
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contact()
    {
        return $this->hasOne(Contact::class, 'id', 'contact_id');
    }

    /**
     * Get the icon associated with the PayTicket
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function icon()
    {
        return $this->hasOne(IconList::class, 'icon_code', 'icon_code');
    }
}
