<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TicketFiled extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $datefiled;
    public $comments;
    public $image;
public $ticket;
    public function __construct($ticket)
    {
        $this->ticket=$ticket;
    }
    public function setDateFiled($datefiled){
        $this->datefiled=$datefiled;
    }
    public function setComments($comments){
        $this->comments=$comments;
    }

    public function setImagePath($image){
        $this->image=$image;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.ticketfiled')->attach(public_path().'/uploads/email/'.$this->image,[
            'as'=>$this->image
        ]);
    }
}
