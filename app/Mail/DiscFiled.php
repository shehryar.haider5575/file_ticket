<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DiscFiled extends Mailable
{
      use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $ticket;
    public $fileName;
    public $name;


    public function __construct($ticket,$subject, $fileName, $name)
    {
        $this->subject  =   $subject;
        $this->ticket   =   $ticket;
        $this->fileName =   $fileName;
        $this->name     =   $name;
    }
    // public function setDateFiled($datefiled){
    //     $this->datefiled=$datefiled;
    // }
    // public function setName($name){
    //     $this->name=$name;
    // }
    //  public function setSubject($subject){
    //     $this->subject=$subject;
    // }

    // public function setImagePath($fileName){
    //     $this->image=$fileName.'.docx';
    // }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($subject);
        // die();
        return $this->markdown('email.discmail')->subject($this->subject)->attach(base_path().'/'.$this->fileName,[
            'as'=>$this->fileName
        ]);
    }

}
