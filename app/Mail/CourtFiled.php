<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CourtFiled extends Mailable
{
      use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $datefiled;
    public $name;
    public $image;
    public $ticket;


    public function __construct($ticket,$subject)
    {
        $this->subject = $subject;
        $this->ticket=$ticket;
        $this->image='attachfile.docx';
        // $this->datefiled=$datefiled;
        // $this->name=$name;
        // $this->image=$image;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($subject);
        // die();
        return $this->markdown('email.courtmail')->subject($this->subject)
        ->attach(base_path().'/'.$this->image,[
            'as'=>$this->image
        ]);
    }

}
