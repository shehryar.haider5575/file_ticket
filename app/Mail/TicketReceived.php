<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TicketReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    // public $icon;
    // public $ticket_no;
    // public $ticket_no1;
    // public $ticket_no2;
    // public $ticket_no3;
    public $mail_items;
    public $subject;
    public $attach;

    public function __construct($mail_items,$subject)
    {
        $this->subject=$subject;
        $this->mail_items=$mail_items;
        $this->attach='attachfile.docx';
    }
    // public function setTicket2($ticket_no1){
    //     $this->ticket_no1=$ticket_no1;
    // }
    // public function setTicket3($ticket_no2){
    //     $this->ticket_no2=$ticket_no2;
    // }
    // public function setTicket4($ticket_no3){
    //     $this->ticket_no3=$ticket_no3;
    // }
    // public function setTicket5($ticket_no4){
    //     $this->ticket_no4=$ticket_no4;
    // }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.ticketreceived')->subject($this->subject)
        ->attach(base_path().'/'.$this->attach,[
            'as'=>$this->attach
        ]);
    }
}
